﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;


namespace PrescriptionApp.CustomeRender
{
    public class PLPicker : Picker
    {
        public static readonly BindableProperty SetHintTextColorProperty =
            BindableProperty.Create(nameof(SetHintTextColor), typeof(Color), typeof(PLPicker), Color.Default);


        public Color SetHintTextColor
        {
            get { return (Color)GetValue(SetHintTextColorProperty); }
            set { SetValue(SetHintTextColorProperty, value); }
        }

    }

}
