﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.ViewModel;
using PrescriptionApp.Model;
using PrescriptionApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddAppoinmentPageXaml : BaseContentPage<AddAppoinmentPageViewModel> { };
    public partial class AddAppoinmentPage : AddAppoinmentPageXaml
    {
        public AddAppoinmentPage()
        {
            InitializeComponent();
        }


    }
}
