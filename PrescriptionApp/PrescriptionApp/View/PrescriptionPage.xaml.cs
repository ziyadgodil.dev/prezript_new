﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class PrescriptionPageXaml : BaseContentPage<PrescriptionPageViewModel> { };
    public partial class PrescriptionPage : PrescriptionPageXaml 
    {
       
        public PrescriptionPage()
        {
            InitializeComponent();
        }

    }
}