﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.View
{
    public partial class SidebarMenu : FlyoutPage
    {
        public ICommand OnMenuClick => new Command<string>(MenuClick);

        public ICommand LogoutCommand => new Command(Logout);
        public SidebarMenu()
        {
            InitializeComponent();
            BindingContext = this;

            var browser = new WebView
            {
                Source="www.goggle.com"
            };
          MessagingCenter.Subscribe<SidebarMenu>(this, "OpenMenu", (value) =>
          {
                IsPresented = true;
          });
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            
            UserName = Settings.FirstName + " " + Settings.LastName;
            lblName.Text = UserName;

            ImageUrl = Services.ServiceConfiguration.ImageUrl + Path.GetFileName(Settings.UserImage);
            lblImageURL.Source = ImageUrl;

        }

        string _webview { get; set; }

        public string Webview { get; set; }
        public string UserName { get;  set; }
        string _imageUrl { get;  set; }

        public string ImageUrl
        {
            get => _imageUrl;
            set
            {
                _imageUrl = value;
                OnPropertyChanged(nameof(ImageUrl));
            }
        }

        // public string Image { get;  set; }

        public async void MenuClick(string obj)
        {

            switch (obj)
            {
                case "1":
                    await Application.Current.MainPage.Navigation.PushAsync(new ProfilePage());
                    IsPresented = false;
                    break;
                case "2":
                    await Application.Current.MainPage.Navigation.PushAsync(new PrivacyPolicy());
                  //  Detail =  new NavigationPage((Page) Activator.CreateInstance(typeof(PrescriptionPage)));
                    IsPresented = false;
                    break;
                case "4":
                    await Application.Current.MainPage.Navigation.PushAsync(new HelpSupportPage());
                    //  Detail =  new NavigationPage((Page) Activator.CreateInstance(typeof(PrescriptionPage)));
                    IsPresented = false;
                    break;
                case "5":
                    await Application.Current.MainPage.Navigation.PushAsync(new HealthBlog());
                    //  Detail =  new NavigationPage((Page) Activator.CreateInstance(typeof(PrescriptionPage)));
                    IsPresented = false;
                    break;

                default:
                    break;

            }
            IsPresented = false;
        }

        async void Logout()
        {
            var Confirm = await Application.Current.MainPage.DisplayAlert("Prezript", "Are you sure you want to logout?", "Yes", "Cancel");
            if (Confirm)
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {

                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.Logout();

                        if (result != null && result.Status)
                        {
                            Application.Current.MainPage = new NavigationPage(new SignInPage());
                            Settings.UserLoggedIn = false;
                            Settings.FirstName=null;
                            Settings.LastName=null;


                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
        }


    }
}

