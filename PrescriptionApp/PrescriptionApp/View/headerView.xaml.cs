﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.View
{	
	public partial class headerView : Grid
    {
        public ICommand ShowMenu => new Command(Show);
        //public ICommand LogoutCommand => new Command(Logout);
        ////public bool IsBack { get; set; } = false;

        public string Image { get; set; }
       

 

        public headerView ()
		{
			InitializeComponent ();
            BindingContext = this;
            Image = Services.ServiceConfiguration.ImageUrl + Path.GetFileName(Settings.UserImage);
            DashImage.Source = Image;
            DashImage.Source = Image.ToString();
        }
        public void Show()
        {
            MessagingCenter.Send(new SidebarMenu() { IsPresented = true }, "OpenMenu");
        }

        //async void Logout()
        //{
        //    var Confirm = await Application.Current.MainPage.DisplayAlert("Prezript", "Are you sure you want to logout?", "Yes", "Cancel");
        //    if (Confirm)
        //    {
        //        using (UserDialogs.Instance.Loading("Loading.."))
        //        {

        //            if (App.IsConnected)
        //            {
        //                var result = await App.PAServiceManager.Logout();

        //                if (result != null && result.Status)
        //                {
        //                    Application.Current.MainPage = new NavigationPage(new SignInPage());
        //                    Settings.UserLoggedIn = false;
        //                }

        //            }
        //            else
        //            {
        //                await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
        //            }
        //        }
        //    }
        //}
    }
}

