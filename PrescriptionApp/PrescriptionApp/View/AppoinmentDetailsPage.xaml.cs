﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class AppoinmentDetailsPageXaml : BaseContentPage<AppoinmentDetailsPageViewModel> { }

    public partial class AppoinmentDetailsPage : AppoinmentDetailsPageXaml
    {
        public AppoinmentDetailsPage()
        {
            InitializeComponent();
        }
    }
}