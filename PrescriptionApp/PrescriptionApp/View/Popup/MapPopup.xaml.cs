﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPopup : PopupPage
    {
        public event EventHandler<double[]> SelectionHandler;

        public double lat { get; set; }
        public double lng { get; set; }

        public MapPopup()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            map.InitialCameraUpdate = CameraUpdateFactory.NewCameraPosition(
                   new CameraPosition(new Position(lat, lng), 14d));

            var pin = new Pin()
            {
                Type = PinType.Place,
                Label = "My Location",
                Position = new Position(lat, lng),
                Tag = "my_location",
            };
            map.Pins.Clear();
            map.Pins.Add(pin);

            map.MapClicked += (sender, e) =>
            {
                var pin1 = new Pin()
                {
                    Type = PinType.Place,
                    Label = "My Location",
                    Position = new Position(e.Point.Latitude, e.Point.Longitude),
                    Tag = "my_location",
                };
                map.Pins.Clear();
                map.Pins.Add(pin1);
                map.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromMeters(5000)));
                SelectionHandler?.Invoke(this, new double[] { e.Point.Latitude, e.Point.Longitude });
                PopupNavigation.Instance.PopAsync();
            };
            base.OnAppearing();
        }
    }
}