﻿using PrescriptionApp.Model;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrescriptionPopUpPage : PopupPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        internal bool SetPropertyChanged<T>(ref T currentValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            return PropertyChanged.SetProperty(this, ref currentValue, newValue, propertyName);
        }

        internal void SetPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



        //strength
        private List<string> _listStrength;
        public List<string> ListStrength

        {
            get { return _listStrength; }
            set { _listStrength = value; SetPropertyChanged(nameof(ListStrength)); }

        }


        //preparation
        private List<string> _listPreparation;
        public List<string> ListPreparation

        {
            get { return _listPreparation; }
            set { _listPreparation = value; SetPropertyChanged(nameof(ListPreparation)); }

        }

        //Route
        private List<string> _listRoute;
        public List<string> ListRoute

        {
            get { return _listRoute; }
            set { _listRoute = value; SetPropertyChanged(nameof(ListRoute)); }

        }

        //Route
        private List<string> _listDirection;
        public List<string> ListDirection

        {
            get { return _listDirection; }
            set { _listDirection = value; SetPropertyChanged(nameof(ListDirection)); }

        }

        //Frequency
        private List<string> _listFrequency;
        public List<string> ListFrequency

        {
            get { return _listFrequency; }
            set { _listFrequency = value; SetPropertyChanged(nameof(ListFrequency)); }

        }





        private string _selectedStrength;
        public string SelectedStrength

        {
            get { return _selectedStrength; }
            set { _selectedStrength = value; SetPropertyChanged(nameof(SelectedStrength)); }

        }




        public PrescriptionPopUpPage()
        {
            InitializeComponent();

            //strength
            ListStrength = new List<string>();
            ListStrength.Add("mg");
            ListStrength.Add("gm");
            ListStrength.Add("ng");
            ListStrength.Add("mcg");


            //preparation
            ListPreparation = new List<string>();
            ListPreparation.Add("Tab.");
            ListPreparation.Add("Cap.");
            ListPreparation.Add("Injec.");
            ListPreparation.Add("Syr.");
            ListPreparation.Add("Drops.");

            //route
            ListRoute = new List<string>();
            ListRoute.Add("Topical");
            ListRoute.Add("Oral");
            ListRoute.Add("Rectal");
            ListRoute.Add("Vaginal");
            ListRoute.Add("Inhalation");
            ListRoute.Add("Local");
            ListRoute.Add("Chew");
            ListRoute.Add("Intradermal");
            ListRoute.Add("Subcutaneous");


            //direction
            ListDirection = new List<string>();
            ListDirection.Add("Before Meal");
            ListDirection.Add("After Meal");



            //frequency
            ListFrequency = new List<string>();
            ListFrequency.Add("Immediately");
            ListFrequency.Add("Once a Day");
            ListFrequency.Add("Thrice a Day");
            ListFrequency.Add("Fourth times a Day");
            ListFrequency.Add("Every hour");


            this.BindingContext = this;
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

        }



        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PopAllPopupAsync();

        }

    }
}