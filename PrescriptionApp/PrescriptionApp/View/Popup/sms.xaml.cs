﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class sms : PopupPage, INotifyPropertyChanged
    {
		public sms ()
		{
			InitializeComponent ();
		}

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                     Xamarin.Essentials.Sms.ComposeAsync(new SmsMessage("", EntryNumber.Text));
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
            }
        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            try
            {
                Xamarin.Essentials.Sms.ComposeAsync(new SmsMessage("", EntryNumber1.Text));
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
            }
        }
    }
}