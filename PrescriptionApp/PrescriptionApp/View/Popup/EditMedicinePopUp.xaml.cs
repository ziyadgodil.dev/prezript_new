﻿using Acr.UserDialogs;
using PrescriptionApp.Model;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditMedicinePopUp : PopupPage, INotifyPropertyChanged
    {
        public event EventHandler<AddEditmedicineModel> SelectionHandler;

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        bool _isEditMode { get; set; }
        public bool IsEditMode
        {
            get => _isEditMode;
            set
            {
                _isEditMode = value;
                SetPropertyChanged(nameof(IsEditMode));
            }
        }

        //strength
        private bool _isEdit;
        public bool IsEdit

        {
            get { return _isEdit; }
            set { _isEdit = value; SetPropertyChanged(nameof(IsEdit)); SetPropertyChanged(nameof(PopupTitle)); }

        }

        public string PopupTitle
        {
            get => IsEdit ? "Edit Your Medicine Here" : "Add Your Medicine Here";
        }

        AddEditmedicineModel _addEditmedicine;
        public AddEditmedicineModel AddEditmedicine
        {
            get => _addEditmedicine;
            set
            {
                _addEditmedicine = value;
                SetPropertyChanged(nameof(AddEditmedicine));
            }
        }

        //strength
        ObservableCollection<string> _listStrength;
        public ObservableCollection<string> ListStrength
        {
            get { return _listStrength; }
            set { _listStrength = value; SetPropertyChanged(nameof(ListStrength)); }

        }

        //preparation
        ObservableCollection<string> _listPreparation;
        public ObservableCollection<string> ListPreparation
        {
            get { return _listPreparation; }
            set { _listPreparation = value; SetPropertyChanged(nameof(ListPreparation)); }

        }

        //Route
        ObservableCollection<string> _listRoute;
        public ObservableCollection<string> ListRoute
        {
            get { return _listRoute; }
            set { _listRoute = value; SetPropertyChanged(nameof(ListRoute)); }

        }

        //Route
        ObservableCollection<string> _listDirection;
        public ObservableCollection<string> ListDirection
        {
            get { return _listDirection; }
            set { _listDirection = value; SetPropertyChanged(nameof(ListDirection)); }

        }

        //Frequency
        ObservableCollection<string> _listFrequency;
        public ObservableCollection<string> ListFrequency
        {
            get { return _listFrequency; }
            set { _listFrequency = value; SetPropertyChanged(nameof(ListFrequency)); }
        }


        public ICommand CancelCommand => new Command(Cancel);
        public ICommand SaveCommand => new Command(SaveData);
        public ICommand IsEditMedicineCommand => new Command(Editmedicine);

        public ICommand RemoveCommand => new Command(RemoveData);

        public EditMedicinePopUp()
        {
            InitializeComponent();
            BindingContext = this;
            // IsEditMode = !IsEdit;
            //strength
            ListStrength = new ObservableCollection<string>
            {
                "mg",
                "gm",
                "ng",
                "mcg"
            };


            //preparation
            ListPreparation = new ObservableCollection<string>
            {
                "Tab.",
                "Cap.",
                "Injec.",
                "Syr.",
                "Drops."
            };

            //route
            ListRoute = new ObservableCollection<string>
            {
                "Topical",
                "Oral",
                "Rectal",
                "Vaginal",
                "Inhalation",
                "Local",
                "Chew",
                "Intradermal",
                "Subcutaneous"
            };


            //direction
            ListDirection = new ObservableCollection<string>
            {
                "Before Meal",
                "After Meal"
            };



            //frequency
            ListFrequency = new ObservableCollection<string>
            {
                "Immediately",
                "Once a Day",
                "Thrice a Day",
                "Fourth times a Day",
                "Every hour"
            };

        }



        protected override void OnAppearing()
        {
            base.OnAppearing();
            //if (AddEditmedicine.MedicineId != 0)
            //{
            //    AddEditmedicine.StrengthUnit = ListStrength.FirstOrDefault(x => x == AddEditmedicine.StrengthUnit);
            //}
        }



        async void Cancel()
        {
            await PopupNavigation.Instance.PopAsync();
        }
        void Editmedicine()
        {
            IsEdit = !IsEdit;
        }
        async void SaveData()
        {
            if (string.IsNullOrEmpty(AddEditmedicine.DrugsName) ||
                string.IsNullOrEmpty(AddEditmedicine.Strength) ||
                string.IsNullOrEmpty(AddEditmedicine.StrengthUnit) ||
                string.IsNullOrEmpty(AddEditmedicine.PreparationValue) ||
                string.IsNullOrEmpty(AddEditmedicine.DirectionValue))
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", "Plaese enter All Values", "OK");
                return;
            }
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.AddMedicine(AddEditmedicine);
                        if (result.Status)
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Medicine added successfully", "Ok");
                            SelectionHandler?.Invoke(null, result.Medicine);
                            await PopupNavigation.Instance.PopAsync();
                        }
                        else if (result == null || result.Status)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }

                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        async void RemoveData()
        {
            var msg = await Application.Current.MainPage.DisplayAlert("Alert", "Are you sure for removing this entry?", "Yes", "No");

            if (msg == true)
            {
                SelectionHandler?.Invoke(null, AddEditmedicine);
                await PopupNavigation.Instance.PopAsync();
            }

        }




    }

}