﻿using Acr.UserDialogs;
using PrescriptionApp.Model;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppointmentPopup : PopupPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICommand DoneCommand => new Command(Done);
        public ICommand CancelCommand => new Command(Close);

        DateTime _selectedDate { get; set; }
        public DateTime SelectedDate
        {
            get => _selectedDate;
            set
            {
                _selectedDate = value;
                SetPropertyChanged(nameof(SelectedDate));
            }
        }

        public TimeSpan? OpeningTime { get; set; }
        public TimeSpan? ClosingTime { get; set; }

        TimeSpan _selectedTime { get; set; }
        public TimeSpan SelectedTime
        {
            get => _selectedTime;
            set
            {
                _selectedTime = value;
                SetPropertyChanged(nameof(SelectedTime));
            }
        }
        int _userId { get; set; }
        public int UserId
        {
            get => _userId;
            set
            {
                _userId = value;
                SetPropertyChanged(nameof(UserId));
            }
        }
        public AppointmentPopup()
        {
            InitializeComponent();
            BindingContext = this;

            DatePicker1.MinimumDate = DateTime.Now;
            TimePicker1.Time = DateTime.Now.TimeOfDay;
        }

        private async void Close()
        {
            await Application.Current.MainPage.Navigation.PopAllPopupAsync();
        }

        public async void Done()
        {
            if (DateTime.Now < SelectedDate || (DateTime.Now.Date == SelectedDate.Date && DateTime.Now.TimeOfDay < SelectedTime))
            {
                if (OpeningTime < SelectedTime && SelectedTime < ClosingTime)
                {
                    BookAppointment();
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Alert!", "Please Select Time Between Official Opening Time Or Closing Time!", "OK");
                    return;

                }

            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", "Please Select Date Or Time Grater Than Current Date Or Time !", "OK");
                //await Application.Current.MainPage.Navigation.PopAsync(true);
               
                return;
            }
        }
        public async void BookAppointment()
        {
            try
            {
                //check profile is  d "ProfileDetail"
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    AddApointmentInputModel addApointmentInputModel = new AddApointmentInputModel();
                    addApointmentInputModel.UserId = UserId;
                    addApointmentInputModel.AppointmentDate = (SelectedDate + SelectedTime).ToLocalTime();


                    var result = await App.PAServiceManager.AddAppointment(addApointmentInputModel);
                    if (result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Success", "Appointment booked successfully.", "Ok");
                        await PopupNavigation.Instance.PopAsync();
                        await Application.Current.MainPage.Navigation.PopToRootAsync();
                        await Application.Current.MainPage.Navigation.PopAsync(false);
                        await Application.Current.MainPage.Navigation.PopAsync(false);
                    }
                    else if (result == null || result.Status)
                    {
                        //ProfileData = CopyProfileData;
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }

                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", ex.Message, "Ok");
            }
        }

    }
}