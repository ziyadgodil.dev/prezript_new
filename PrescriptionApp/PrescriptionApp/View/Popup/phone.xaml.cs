﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class phone : PopupPage, INotifyPropertyChanged
    {
        public phone()
        {
            InitializeComponent();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

            try
            {
                Xamarin.Essentials.PhoneDialer.Open(EntryNumber.Text);
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Alert", "something went wrong", "ok");
            }
        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            try
            {
                Xamarin.Essentials.PhoneDialer.Open(EntryNumber1.Text);
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Alert", "something went wrong", "ok");
            }
        }

        //private void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Xamarin.Essentials.Sms.ComposeAsync(new SmsMessage(EntryNumber123.Text, EntryNumber1.Text));
        //    }
        //    catch (ArgumentNullException anEx)
        //    {
        //        // Number was null or white space
        //    }
        //    catch (FeatureNotSupportedException ex)
        //    {
        //        // Phone Dialer is not supported on this device.
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
    }
}