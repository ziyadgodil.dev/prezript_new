﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNotePopup : PopupPage, INotifyPropertyChanged
    {
        public event EventHandler<NotesModel> SelectionHandler;

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool _isEdit;
        public bool IsEdit

        {
            get { return _isEdit; }
            set { _isEdit = value; SetPropertyChanged(nameof(IsEdit)); SetPropertyChanged(nameof(EditSaveText)); }

        }
        private bool _isEditMode;
        public bool IsEditMode

        {
            get { return _isEditMode; }
            set { _isEditMode = value; SetPropertyChanged(nameof(IsEditMode)); SetPropertyChanged(nameof(PopupTitle)); }
        }

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        public string PopupTitle => IsEditMode ? "Edit Your Note Here" : "Add Your Note Here";

        public string EditSaveText => IsEdit ? "Save Note" : "Edit Note";

        NotesModel _note;
        public NotesModel Note
        {
            get => _note;
            set
            {
                _note = value;
                SetPropertyChanged(nameof(Note));
            }
        }
        public ICommand CancleCommand => new Command(Cancel);
        public ICommand AddCommand => new Command(AddData);
        public ICommand EditCommand => new Command(Edit);

        public AddNotePopup()
        {
            InitializeComponent();
            BindingContext = this;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        async void Cancel()
        {
            await PopupNavigation.Instance.PopAsync();
        }


        void Edit()
        {
            if (IsEdit)
            {
                AddData();
            }
            else
            {
                IsEdit = true;
            }
        }

        async void AddData()
        {
            if (string.IsNullOrEmpty(Note.Description) || string.IsNullOrEmpty(Note.Title))
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", "All Fields required!", "OK");
                return;
            }
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var result = await App.PAServiceManager.AddNotes(Note);
                    if (result.Status)
                    {
                        if (Note.NoteId == 0)
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Add note successfully", "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Edit note successfully", "Ok");
                        }


                        SelectionHandler?.Invoke(null, result.Note);
                        await PopupNavigation.Instance.PopAsync();
                    }
                    else if (result == null || result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }



        }
    }

}
