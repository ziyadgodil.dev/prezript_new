﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DegreePopup : PopupPage
    {
        public DegreePopup()
        {
            InitializeComponent();
        }
        

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PopAllPopupAsync();

        }
    }
}