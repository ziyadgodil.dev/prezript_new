﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Resources;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PrescriptionApp.Helper.Base;
using System.Windows.Input;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReasonPopup : PopupPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        string _descriptionText { get; set; }
        public string DescriptionText
        {
            get => _descriptionText;
            set
            {
                _descriptionText = value;
                SetPropertyChanged(nameof(DescriptionText));
            }
        }
        int _userId { get; set; }
        public int UserId
        {
            get => _userId;
            set
            {
                _userId = value;
                SetPropertyChanged(nameof(UserId));
            }
        }
        public int Appointmentstatus { get; set; }

        public ICommand CancelCommand => new Command(Cancel);
        public ICommand SubmitCommand => new Command(Submit);

        public ReasonPopup()
        {
            InitializeComponent();
            BindingContext = this;
        }

        private async void Cancel()
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private async void Submit()
        {
            if (string.IsNullOrWhiteSpace(DescriptionText))
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", "Please enter reason..", "OK");
                return;
            }
            else
            {
                try
                {

                    using (UserDialogs.Instance.Loading("Loading.."))
                    {
                        AcceptorRejectAppointmentInputModel acceptorRejectAppointmentInputModel = new AcceptorRejectAppointmentInputModel();
                        acceptorRejectAppointmentInputModel.AppointmentId = UserId;
                        acceptorRejectAppointmentInputModel.Reason = DescriptionText.Trim();
                        acceptorRejectAppointmentInputModel.Status = AppointmentStatus.Approved;

                        var result = await App.PAServiceManager.AcceptorRejectAppointment(acceptorRejectAppointmentInputModel);
                        if (result.Status)
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Appointment canceled", "Ok");
                            await PopupNavigation.Instance.PopAsync();
                            await Application.Current.MainPage.Navigation.PopAsync();
                        }
                        else if (result == null || result.Status)
                        {
                            //ProfileData = CopyProfileData;
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }

                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
        }
    }
}