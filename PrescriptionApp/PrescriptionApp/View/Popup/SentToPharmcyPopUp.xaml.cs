﻿using Acr.UserDialogs;
using PrescriptionApp.Model;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Services;
using Xamarin.Essentials;

namespace PrescriptionApp.View.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SentToPharmcyPopUp : PopupPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        int _approveRejectVisible { get; set; }
        public int ApproveRejectVisible
        {
            get => _approveRejectVisible;
            set { _approveRejectVisible = value; SetPropertyChanged(nameof(ApproveRejectVisible)); }
        }

        SentToPharmcyInputModel _sentToPharmcyInput { get; set; } = new SentToPharmcyInputModel();

        public SentToPharmcyInputModel SentToPharmcyInput
        {
            get => _sentToPharmcyInput;
            set
            {
                _sentToPharmcyInput = value;
                SetPropertyChanged(nameof(SentToPharmcyInput));
            }
        }

        bool _isRejected { get; set; }
        public bool IsRejected
        {
            get => _isRejected;
            set
            {
                _isRejected = value;
                SetPropertyChanged(nameof(IsRejected));
            }
        }

        UpdatePrescriptionByPharmacyInputModel _updatePrescriptionByPharmacyInput { get; set; } = new UpdatePrescriptionByPharmacyInputModel();

        public UpdatePrescriptionByPharmacyInputModel UpdatePrescriptionByPharmacyInput
        {
            get => _updatePrescriptionByPharmacyInput;
            set
            {
                _updatePrescriptionByPharmacyInput = value;
                SetPropertyChanged(nameof(UpdatePrescriptionByPharmacyInput));
            }
        }


        public ICommand SendCommand => new Command(Send);
        public ICommand CancelCommand => new Command(Cancel);
        public ICommand SelectLocationCommand => new Command(SelectLocation);

        //public ICommand UpdateStatusCommand => new Command(UpdateStatus);

        //public ICommand RejectCommand => new Command(Reject);
        //public ICommand ApproveCommand => new Command(Approve);

        public SentToPharmcyPopUp()
        {
            InitializeComponent();
            BindingContext = this;
            AddressField.IsReadOnly = true;
        }

        //private void Approve()
        //{
        //    ApproveRejectVisible = (int)PrescriptionApproval.SentToPharmacy;
        //    UpdatePrescriptionByPharmacyInput.Status = PrescriptionApproval.Accept;
        //}

        //private void Reject()
        //{
        //    ApproveRejectVisible = (int)PrescriptionApproval.SentToCustomer;
        //    UpdatePrescriptionByPharmacyInput.Status = PrescriptionApproval.Reject;
        //}

        async void Send()
        {
            if (UserType == UserType.Pharma)
            {
                if (ApproveRejectVisible == (int)PrescriptionApproval.SentToPharmacy)
                {
                    if (UpdatePrescriptionByPharmacyInput.Amount == 0)
                    {
                        await Application.Current.MainPage.DisplayAlert("Prezript Alert!", "Please enter Total Cost of Prescription", "OK");
                        return;
                    }
                    else
                    {
                        UpdatePrescriptionByPharmacyInput.Status = PrescriptionApproval.Accept;
                        UpdateStatus();
                    }
                }
                else if (ApproveRejectVisible == (int)PrescriptionApproval.SentToCustomer)
                {
                    if (string.IsNullOrEmpty(UpdatePrescriptionByPharmacyInput.Reason))
                    {
                        await Application.Current.MainPage.DisplayAlert("Prezript Alert!", "Please enter reason", "OK");
                        return;
                    }
                    else
                    {
                        UpdatePrescriptionByPharmacyInput.Status = PrescriptionApproval.Reject;
                        UpdateStatus();
                    }
                }
            }
            else if (UserType == UserType.Patient)
            {
                if (SentToPharmcyInput.SelectedOption == "Pick Up")
                {
                    DeleveryPickup();
                }
                else if (SentToPharmcyInput.SelectedOption == "Delivery")
                {
                    if (string.IsNullOrEmpty(SentToPharmcyInput.Address))
                    {
                        await Application.Current.MainPage.DisplayAlert("Prezript Alert!", "Please enter delivery address", "Ok");
                        return;
                    }
                    else
                    {
                        DeleveryPickup();
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert!", "Please select delivery Type", "Ok");
                    return;
                }
            }

        }

        public async void DeleveryPickup()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var result = await App.PAServiceManager.SentPrescriptionToPharmacy(SentToPharmcyInput);
                    if (result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Success", "Prescription Sent Successfully", "Ok");
                        await PopupNavigation.Instance.PopAsync();
                        await Application.Current.MainPage.Navigation.PopAsync();
                    }
                    else if (!result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }

                }

            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
            }
        }

        public async void UpdateStatus()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {

                    var result = await App.PAServiceManager.PrescriptionUpdatebyPharmacy(UpdatePrescriptionByPharmacyInput);

                    if (result.Status)
                    {
                        if (UpdatePrescriptionByPharmacyInput.Status == PrescriptionApproval.Accept)
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Prescription approved successfully.", "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Prescription rejected successfully", "Ok");
                        }

                        await PopupNavigation.Instance.PopAsync();
                        await Application.Current.MainPage.Navigation.PopAsync();
                        return;

                    }
                    else if (!result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        async void Cancel()
        {
            await PopupNavigation.Instance.PopAsync();
        }
        async void SelectLocation()
        {
            try
            {
                var action = await Application.Current.MainPage.DisplayActionSheet("Select an Option", "Cancel", null, "Auto", "Custom");

                if (!string.IsNullOrEmpty(action))
                {
                    if (action.Equals("Auto"))
                    {
                        var request = new GeolocationRequest(GeolocationAccuracy.Best);
                        var location = await Geolocation.GetLocationAsync(request);
                        GetAddress(location.Latitude, location.Longitude);
                        AddressField.IsReadOnly = false;
                        this.SentToPharmcyInput.Latitude = location.Latitude.ToString();
                        this.SentToPharmcyInput.Longitude = location.Longitude.ToString();
                    }
                    else if (action.Equals("Custom"))
                    {
                        var request = new GeolocationRequest(GeolocationAccuracy.Best);
                        var location = await Geolocation.GetLocationAsync(request);
                        var page = new MapPopup();
                        page.SelectionHandler += (object sender, double[] data) =>
                        {
                            GetAddress(data[0], data[1]);
                            this.SentToPharmcyInput.Latitude = data[0].ToString();
                            this.SentToPharmcyInput.Longitude = data[1].ToString();
                            AddressField.IsReadOnly = false;
                        };
                        page.lat = location.Latitude;
                        page.lng = location.Longitude;
                        await PopupNavigation.Instance.PushAsync(page);
                    } 
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", ex.Message.ToString(), "OK");
            }

        }

        async void GetAddress(double Latitude, double Longitude)
        {
            var placemarks = (await Geocoding.GetPlacemarksAsync(
                    Latitude, Longitude
                    ))?.FirstOrDefault();

            SentToPharmcyInput.Address = placemarks.FeatureName + ", " + placemarks.SubLocality + ", " + placemarks.Locality + ", " + placemarks.AdminArea + ", " + placemarks.CountryCode + ", " + placemarks.PostalCode;
        }
    }
}