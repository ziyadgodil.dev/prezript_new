﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials; 
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PhoneDialer : ContentPage
	{
		public PhoneDialer ()
		{
			InitializeComponent ();
		}


        private void Button_Clicked_1(object sender, EventArgs e)
        {
            try
            {

                try
                {
                  Xamarin.Essentials.PhoneDialer.Open(EntryNumber.Text);
                }
                catch (ArgumentNullException anEx)
                {
                    // Number was null or white space
                }
                catch (FeatureNotSupportedException ex)
                {
                    // Phone Dialer is not supported on this device.
                }
                catch (Exception ex)
                {
                    // Other error has occurred.
                }
            }
            catch (Exception)
            {
                App.Current.MainPage.DisplayAlert("sda", "ee", "fss");
                ;
            }
        }
    }
}