﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.View;
using PrescriptionApp.View.Popup;
using PrescriptionApp.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PrescriptionApp.View
{
    public class DashboardPageXaml : BaseContentPage<DashboardPageViewModel> {

       
    }

    public partial class DashboardPage : DashboardPageXaml
    {
        public DashboardPage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            FlotingButtonGrid.BackgroundColor = Color.Transparent;
            FlotingButtonGrid.InputTransparent = true;

            PluseImg.RotateTo(90);
            search.TranslateTo(0, 20);
            trophy.TranslateTo(0, 20);

            search.IsVisible = false;
            trophy.IsVisible = false;
            IsLeague = false;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            IsLeague = true;

        }

        private async void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            var page = new phone();
            await PopupNavigation.Instance.PushAsync(page);
        }

        public bool IsLeague { get; set; } = true;
        async void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            if (IsLeague)

            {
                FlotingButtonGrid.BackgroundColor = Color.FromHex("#40000000");
                FlotingButtonGrid.InputTransparent = false;

                search.IsVisible = !search.IsVisible;
                trophy.IsVisible = !trophy.IsVisible;
                PluseImg.RotateTo(40);
                search.TranslateTo(0, -20);
                await trophy.TranslateTo(0, -10);
                IsLeague = false;
            }
            else
            {
                FlotingButtonGrid.BackgroundColor = Color.Transparent;
                FlotingButtonGrid.InputTransparent = true;

                PluseImg.RotateTo(90);
                search.TranslateTo(0, 20);
                await trophy.TranslateTo(0, 20);

                search.IsVisible = !search.IsVisible;
                trophy.IsVisible = !trophy.IsVisible;
                IsLeague = true;

            }
        }

       async  private void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        {
            var page = new sms();
            await PopupNavigation.Instance.PushAsync(page);
        }
    }
}
