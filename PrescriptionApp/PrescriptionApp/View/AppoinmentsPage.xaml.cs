﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class AppoinmentsPageXaml : BaseContentPage<AppoinmentsPageViewModel> { };
    public partial class AppoinmentsPage : AppoinmentsPageXaml
    {
        public AppoinmentsPage()
        {
            InitializeComponent();
        }
        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnPropertyRaised(string propertyname)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        //}

        //ObservableCollection<AppoinmentsModel> _appoinmentsList { get; set; }
        //public ObservableCollection<AppoinmentsModel> AppoinmentsList
        //{
        //    get => _appoinmentsList;
        //    set { _appoinmentsList = value; OnPropertyRaised(nameof(AppoinmentsList)); }
        //}

        //public AppoinmentsPage()
        //{
            //InitializeComponent();
            //BindingContext = this;

            //AppoinmentsList = new ObservableCollection<AppoinmentsModel>
            //{
            //    new AppoinmentsModel()
            //    { 
            //        Image = "profile1",
            //        Date = DateTime.Now, 
            //        Name = "Dr. Jemes Roy", 
            //        Degree = "M.B.B.S", 
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },
            //    new AppoinmentsModel()
            //    {
            //        Image = "profile1",
            //        Date = DateTime.Now,
            //        Name = "Dr. Jemes Roy",
            //        Degree = "M.B.B.S",
            //        Address = "U/11, Yash Kamal Complex, Udhna Main Road, Udhna, Surat - 394210,Beside BJP Office,Opposite Jivan Jyot (Map)"
            //    },


            //};
        //}

        //private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        //{
        //    Navigation.PopAsync();
        //}
        //private void AppoinmentDetails_Tapped(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new AppoinmentDetailsPage());
        //}
    }
}