﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class PrescriptionViewPageXaml : BaseContentPage<PrescriptionViewPageViewModel> { };
    public partial class PrescriptionViewPage : PrescriptionViewPageXaml
    {
        public PrescriptionViewPage()
        {
            InitializeComponent();
        }
    }
}