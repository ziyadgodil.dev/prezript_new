﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class DoctorsPageXaml : BaseContentPage<DoctorsPageViewModel> { };
    public partial class DoctorsPage : DoctorsPageXaml 
    {
        public DoctorsPage()
        {
            InitializeComponent();
        }
    }
}