﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class DocorsDetailsPageXaml : BaseContentPage<DoctorsDetailsPageViewModel> { }

    public partial class DoctorsDetailsPage : DocorsDetailsPageXaml
    {
        public DoctorsDetailsPage()
        {
             
            InitializeComponent();
            //this.BindingContext = new RattingBarViewModal();
        }
        
    }
}