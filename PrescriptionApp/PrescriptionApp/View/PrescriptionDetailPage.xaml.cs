﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.OpenWhatsApp;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PrescriptionApp.Helper;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.View;
using PrescriptionApp.ViewModel;
using PrescriptionApp.Model;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Essentials;
using System.ComponentModel;

namespace PrescriptionApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public class PrescriptionDetailPageXaml : BaseContentPage<PrescriptionDetailPageViewModel> { };
    public partial class PrescriptionDetailPage : PrescriptionDetailPageXaml, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void SetPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        double Latitude = 0;
        double Longitude = 0;

        public PrescriptionDetailPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<LocationModel>(this, "LocationLoaded", (e) =>
            {
                Latitude = double.Parse(e.Latitude);
                Longitude = double.Parse(e.Longitude);

                if (e.UserType == Helper.Resources.UserType.Patient)
                    SetupMap(PharmaMapGrid);
                else if (e.UserType == Helper.Resources.UserType.Pharma)
                    SetupMap(UserMapGrid);

            });

           
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<LocationModel>(this, "LocationLoaded");
        }

        void SetupMap(Grid grid)
        {
            var map = new Xamarin.Forms.GoogleMaps.Map()
            {
                HeightRequest = 100,
                WidthRequest = 100,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HasZoomEnabled = false,
                InitialCameraUpdate = CameraUpdateFactory.NewCameraPosition(
                             new CameraPosition(new Position(Latitude, Longitude), 14d)),
            };

            map.UiSettings.ZoomControlsEnabled = false;
            map.UiSettings.ScrollGesturesEnabled = false;
            map.UiSettings.MapToolbarEnabled = false;
            map.UiSettings.MyLocationButtonEnabled = false;
            map.UiSettings.RotateGesturesEnabled = false;
            map.UiSettings.TiltGesturesEnabled = false;
            map.UiSettings.ZoomGesturesEnabled = false;
            var pin = new Pin()
            {
                Type = PinType.Place,
                Label = "My Location",
                Position = new Position(Latitude, Longitude),
                Tag = "my_location",
            };
            map.Pins.Clear();
            map.Pins.Add(pin);
            var box = new BoxView()
            {
                BackgroundColor = Color.Transparent,
                Margin = 0
            };
            grid.Children.Add(map, 2, 0);
            grid.Children.Add(box, 2, 0);

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, ex) =>
            {
                var location = new Location(Latitude, Longitude);
                Xamarin.Essentials.Map.OpenAsync(location);
            };
            box.GestureRecognizers.Add(tapGestureRecognizer);
        }

       
    }
}