﻿using Prescription.ViewModel;
using PrescriptionApp.Helper.Base;
using Xamarin.Forms.Xaml;

namespace Prescription.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public class OnBoardingXaml : BaseContentPage<OnBoardingViewModel> { }

    public partial class OnBoarding : OnBoardingXaml
    {
		public OnBoarding ()
		{
			InitializeComponent ();
		}
	}
}