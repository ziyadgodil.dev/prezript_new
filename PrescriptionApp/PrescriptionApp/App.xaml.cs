﻿using System;
using Plugin.FirebasePushNotification;
using Prescription.View;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp
{
    public partial class App : Application
    {
        public static RestService PAServiceManager { get; private set; }
        public static bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;

        public App()
        {
            InitializeComponent();

            UserAppTheme = OSAppTheme.Light;
            PAServiceManager = new RestService();

            if (Device.RuntimePlatform.ToLower() == "android")
            {
                Settings.DeviceType = "Android";
            }
            else
            {
                Settings.DeviceType = "iOS";
            }

            if (Settings.IsFirstTime)
            {
                MainPage = new NavigationPage(new OnBoarding());
            }
            else
            {
                //if (Settings.UserLoggedIn)
                //    MainPage = new NavigationPage(new DashboardPage());
                //else
                //    MainPage = new NavigationPage(new SignInPage());

                if (Settings.UserLoggedIn)
                    Application.Current.MainPage = new NavigationPage(new SidebarMenu());
                else
                    MainPage = new NavigationPage(new SignInPage());
            }

            //MainPage = new NavigationPage(new OnBoarding());

            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                if (!Settings.UserLoggedIn)
                {
                    return;
                }
                if (p.Data.ContainsKey("Page"))
                {
                    //string page = p.Data["Page"].ToString();

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Current.MainPage.Navigation.PushAsync(new NotificationPage());
                        //switch (page)
                        //{
                        //    case "Appointment":
                        //        Current.MainPage.Navigation.PushAsync(new AppoinmentsPage());
                        //        break;
                        //    case "Prescription":
                        //        Current.MainPage.Navigation.PushAsync(new PrescriptionPage());
                        //        break;
                        //    default:
                        //        Current.MainPage.Navigation.PushAsync(new NotificationPage());
                        //        break;
                        //}
                    });
                }
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

