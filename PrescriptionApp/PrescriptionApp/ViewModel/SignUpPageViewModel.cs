﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Linq;
using Rg.Plugins.Popup.Services;
using PrescriptionApp.View.Popup;

namespace PrescriptionApp.ViewModel
{
    public class SignUpPageViewModel : BaseViewModel
    {

        ObservableCollection<TypeModel> _usertypeList { get; set; }
        public ObservableCollection<TypeModel> UserTypeList
        {
            get => _usertypeList;
            set
            {
                _usertypeList = value;
                SetPropertyChanged(nameof(UserTypeList));
            }
        }

        string _address { get; set; }
        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                SetPropertyChanged(nameof(Address));
            }
        }
        bool _isEditable { get; set; }
        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                _isEditable = value;
                SetPropertyChanged(nameof(IsEditable));
            }
        }

        TypeModel _selectedUserType { get; set; }
        public TypeModel SelectedUserType
        {
            get => _selectedUserType;
            set
            {
                _selectedUserType = value;
                SetPropertyChanged(nameof(SelectedUserType));
            }
        }


        public ICommand SelectLocationCommand => new Command(SelectLocation);

        public ICommand SignUpCommand { get; set; }
        public ICommand BackCommand { get; set; }
        RegistrationInputModel _registrationInput { get; set; } = new RegistrationInputModel();
        public RegistrationInputModel RegistrationInput
        {
            get { return _registrationInput; }
            set { _registrationInput = value; SetPropertyChanged(nameof(RegistrationInput)); }
        }
        public SignUpPageViewModel()
        {
            IsEditable = true;
            SignUpCommand = new Command(SignUp);
            BackCommand = new Command(Back);
            UserTypeList = new ObservableCollection<TypeModel>()
            {
                new TypeModel()
                {
                    Key=1,
                    UserType="Patient"

                },
                new TypeModel()
                {
                   Key=2,
                    UserType="Specialist"
                },
                new TypeModel()
                {
                    Key=3,
                    UserType="Pharmacist"
                },
            };
        }
        const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        const string passwordRegex = @"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$";

        public async void SignUp()
        {
            if (string.IsNullOrEmpty(RegistrationInput.Email)
                || string.IsNullOrEmpty(RegistrationInput.Contact)
                || string.IsNullOrEmpty(RegistrationInput.Password) || SelectedUserType == null)
            {
                await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please fill all information.", "Ok");
                return;
            }
            else if (!Regex.IsMatch(RegistrationInput.Email, emailRegex))
            {
                await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please enter valid email.", "Ok");
                return;
            }
            else if (!Regex.IsMatch(RegistrationInput.Contact, "\\A[0-9]{10}\\z"))
            {
                await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Mobile number is not valid.", "Ok");
                return;
            }
            else if (!Regex.IsMatch(RegistrationInput.Password, passwordRegex))
            {
                await Application.Current.MainPage.DisplayAlert("Password Policy", "Please enter Strong Password like One UpperCase, Special Charactor and Eight charactor long....", "Ok");
                return;
            }
            else if (string.IsNullOrEmpty(RegistrationInput.Address))
            {
                await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please enter Address", "Ok");
                return;
            }
            try
            {
                if (App.IsConnected)
                {
                    using (UserDialogs.Instance.Loading("Loading.."))
                    {
                        RegistrationInput.UserTypeId = SelectedUserType.Key;
                        var result = await App.PAServiceManager.Registration(RegistrationInput);

                        if (result != null && result.Status)
                        {
                            await Application.Current.MainPage.DisplayAlert("Success", "Registered Successfully", "Ok");
                            await Application.Current.MainPage.Navigation.PushAsync(new SignInPage());
                        }
                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                        }
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");

            }
        }
        async void Back()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new SignInPage());
        }

        async void SelectLocation()
        {

            var action = await Application.Current.MainPage.DisplayActionSheet("Select an Option", "Cancel", null, "Auto", "Custom");

            if (!string.IsNullOrEmpty(action))
            {
                if (action.Equals("Auto"))
                {
                    var request = new GeolocationRequest(GeolocationAccuracy.Best);
                    var location = await Geolocation.GetLocationAsync(request);
                    GetAddress(location.Latitude, location.Longitude);
                    IsEditable = false;
                    this.RegistrationInput.Latitude = location.Latitude.ToString();
                    this.RegistrationInput.Longitude = location.Longitude.ToString();
                }
                else if (action.Equals("Custom"))
                {
                    var request = new GeolocationRequest(GeolocationAccuracy.Best);
                    var location = await Geolocation.GetLocationAsync(request);
                    var page = new MapPopup();
                    page.SelectionHandler += (object sender, double[] data) =>
                    {
                        GetAddress(data[0], data[1]);
                        this.RegistrationInput.Latitude = data[0].ToString();
                        this.RegistrationInput.Longitude = data[1].ToString();
                        IsEditable = false;
                    };
                    page.lat = location.Latitude;
                    page.lng = location.Longitude;
                    await PopupNavigation.Instance.PushAsync(page);
                }
            }
        }

        async void GetAddress(double Latitude, double Longitude)
        {
            var placemarks = (await Geocoding.GetPlacemarksAsync(
                    Latitude, Longitude
                    ))?.FirstOrDefault();

            RegistrationInput.Address = placemarks.FeatureName + ", " + placemarks.SubLocality + ", " + placemarks.Locality + ", " + placemarks.AdminArea + ", " + placemarks.CountryCode + ", " + placemarks.PostalCode;
        }
    }
}
