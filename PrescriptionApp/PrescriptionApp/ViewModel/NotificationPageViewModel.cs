﻿
using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Design;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace PrescriptionApp.ViewModel
{
    public class NotificationPageViewModel : BaseViewModel
    {
        ObservableCollection<NotificationModel> _NotificationList { get; set; }
        public ObservableCollection<NotificationModel> NotificationList
        {
            get => _NotificationList;
            set
            {
                _NotificationList = value;
                SetPropertyChanged(nameof(NotificationList));

            }
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();


        }
        public ICommand BackCommand => new Command(Back);
        public ICommand NotificationDetailsCommand => new Command<NotificationModel>(NotificationDetails);

        public NotificationPageViewModel()
        {

        }
        public async void LoadData() //get Notification
        {
            try
            {
                
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetNotification();

                        if (result != null && result.Data != null)
                        {
                            NotificationList = result.Data;

                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void Back()
        {
            Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void NotificationDetails(NotificationModel notificationModel)
        {
            using (UserDialogs.Instance.Loading("Loading.."))
            {
                var result = await App.PAServiceManager.ReadWriteNotification(new ReadWriteNotificationInputModel() { NotificationId = notificationModel.MessageId });
                if (result != null && result.Status)
                {
                    notificationModel.Status = true;
                }
                if (notificationModel.NotificationType == NotificationTypes.Appointment)
                {
                    if (IsBusy)
                    {
                        return;
                    }
                    else
                    {
                        IsBusy = true;
                    }
                    var page = new AppoinmentDetailsPage();
                    page.ViewModel.AppointmentUserId = (int)notificationModel.NotificationTypeId;
                    await App.Current.MainPage.Navigation.PushAsync(page);
                    IsBusy = false;
                }
                else if (notificationModel.NotificationType == NotificationTypes.Prescription)
                {
                    if (IsBusy)
                    {
                        return;
                    }
                    else
                    {
                        IsBusy = true;
                    }
                    var page = new PrescriptionDetailPage();
                    page.ViewModel.PrecriptionId = (int)notificationModel.NotificationTypeId;
                    await App.Current.MainPage.Navigation.PushAsync(page);
                    IsBusy = false;
                }
                else
                    return;
            }


        }
    }
}
