﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class PharmaciesPageViewModel : BaseViewModel
    {
        public event EventHandler<PharmaciesModel> SelectionHandler;

        ObservableCollection<PharmaciesModel> AllPharmacies { get; set; }

        ObservableCollection<PharmaciesModel> _listBPharmacies { get; set; }
        public ObservableCollection<PharmaciesModel> ListPharmacies
        {
            get => _listBPharmacies;
            set
            {
                _listBPharmacies = value;
                SetPropertyChanged(nameof(ListPharmacies));
            }
        }
        public bool Selection { get; set; }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }

        public ICommand BackCmmand => new Command(Back);
        public ICommand PharmacyDetailCommand => new Command<PharmaciesModel>(PharmacyDetail);

        public PharmaciesPageViewModel()
        {

        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        public async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var result = await App.PAServiceManager.GetAllPharmacy();

                    if (result != null && result.Data != null)
                    {
                        ListPharmacies = result.Data;
                        AllPharmacies = result.Data;
                    }
                    else if (result == null)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public async void PharmacyDetail(PharmaciesModel pharmaciesModel)
        {
            if (Selection)
            {
                await Application.Current.MainPage.Navigation.PopAsync();
                SelectionHandler?.Invoke(null, pharmaciesModel);
            }
            else
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new UserProfilePage();
                //page.ViewModel.UserId = pharmaciesModel.PharmacyId;
                page.ViewModel.PharmaId = pharmaciesModel.PharmacyId;
                page.ViewModel.UserProfileTitle = "Profile";
                await Application.Current.MainPage.Navigation.PushAsync(page);
                IsBusy = false;
            }
        }

        void Search()
        {
            if (string.IsNullOrEmpty(SearchText))
            {
                ListPharmacies = AllPharmacies;
            }
            else
            {
                ListPharmacies = new ObservableCollection<PharmaciesModel>(AllPharmacies.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            }
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

    }
}
