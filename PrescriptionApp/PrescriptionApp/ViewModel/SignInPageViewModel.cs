﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace PrescriptionApp.ViewModel
{
    public class SignInPageViewModel : BaseViewModel
    {
        LoginInputModel _loginInput { get; set; } = new LoginInputModel();
        public LoginInputModel LoginInput
        {
            get => _loginInput;
            set { _loginInput = value; SetPropertyChanged(nameof(LoginInput)); }
        }

        public ICommand SelectionCommand { get; }
        public ICommand LoginCommand => new Command(DoLogin);
        public ICommand SignUpCommand => new Command(SignUp);
        public ICommand ForgotCommand => new Command(Forgot);

        public SignInPageViewModel()
        {

        }

        public async void DoLogin()
        {
            if (string.IsNullOrEmpty(LoginInput.MobileNo) )
            {
                await Application.Current.MainPage.DisplayAlert("Alert", "Please Enter Username", "Ok");
                return;
            }
            else if (string.IsNullOrEmpty(LoginInput.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Alert", "Please Enter Password", "Ok");
                return;
            }
            else if (!Regex.IsMatch(LoginInput.MobileNo, "\\A[0-9]{10}\\z"))
            {
                await Application.Current.MainPage.DisplayAlert("Alert", "Please Enter valid phone number", "Ok");
                return;
            }
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        LoginResponseModel result = await App.PAServiceManager.Login(LoginInput);

                        if (result != null && result.Status && result.DataDetail != null)
                        {
                            Settings.UserType = result.DataDetail.UserTypeId;
                            Settings.UserId = result.DataDetail.UserId;
                            Settings.FirstName = result.DataDetail.FirstName;
                            Settings.LastName = result.DataDetail.LastName;
                            Settings.Email = result.DataDetail.Email;
                            Settings.MobileNo = result.DataDetail.ContactNo;
                            Settings.UserImage = result.DataDetail.ImageUrl;
                            Settings.Latitude = result.DataDetail.Latitude;
                            Settings.Longitude = result.DataDetail.Longitude;
                            Settings.Address = result.DataDetail.Address;
                            Settings.NewLogin = true;

                            if (!result.DataDetail.ProfileUpdated)
                            {
                                await Application.Current.MainPage.Navigation.PushAsync(new ProfilePage());
                            }
                            else
                            {
                                Settings.UserLoggedIn = true;
                                Application.Current.MainPage = new NavigationPage(new SidebarMenu());
                            }
                        }
                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
            }
        }

        public async void SignUp()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new SignUpPage());
            IsBusy = false;
        }
        public async void Forgot()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new ForgotPage());
            IsBusy = false;

        }
    }
}
