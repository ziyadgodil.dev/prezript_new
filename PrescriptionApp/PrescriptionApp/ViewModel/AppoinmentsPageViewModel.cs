﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{

    public class AppoinmentsPageViewModel : BaseViewModel
    {
        ObservableCollection<AppoinmentsModel> UpcomingList { get; set; }

        ObservableCollection<AppoinmentsModel> PastList { get; set; }

        ObservableCollection<AppoinmentsModel> _appoinmentsList { get; set; }
        public ObservableCollection<AppoinmentsModel> AppoinmentsList
        {
            get => _appoinmentsList;
            set { _appoinmentsList = value; SetPropertyChanged(nameof(AppoinmentsList)); }
        }
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }

        int _selectedTab { get; set; } = 1;
        public int SelectedTab
        {
            get => _selectedTab;
            set { _selectedTab = value; SetPropertyChanged(nameof(SelectedTab)); }
        }


        public ICommand BackCommand => new Command(Back);

        public ICommand AddCommand => new Command(Add);
        public ICommand AppoinmentDetailsCommand => new Command<AppoinmentsModel>(AppoinmentDetails);

        public ICommand ChangeAppoitmentTab => new Command<string>(AppoitmentTypeChanged);

        public AppoinmentsPageViewModel()
        {

        }


        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
            Search();
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopToRootAsync();
        }

        public async void AppoinmentDetails(AppoinmentsModel appoinmentsModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new AppoinmentDetailsPage();
            page.ViewModel.AppointmentUserId = appoinmentsModel.UserId;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        public async void Add()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new AddAppoinmentPage());
            IsBusy = false;
        }
        public async void LoadData() //get appoinmentdetails
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetAppoinmentsDetails();

                        if (result != null && result.Data != null)
                        {


                            UpcomingList = result.Data.Upcoming;

                            PastList = result.Data.Past;

                            if (SelectedTab == 1)
                                AppoinmentsList = UpcomingList;
                            else
                                AppoinmentsList = PastList;


                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        void Search()
        {
            if (SelectedTab == 1)
            {
                if (string.IsNullOrEmpty(SearchText))
                    AppoinmentsList = UpcomingList;
                else
                    AppoinmentsList = new ObservableCollection<AppoinmentsModel>(UpcomingList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            }
            else if (SelectedTab == 2)
            {
                if (string.IsNullOrEmpty(SearchText))
                    AppoinmentsList = PastList;
                else
                    AppoinmentsList = new ObservableCollection<AppoinmentsModel>(PastList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            }

        }

        public void AppoitmentTypeChanged(string Type)
        {
            SelectedTab = Convert.ToInt32(Type);

            if (SelectedTab == 1)
            {
                AppoinmentsList = UpcomingList;
            }
            else if (SelectedTab == 2)
            {
                AppoinmentsList = PastList;
            }

        }
    }
}
