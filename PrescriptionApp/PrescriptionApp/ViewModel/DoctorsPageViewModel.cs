﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class DoctorsPageViewModel : BaseViewModel
    {
        ObservableCollection<DoctorsModel> _doctorsList { get; set; }
        public ObservableCollection<DoctorsModel> DoctorsList
        {
            get => _doctorsList;
            set { _doctorsList = value; SetPropertyChanged(nameof(DoctorsList)); }
        }

        ObservableCollection<DoctorsModel> _allDoctorsList { get; set; }
        public ObservableCollection<DoctorsModel> AllDoctorsList
        {
            get => _allDoctorsList;
            set { _allDoctorsList = value; SetPropertyChanged(nameof(AllDoctorsList)); }
        }

        public bool Selection { get; set; }

        string _searchText { get; set; }

        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                search();
            }
        }
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        int _userId { get; set; }
        public int UserID
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
                SetPropertyChanged(nameof(UserID));
            }
        }
        string _image { get; set; }
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); }
        }
         

        string _userTname { get; set; }
        public string UserTName
        {
            get => _userTname;
            set { _userTname = value; SetPropertyChanged(nameof(UserTName)); }
        }

        string _monoGram { get; set; }
        public string MonoGram
        {
            get => _monoGram;
            set { _monoGram = value; SetPropertyChanged(nameof(MonoGram)); }
        }
        public ICommand BackCommand { get; set; }
        public ICommand DoctorsDetailsCommand { get; set; }
        public ICommand AppointmentCommand => new Command(AppointmentDetails);
        public override void OnAppearing()
        {
            base.OnAppearing();
          
           
           
            MonoGram = Settings.FirstName.ToCharArray()[0].ToString().ToUpper() + Settings.LastName.ToCharArray()[0].ToString().ToUpper();
            LoadData(); //get doctors details
        }
        public DoctorsPageViewModel()
        {
            BackCommand = new Command(Back);
            DoctorsDetailsCommand = new Command<DoctorsModel>(DoctorsDetails);
        }

        public async void AppointmentDetails()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new AppoinmentsPage());
        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void DoctorsDetails(DoctorsModel doctorsModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            if (Selection)
            {
                var prescription = new AddPrescriptionPage();
                prescription.ViewModel.DoctorDetails = doctorsModel;
                prescription.ViewModel.IsSelectDocVisible = false;
                await Application.Current.MainPage.Navigation.PushAsync(prescription);
            }
            else
            {
                var page = new UserProfilePage();
                if (UserType == UserType.Doctor)
                {
                    page.ViewModel.DoctorId = doctorsModel.UserId;
                    page.ViewModel.UserProfileTitle = "Profile";
                }
                else if(UserType==UserType.Patient)
                {
                    page.ViewModel.UserId = doctorsModel.UserId;
                    page.ViewModel.DoctorId = doctorsModel.UserId;
                    page.ViewModel.UserProfileTitle = "Specialist Profile";
                }
                await Application.Current.MainPage.Navigation.PushAsync(page);
            }
            IsBusy = false;
        }
        public async void LoadData(string filterText = "") //get doctors details
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {

                        var result = await App.PAServiceManager.GetAllDoctors();

                        if (result != null && result.Data != null)
                        {

                            DoctorsList = result.Data;
                            AllDoctorsList = DoctorsList;
                            search();
                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        void search()
        {
            if (string.IsNullOrEmpty(SearchText))
            {
                DoctorsList = AllDoctorsList;
            }
            else
            {
                DoctorsList = new ObservableCollection<DoctorsModel>(AllDoctorsList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            }
        }
    }
}
