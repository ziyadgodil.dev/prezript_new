﻿using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using PrescriptionApp.View.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class ProfilePageViewModel : BaseViewModel
    {
        private MediaFile file;

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        bool _isEditMode { get; set; }
        public bool IsEditMode
        {
            get => _isEditMode;
            set
            {
                _isEditMode = value;
                SetPropertyChanged(nameof(IsEditMode));
                SetPropertyChanged(nameof(IsEdit));
            }
        }

        public string IsEdit
        {
            get => IsEditMode ? "rightedit" : "pencilWhite";
        }

        UserProfileModel CopyProfileData { get; set; }
        UserProfileModel _profileDetail { get; set; }
        public UserProfileModel ProfileDetail
        {
            get => _profileDetail;
            set { _profileDetail = value; SetPropertyChanged(nameof(ProfileDetail)); }
        }
        UserProfileModel TempProfileData { get; set; }
        TimeSpan _openingTime { get; set; } = new TimeSpan(9, 0, 0);
        public TimeSpan OpeningTime
        {
            get => _openingTime;
            set { _openingTime = value; SetPropertyChanged(nameof(OpeningTime)); }
        }

        TimeSpan _closingTime { get; set; } = new TimeSpan(5, 0, 0);
        public TimeSpan ClosingTime
        {
            get => _closingTime;
            set { _closingTime = value; SetPropertyChanged(nameof(ClosingTime)); }
        }


        ObservableCollection<SpecialtyModel> _specialtyList { get; set; }
        public ObservableCollection<SpecialtyModel> SpecialtyList
        {
            get => _specialtyList;
            set { _specialtyList = value; SetPropertyChanged(nameof(SpecialtyList)); }
        }

        public ICommand BackCommand => new Command(Back);
        public ICommand EditCommand => new Command(Edit);
        public ICommand UploadProfileImageCommand => new Command(UploadProfileImage);
        public ICommand ChangePasswordCommand => new Command(PasswordChange);
        public ICommand LogOutCommand => new Command(LogOut);
        public ICommand SelectLocationCommand => new Command(SelectLocation);

        public ProfilePageViewModel()
        {

        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        async void LogOut()
        {
            var Confirm = await Application.Current.MainPage.DisplayAlert("Alert!", "Are you want to logout?", "Yes", "Cancel");
            if (Confirm)
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {

                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.Logout();

                        if (result != null && result.Status)
                        {
                            Application.Current.MainPage = new NavigationPage(new SignInPage());
                            Settings.UserLoggedIn = false;
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
        }

        public void Edit()
        {
            if (IsEditMode)
                Save();
            else
                IsEditMode = true;
        }


        private  void PasswordChange()
        {
            //await PopupNavigation.Instance.PushAsync(new ChangePassword());
        }

        public void UploadProfileImage()
        {
            EditImage();
        }

        public async void LoadData()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading..");
                var resultspecialty = await App.PAServiceManager.GetAllSpeciality();
                if (resultspecialty != null && resultspecialty.Data != null)
                {
                    SpecialtyList = resultspecialty.Data.Speciality;
                    SpecialtyList.RemoveAt(0);
                }
                else if (resultspecialty == null)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", resultspecialty.Message, "Ok");
                }

                var result = await App.PAServiceManager.GetProfileDetail();

                if (result != null && result.Data != null)
                {
                    ProfileDetail = result.Data;
                    CopyProfileData = result.Data;
                    if (result.Data.Speciality != null)
                        ProfileDetail.Speciality = SpecialtyList.FirstOrDefault(x => x.SpecialityId == result.Data.Speciality.SpecialityId);
                }
                else if (result == null)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                }

                UserDialogs.Instance.HideLoading();
            }

            catch (Exception ex)
            {
                ex.ToString();
                UserDialogs.Instance.HideLoading();
            }
        }


        const string EmailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        const string DegreeRegex = @"^[a-zA-Z,]+$";

        public async void Save()
        {
            try
            {
                if (string.IsNullOrEmpty(ProfileDetail.FirstName))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter your first name.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.LastName))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter your last name.", "Ok");
                    return;
                }
                else if ((string.IsNullOrEmpty(ProfileDetail.Degree) && (UserType == UserType.Doctor)))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter your degree.", "Ok");
                    return;
                }
                else if (ProfileDetail.Speciality == null && (UserType == UserType.Doctor))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter your speciality.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.UserAddress))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter address.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.Email))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter email.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.Contact) && ProfileDetail.Contact.Length != 10)
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter your Mobile number.", "Ok");
                    return;
                }

                else if (string.IsNullOrEmpty(ProfileDetail.NationalId) && (UserType == UserType.Patient || UserType == UserType.Doctor))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter Natinal id.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.BusinessRegistrationNumber) && (UserType == UserType.Doctor || UserType == UserType.Pharma))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter BusinessRegistrationNumber.", "Ok");
                    return;
                }

                else if (OpeningTime == null && (UserType == UserType.Doctor || UserType == UserType.Pharma))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter Opening time.", "Ok");
                    return;
                }
                else if (ClosingTime == null && (UserType == UserType.Doctor || UserType == UserType.Pharma))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter Closing time.", "Ok");
                    return;
                }
                else if (string.IsNullOrEmpty(ProfileDetail.AboutMe))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter about.", "Ok");
                    return;
                }

                if (UserType == UserType.Doctor)
                {
                    if (!Regex.IsMatch(ProfileDetail.Degree, DegreeRegex))
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Please enter valid Degree.", "Ok");
                        return;
                    }
                }
                if (!Regex.IsMatch(ProfileDetail.Email, EmailRegex))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter valid email.", "Ok");
                    return;
                }
                if (!Regex.IsMatch(ProfileDetail.Contact, "\\A[0-9]{10}\\z"))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please enter valid Mobile number.", "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(ProfileDetail.Latitude))
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please enter Latitude", "Ok");
                    return;
                } 
                if (string.IsNullOrEmpty(ProfileDetail.Longitude))
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please enter Longitude", "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(ProfileDetail.UserAddress))
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please enter Address", "Ok");
                    return;
                }

                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    var result = await App.PAServiceManager.UpdateProfile(ProfileDetail);
                    if (result.Status && result.Data != null)
                    {
                        IsEditMode = false;
                        Settings.FirstName = result.Data.FirstName;
                        Settings.LastName = result.Data.LastName;
                        await Application.Current.MainPage.DisplayAlert("Success", "Profile Updated Successfully", "Ok");

                        Settings.UserLoggedIn = true;
                        if (((NavigationPage)Application.Current.MainPage).RootPage is SignInPage)
                            Application.Current.MainPage = new NavigationPage(new DashboardPage());
                    }
                    else
                    {
                        ProfileDetail = CopyProfileData;
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public async void EditImage()
        {
            TempProfileData = ProfileDetail;
            string Base64 = null;
            try
            {
                string action = await Application.Current.MainPage.DisplayActionSheet("", "Cancel", null, "Take Photo", "Choose Photo");

                if (action == "Take Photo")
                {
                    await CrossMedia.Current.Initialize();

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await Application.Current.MainPage.DisplayAlert("No Camera", "No camera avaialble.", "OK");
                        return;
                    }

                    try
                    {
                        var cameraStatus = await Permissions.CheckStatusAsync<Permissions.Camera>();
                        var storageStatus = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();
                        if (cameraStatus != PermissionStatus.Granted)
                        {
                            cameraStatus = await Permissions.RequestAsync<Permissions.Camera>();
                        }
                        if (storageStatus != PermissionStatus.Granted)
                        {
                            storageStatus = await Permissions.RequestAsync<Permissions.StorageWrite>();
                        }
                        if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                        {
                            file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                            {
                                Directory = "Sample",
                                Name = "test.jpg",
                                DefaultCamera = CameraDevice.Front,
                                PhotoSize = PhotoSize.Full,
                                CompressionQuality = 10,
                                AllowCropping = true
                            });

                            if (file == null)
                                return;

                            var stream = file.GetStream();
                            byte[] filebytearray = new byte[stream.Length];
                            stream.Read(filebytearray, 0, (int)stream.Length);
                            Base64 = Convert.ToBase64String(filebytearray);
                        }
                        else
                        {
                           // await Application.Current.MainPage.DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
                        }
                    }
                    catch (Exception error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert!", error.Message.ToString(), "OK");
                    }
                }

                else if (action == "Choose Photo")
                {
                    await CrossMedia.Current.Initialize(); 
                    try
                    {
                        var storageStatus = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
                        if (storageStatus != PermissionStatus.Granted)
                        {
                            storageStatus = await Permissions.RequestAsync<Permissions.StorageRead>();
                        }
                        if (storageStatus == PermissionStatus.Granted)
                        {
                            file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                            {
                                PhotoSize = PhotoSize.Full,
                                CompressionQuality = 10
                            });


                            if (file == null)
                                return;

                            var stream = file.GetStream();
                            byte[] filebytearray = new byte[stream.Length];
                            stream.Read(filebytearray, 0, (int)stream.Length);
                            Base64 = Convert.ToBase64String(filebytearray);

                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Permissions Denied", "Unable to Pick photos.", "OK");
                        }
                    }
                    catch (Exception error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert!", error.Message.ToString(), "OK");

                    }

                }

                if (!string.IsNullOrEmpty(Base64))
                {
                    using (UserDialogs.Instance.Loading("Saving..."))
                    {

                        UpdateImageInputModel updateImageInputModel = new UpdateImageInputModel
                        {
                            //Image=file.Path,
                            Image = Base64,
                            ImageName = file.Path

                        };
                        var res = await App.PAServiceManager.UpdateImage(updateImageInputModel);

                        if (res != null && res.Status)
                        {
                            ProfileDetail.Image = res.Data;
                            Settings.UserImage = res.Data;
                            MessagingCenter.Send(new MessagingCenterModel { }, "ProfileImageUpdate");
                        }
                        else if (!res.Status)
                        {
                            //await Application.Current.MainPage.DisplayAlert("Alert", res.msg, "ok");
                            await Application.Current.MainPage.DisplayAlert("Alert", "Not Updated", "OK");
                        }
                        ProfileDetail = TempProfileData;
                        ProfileDetail.Image = res.Data;

                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Alert!", "Invalid Image", "OK");
                }

            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", ex.Message.ToString(), "OK");
            }
        }

        async void SelectLocation()
        {
            try
            {
                var action = await Application.Current.MainPage.DisplayActionSheet("Select an Option", "Cancel", null, "Auto", "Custom");

                if (!string.IsNullOrEmpty(action))
                {
                    if (action.Equals("Auto"))
                    {
                        var request = new GeolocationRequest(GeolocationAccuracy.Best);
                        var location = await Geolocation.GetLocationAsync(request);
                        GetAddress(location.Latitude, location.Longitude);
                        this.ProfileDetail.Latitude = location.Latitude.ToString();
                        this.ProfileDetail.Longitude = location.Longitude.ToString();
                    }
                    else if (action.Equals("Custom"))
                    {
                        var request = new GeolocationRequest(GeolocationAccuracy.Best);
                        var location = await Geolocation.GetLocationAsync(request);
                        var page = new MapPopup();
                        page.SelectionHandler += (object sender, double[] data) =>
                        {
                            GetAddress(data[0], data[1]);
                            this.ProfileDetail.Latitude = data[0].ToString();
                            this.ProfileDetail.Longitude = data[1].ToString();
                        };
                        page.lat = location.Latitude;
                        page.lng = location.Longitude;
                        await PopupNavigation.Instance.PushAsync(page);
                    } 
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Alert!", ex.Message.ToString(), "OK");
            }
        }

        async void GetAddress(double Latitude, double Longitude)
        {
            var placemarks = (await Geocoding.GetPlacemarksAsync(
                    Latitude, Longitude
                    ))?.FirstOrDefault();

            ProfileDetail.UserAddress = placemarks.FeatureName + ", " + placemarks.SubLocality + ", " + placemarks.Locality + ", " + placemarks.AdminArea + ", " + placemarks.CountryCode + ", " + placemarks.PostalCode;
        }

    }
}
