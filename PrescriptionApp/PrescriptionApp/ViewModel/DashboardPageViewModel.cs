﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class DashboardPageViewModel : BaseViewModel
    {
        public class OnBoardingModel : BaseModel
        {
            ImageSource _carosuelImage { get; set; }
            public ImageSource CarosuelImage
            {
                get => _carosuelImage;
                set
                {
                    _carosuelImage = value;
                    SetPropertyChanged(nameof(CarosuelImage));
                }
            }
            public string CarosuelTitle { get; set; }

            public string CarosuelDescription { get; set; }

            public string buttonNext { get; set; }

            public string Skip { get; set; }

        }
 

        ObservableCollection<OnBoardingModel> _onBoardingsData { get; set; } = new ObservableCollection<OnBoardingModel>();

        public ObservableCollection<OnBoardingModel> OnBoardingsData
        {
            get => _onBoardingsData;
            set
            {
                _onBoardingsData = value;
                SetPropertyChanged(nameof(OnBoardingsData));
            }
        }

    
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        string _image { get; set; }
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); }
        }
        bool _deviceidUpdated { get; set; } = false;
        public bool DeviceidUpdated
        {
            get => _deviceidUpdated;
            set { _deviceidUpdated = value; SetPropertyChanged(nameof(DeviceidUpdated)); }
        }

        string _username { get; set; }
        public string UserName
        {
            get => _username;
            set { _username = value; SetPropertyChanged(nameof(UserName)); }
        }

        string _userTname { get; set; }
        public string UserTName
        {
            get => _userTname;
            set { _userTname = value; SetPropertyChanged(nameof(UserTName)); }
        }

        string _monoGram { get; set; }
        public string MonoGram
        {
            get => _monoGram;
            set { _monoGram = value; SetPropertyChanged(nameof(MonoGram)); }
        }
        NotificationCountModel _notificationCount { get; set; }
        public NotificationCountModel NotificationCount
        {
            get => _notificationCount;
            set
            {
                _notificationCount = value;
                SetPropertyChanged(nameof(NotificationCount));
            }
        }


        bool _isvisiblePCount { get; set; } = false;
        public bool IsvisiblePCount
        {
            get => _isvisiblePCount;
            set
            {
                _isvisiblePCount = value;
                SetPropertyChanged(nameof(IsvisiblePCount));
            }
        }
        bool _isvisibleACount { get; set; } = false;
        public bool IsvisibleACount
        {
            get => _isvisibleACount;
            set
            {
                _isvisibleACount = value;
                SetPropertyChanged(nameof(IsvisibleACount));
            }
        }
        bool _isvisibleNCount { get; set; } = false;
        public bool IsvisibleNCount
        {
            get => _isvisibleNCount;
            set
            {
                _isvisibleNCount = value;
                SetPropertyChanged(nameof(IsvisibleNCount));
            }
        }

        public ICommand LogoutCommand => new Command(Logout);
        public ICommand DoctorsCommand => new Command(Doctors);
        public ICommand PharmaciesCommand => new Command(Pharmacies);
        public ICommand PrescriptionsCommand => new Command(Prescriptions);
        public ICommand AppointmentsCommand => new Command(Appointments);
        public ICommand NotesCommand => new Command(Notes);
        public ICommand NotificationCommand => new Command(Notification);
        public ICommand ProfileCommand => new Command(Profile);
        public ICommand UserProfileCommand => new Command(UserProfile);
        public ICommand PatientsCommand => new Command(Patients);

        public DashboardPageViewModel()
        {
            MessagingCenter.Subscribe<MessagingCenterModel>(this, "ProfileImageUpdate", (sender) =>
            {
                Image = ServiceConfiguration.ImageUrl + Path.GetFileName(Settings.UserImage);
            });


            OnBoardingsData = new ObservableCollection<OnBoardingModel>
            {
                new OnBoardingModel
                {
                    CarosuelImage ="WomenDoctor.png",
                    CarosuelTitle="Book The Appointment",
                    CarosuelDescription="Find & book appointment with Doctors , clinics, Hospitals & Diagnostic Tests",
                },
                new OnBoardingModel
                {
                    CarosuelImage ="MaleDoctor.png",
                    CarosuelTitle="Online Consultation",
                    CarosuelDescription="Select the specialist and make an Consult through our app! ",

                },
                  new OnBoardingModel
                {
                    CarosuelImage ="PharmacyDoctor.png",
                    CarosuelTitle="Online Pharmacy",
                    CarosuelDescription="Order any medicine or health product and we'll deliver it for free. Enjoy discounts on everythink",


                },
            };

        }

        public override void OnAppearing()
        {
            //base.OnAppearing();
            //GetToken();
            //LoadData(); //get Notificationcount
            //Settings.FirstName = "";
            //Settings.LastName = "";
            //Image = ServiceConfiguration.ImageUrl + Path.GetFileName(Settings.UserImage);
            //UserName = Settings.FirstName + " " + Settings.LastName;
            ////MonoGram = Settings.FirstName.ToCharArray()[0].ToString().ToUpper() + Settings.LastName.ToCharArray()[0].ToString().ToUpper();
            base.OnAppearing();
            GetToken();
            LoadData(); //get Notificationcount
            Image = ServiceConfiguration.ImageUrl + Path.GetFileName(Settings.UserImage);
            UserName = Settings.FirstName + " " + Settings.LastName;
            //MonoGram = Settings.FirstName.ToCharArray()[0].ToString().ToUpper() + Settings.LastName.ToCharArray()[0].ToString().ToUpper();
        }


        public async void GetToken()
        {
            if (Settings.NewLogin || Settings.NewToken)
            {
                if (string.IsNullOrEmpty(Settings.FCMToken))
                    return;
                else
                {

                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.UpdateDevice(new UpdateDeviceInputModel() { DeviceId = Settings.FCMToken, DeviceType = Settings.DeviceType });

                        if (result != null && result.Status)
                        {
                            Settings.NewLogin = false;
                            Settings.NewToken = false;
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }

                }
            }
        }
        public async void LoadData() //get Notificationcount
        {
            try
            {

                if (App.IsConnected)
                {
                    var result = await App.PAServiceManager.GetNotificationCount();

                    if (result != null && result.Data != null)
                    {
                        NotificationCount = result.Data;
                        IsvisiblePCount = false;
                        IsvisibleACount = false;
                        IsvisibleNCount = false;


                        if (NotificationCount.Prescription != 0)
                        {
                            IsvisiblePCount = true;
                        }
                        if (NotificationCount.Appointment != 0)
                        {
                            IsvisibleACount = true;
                        }
                        if (NotificationCount.Notification != 0)
                        {
                            IsvisibleNCount = true;
                        }




                    }
                    else if (result == null)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                }

            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        async void Logout()
        {
            var Confirm = await Application.Current.MainPage.DisplayAlert("Prezript", "Are you sure you want to logout?", "Yes", "Cancel");
            if (Confirm)
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {

                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.Logout();

                        if (result != null && result.Status)
                        {
                            Application.Current.MainPage = new NavigationPage(new SignInPage());
                            Settings.UserLoggedIn = false;
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
        }
        async void Prescriptions()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new PrescriptionPage());
            IsBusy = false;
        }
        async void Doctors()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new DoctorsPage());
            IsBusy = false;

        }

        async void Appointments()
        {

            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new AppoinmentsPage());
            IsBusy = false;
        }
        async void Pharmacies()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new PharmaciesPage());
            IsBusy = false;

        }
        async void Notes()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new MyNotesPage());
            IsBusy = false;

        }
        async void Notification()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new NotificationPage());
            IsBusy = false;

        }
        async void Profile()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new ProfilePage());
            IsBusy = false;

        }
        async void UserProfile()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new UserProfilePage());
            IsBusy = false;

        }
        async void Patients()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new PatientsPage());
            IsBusy = false;

        }

    }
}
