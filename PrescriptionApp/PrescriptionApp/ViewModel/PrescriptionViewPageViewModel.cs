﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{

   public  class PrescriptionViewPageViewModel : BaseViewModel
    {
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        ObservableCollection<PrescriptionByUser> _prescriptionsList { get; set; }
        public ObservableCollection<PrescriptionByUser> PrescriptionsList
        {
            get => _prescriptionsList;
            set { _prescriptionsList = value; SetPropertyChanged(nameof(PrescriptionsList)); }
        }
        public int UserId { get; set; }
        
        public ICommand BackCommand => new Command(Back);
        public ICommand ShowPrescriptionListCommand => new Command<PrescriptionByUser>(ShowPrescription);
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        public async void ShowPrescription(PrescriptionByUser prescription)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new PrescriptionDetailPage();
            page.ViewModel.PrecriptionId = prescription.PrescriptionId;
            page.ViewModel.DoctorId = prescription.UserId; ;
            //page.ViewModel.CustomerId = UserId;
            page.ViewModel.CustomerDescription = prescription.Description;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        public async void LoadData() 
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetPrescriptionByPatient(new GetPrescriptionByPatientInputModel() { UserId=UserId} );

                        if (result != null && result.Data != null)
                        {
                           
                            PrescriptionsList = result.Data;
                            
                            //PrescriptionsList.Remove((PrescriptionModel)PrescriptionsList.Where(x => x.Status == PrescriptionApproval.SentToPharmacy));
                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
