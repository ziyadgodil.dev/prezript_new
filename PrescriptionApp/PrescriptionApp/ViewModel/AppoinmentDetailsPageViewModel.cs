﻿using Acr.UserDialogs;
using Plugin.Media.Abstractions;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class AppoinmentDetailsPageViewModel : BaseViewModel
    {
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        AppoinmentsModel _appoinmentsDetails { get; set; }
        public AppoinmentsModel AppoinmentsDetails
        {
            get => _appoinmentsDetails;
            set
            {
                _appoinmentsDetails = value;
                SetPropertyChanged(nameof(AppoinmentsDetails));
            }
        }

        bool _cancelButtonVisible { get; set; }
        public bool CancelButtonVisible
        {
            get => _cancelButtonVisible;
            set
            {
                _cancelButtonVisible = value;
                SetPropertyChanged(nameof(CancelButtonVisible));
            }
        }

        bool _approveRejectButtonVisible { get; set; }
        public bool ApproveRejectButtonVisible
        {
            get => _approveRejectButtonVisible;
            set
            {
                _approveRejectButtonVisible = value;
                SetPropertyChanged(nameof(ApproveRejectButtonVisible));
            }
        }


        public ICommand BackCommand => new Command(Back);

        public ICommand CancelAppointmentCommand => new Command(CancelAppointment);

        public ICommand ApproveAppointmentCommand => new Command(ApproveAppointment);

        public int AppointmentUserId { get; set; }

        public AppoinmentDetailsPageViewModel()
        {

        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadDataforAppointment();
        }
        public async void LoadDataforAppointment()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetAppointment(new AppointmentDetailsInputModel() { AppointmentId = AppointmentUserId });

                        if (result != null && result.Status && result.Data != null)
                        {
                            AppoinmentsDetails = result.Data;
                            CancelButtonVisible = result.Data.Date.ToLocalTime() > DateTime.Now &&
                                result.Data.Appointmentstatus == AppointmentStatus.Pending &&
                                UserType == UserType.Patient;
                            ApproveRejectButtonVisible = result.Data.Appointmentstatus == AppointmentStatus.Pending &&
                                UserType == UserType.Doctor;
                        }

                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }

                }

            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");

            }
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();

        }

        private async void ApproveAppointment()
        {
            try
            {
                int DateResult = DateTime.Compare(AppoinmentsDetails.Date.ToLocalTime(), DateTime.Now);
           
                if (DateResult<=0)
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "This Appointment can't be approve.", "Ok");
                    return;
                }
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    AcceptorRejectAppointmentInputModel acceptorRejectAppointmentInputModel = new AcceptorRejectAppointmentInputModel();
                    acceptorRejectAppointmentInputModel.AppointmentId = AppointmentUserId;
                    acceptorRejectAppointmentInputModel.Status = AppointmentStatus.Approved;

                    var result = await App.PAServiceManager.AcceptorRejectAppointment(acceptorRejectAppointmentInputModel);
                    if (result != null && result.Status)
                    {
                        await Application.Current.MainPage.DisplayAlert("Success", "Appointment approved successfully", "Ok");
                        await Application.Current.MainPage.Navigation.PopAsync();
                    }
                    else if (result != null)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private async void CancelAppointment()
        {
            if (UserType == UserType.Patient)
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new ReasonPopup();
                page.UserId = AppointmentUserId;
                page.Appointmentstatus = Convert.ToInt32(AppointmentStatus.Canceled);
                await PopupNavigation.Instance.PushAsync(page);
                IsBusy = false;
            }
            else if (UserType == UserType.Doctor)
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new ReasonPopup();
                page.UserId = AppointmentUserId;
                page.Appointmentstatus = Convert.ToInt32(AppointmentStatus.Rejected);
                await PopupNavigation.Instance.PushAsync(page);
                IsBusy = false;
            }
            else
                await Application.Current.MainPage.Navigation.PopAsync();

        }


    }
}
