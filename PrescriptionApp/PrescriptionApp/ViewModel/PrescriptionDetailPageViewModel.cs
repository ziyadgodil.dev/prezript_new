﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Essentials;
using Rg.Plugins.Popup.Services;
using PrescriptionApp.View.Popup;
using PrescriptionApp.View;
using Acr.UserDialogs;
using PrescriptionApp.Helper.Resources;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Xamarin.Forms.OpenWhatsApp;
using Xamarin.Forms.Shapes;
using System.IO;
using Xamarin.Forms.Internals;

namespace PrescriptionApp.ViewModel
{
    public class PrescriptionDetailPageViewModel : BaseViewModel
    {
        bool First = true;
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        public int PrecriptionId { get; set; }
        public int DoctorId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerDescription { get; set; }
        PrescriptionDetail _prescriptionsDetails { get; set; } = new PrescriptionDetail();
        public PrescriptionDetail PrescriptionsDetails
        {
            get => _prescriptionsDetails;
            set
            {
                _prescriptionsDetails = value;
                SetPropertyChanged(nameof(PrescriptionsDetails));
            }
        }
        UserProfileModel _profileDetail { get; set; }
        public UserProfileModel ProfileDetail
        {
            get => _profileDetail;
            set { _profileDetail = value; SetPropertyChanged(nameof(ProfileDetail)); }
        }

        Pharmacy _pharmacy { get; set; }
        public Pharmacy Pharmacy
        {
            get => _pharmacy;
            set
            {
                _pharmacy = value;
                SetPropertyChanged(nameof(Pharmacy));
                SetPropertyChanged(nameof(PharmacyNotnull));
            }
        }
        Pharmacy _pharmacytemp { get; set; }
        public Pharmacy PharmacyTemp
        {
            get => _pharmacytemp;
            set
            {
                _pharmacytemp = value;
                SetPropertyChanged(nameof(PharmacyTemp));

            }
        }
     
        public double DisplayAmount { get; set; }
        public bool PharmacyNotnull => Pharmacy != null;

        private bool _shareVisible;
        public bool ShareVisible
        {
            get { return _shareVisible; }
            set { _shareVisible = value; SetPropertyChanged(nameof(ShareVisible)); }

        }

        private bool _isReceived;
        public bool isReceived
        {
            get { return _isReceived; }
            set { _isReceived = value; SetPropertyChanged(nameof(isReceived)); }

        }

        private bool _pharmacyVisible;
        public bool PharmacyVisible
        {
            get { return _pharmacyVisible; }
            set
            {
                _pharmacyVisible = value;
                SetPropertyChanged(nameof(PharmacyVisible));
            }

        }

        private bool _selectPharmaVisible;
        public bool SelectPharmaVisible
        {
            get { return _selectPharmaVisible; }
            set
            {
                _selectPharmaVisible = value;
                SetPropertyChanged(nameof(SelectPharmaVisible));
            }

        }

        private bool _medicineVisible;
        public bool MedicineVisible
        {
            get { return _medicineVisible; }
            set
            {
                _medicineVisible = value;
                SetPropertyChanged(nameof(MedicineVisible));
            }

        }
        private bool _medicinePlusVisible;
        public bool MedicinePlusVisible
        {
            get { return _medicinePlusVisible; }
            set
            {
                _medicinePlusVisible = value;
                SetPropertyChanged(nameof(MedicinePlusVisible));
            }

        }
        bool _approveRejectButtonVisible { get; set; } = false;
        public bool ApproveRejectButtonVisible
        {
            get => _approveRejectButtonVisible;
            set
            {
                _approveRejectButtonVisible = value;
                SetPropertyChanged(nameof(ApproveRejectButtonVisible));
            }
        }
        private string _address;
        public string Address
        {
            get { return _address; }
            set { _address = value; SetPropertyChanged(nameof(Address)); }

        }
        private string _addressTitle;
        public string AddressTitle
        {
            get { return _addressTitle; }
            set { _addressTitle = value; SetPropertyChanged(nameof(AddressTitle)); }

        }
        UpdatePrescriptionByPharmacyInputModel _updatePrescriptionByPharmacyInput { get; set; } = new UpdatePrescriptionByPharmacyInputModel();

        public UpdatePrescriptionByPharmacyInputModel UpdatePrescriptionByPharmacyInput
        {
            get => _updatePrescriptionByPharmacyInput;
            set
            {
                _updatePrescriptionByPharmacyInput = value;
                SetPropertyChanged(nameof(UpdatePrescriptionByPharmacyInput));
            }
        }


        public ICommand BackCommand => new Command(Back);
        public ICommand EditCommand => new Command<AddEditmedicineModel>(Edit);
        public ICommand AddCommand => new Command(Add);
        public ICommand SelectPharmacyCommand => new Command(SelectPharmacy);
        public ICommand NotesCommand => new Command(CheckNotes);

        public ICommand ShareCommand => new Command(Share);
        public ICommand CancelCommand => new Command(Cancel);

        public ICommand ApproveCommand => new Command(Approve); 
        //public ICommand preCmd => new Command<AddEditmedicineModel>(pres);
        public ICommand preCmdSms => new Command<AddEditmedicineModel>(presSms);
        public PrescriptionDetailPageViewModel()
        {


        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();

            //if (UserType == UserType.Pharma)
            //    {
            //        ApproveRejectButtonVisible = true; ShareVisible = false;
            //    }

            //if (PrescriptionsDetails.Status == PrescriptionApproval.SentToPharmacy)
            //    ShareVisible = false; ApproveRejectButtonVisible = true;
            if (UserType == UserType.Patient || UserType == UserType.Doctor)
                ApproveRejectButtonVisible = false;

        }

        private async void Cancel()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new SentToPharmcyPopUp();
            var model = new UpdatePrescriptionByPharmacyInputModel();
            model.PrescriptionId = PrecriptionId;
            model.Status = PrescriptionsDetails.Status;
            page.ApproveRejectVisible = (int)PrescriptionApproval.SentToCustomer;
            page.UpdatePrescriptionByPharmacyInput = model;
            await PopupNavigation.Instance.PushAsync(page);
            IsBusy = false;
        }
        private async void Approve()
        {

            try
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new SentToPharmcyPopUp();
                var model = new UpdatePrescriptionByPharmacyInputModel();
                model.PrescriptionId = PrecriptionId;
                model.Status = PrescriptionsDetails.Status;
                page.ApproveRejectVisible = (int)PrescriptionApproval.SentToPharmacy;
                page.UpdatePrescriptionByPharmacyInput = model;
                await PopupNavigation.Instance.PushAsync(page);
                IsBusy = false;
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        //private async void pres(AddEditmedicineModel obj)
        //{

        //    try
        //    {

        //        //   await Application.Current.MainPage.DisplayAlert("Erro", "dfs", "OK");

        //        Chat.Open("", obj.DrugsName.ToString() + "\n" + obj.Strength.ToString() + obj.StrengthUnit.ToString() + "\n" + obj.DirectionValue.ToString()) ;  
        //    }
        //    catch (Exception ex)
        //    {
        //        await Application.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
        //    }
        //}
        private async void presSms(AddEditmedicineModel obj)
        {

            try
            {

                //   await Application.Current.MainPage.DisplayAlert("Erro", "dfs", "OK");
                await Xamarin.Essentials.Share.RequestAsync(new ShareTextRequest(obj.DrugsName.ToString() + "\n" + obj.Strength.ToString() + obj.StrengthUnit.ToString() + "\n" + obj.DirectionValue.ToString(), ""));

                // Chat.Open("", obj.DrugsName.ToString() + "\n" + obj.Strength.ToString() + obj.StrengthUnit.ToString() + "\n" + obj.DirectionValue.ToString());
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
            }
        }
        async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetPrescriptionsById(new PrescriptionByIdInputModel() { PrescriptionId = PrecriptionId, DoctorId = DoctorId });

                        if (result != null && result.Status && result.PrescriptionDetail != null)
                        {
                            PrescriptionsDetails = result.PrescriptionDetail;
                           // PrescriptionsDetails.Medicine.ForEach(x => x.MedicinePlusVisible = MedicinePlusVisible);

                            if (PrescriptionsDetails.Status != PrescriptionApproval.SentToCustomer)
                            {
                                if (result.PrescriptionDetail.Pharmacy != null)
                                {
                                    Pharmacy = result.PrescriptionDetail.Pharmacy;
                                    if (PrescriptionsDetails.Pharmacy.PharmacyName == null)
                                    {
                                        Pharmacy = PharmacyTemp;
                                    }
                                }
                            }
                            else
                            {
                                PrescriptionsDetails.Pharmacy = null;
                            }

                            if (UserType == UserType.Doctor)
                            {
                                ShareVisible = PrescriptionsDetails.Medicine.Count != 0;
                                ApproveRejectButtonVisible = false;
                                MedicineVisible = true;
                                MedicinePlusVisible = true;

                                if (PrescriptionsDetails.Status == PrescriptionApproval.SentToCustomer
                                    || PrescriptionsDetails.Status == PrescriptionApproval.Accept
                                    || PrescriptionsDetails.Status == PrescriptionApproval.SentToPharmacy)
                                {
                                    ShareVisible = false; MedicinePlusVisible = false;
                                }
                            }
                            else if (UserType == UserType.Pharma)
                            {
                                MedicineVisible = true; ApproveRejectButtonVisible = true;
                                if (PrescriptionsDetails.Status == PrescriptionApproval.Reject ||
                                    PrescriptionsDetails.Status == PrescriptionApproval.Accept)
                                {
                                    ShareVisible = false;
                                    ApproveRejectButtonVisible = false;
                                    MedicinePlusVisible = false;
                                }
                                else
                                {
                                    ShareVisible = false;
                                    ApproveRejectButtonVisible = true;
                                }

                                if(result.PrescriptionDetail.Latitude!=null && result.PrescriptionDetail.Longitude != null)
                                {
                                    MessagingCenter.Send(new LocationModel(){Latitude = result.PrescriptionDetail.Latitude,Longitude = result.PrescriptionDetail.Longitude,Address = result.PrescriptionDetail.Address}, "LocationLoaded");
                                }
                                else
                                {
                                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "This user was not update thier address(Latitude and Longitude)", "Ok");
                                }
                               
                            }
                            else if (UserType == UserType.Patient)
                            {
                                ShareVisible = PrescriptionsDetails.Medicine.Count != 0 &&
                                    Pharmacy != null &&
                                    PrescriptionsDetails.Status != PrescriptionApproval.SentToPharmacy;
                                ApproveRejectButtonVisible = false;
                                MedicinePlusVisible = false;


                                if (PrescriptionsDetails.Status == PrescriptionApproval.Pending
                                    || PrescriptionsDetails.Medicine.Count == 0)
                                {
                                    SelectPharmaVisible = false;
                                    PharmacyVisible = false;
                                }
                                if (PrescriptionsDetails.Status != PrescriptionApproval.Pending)
                                {
                                    MedicineVisible = true;
                                    if (Pharmacy != null)
                                        SelectPharmaVisible = true;
                                }
                                if (PrescriptionsDetails.Status == PrescriptionApproval.Reject)
                                {
                                    if (Pharmacy != null)
                                    {
                                        SelectPharmaVisible = false;
                                        PharmacyVisible = true;
                                    }
                                    else
                                    {
                                        SelectPharmaVisible = true;
                                        PharmacyVisible = false;
                                    }
                                }
                                if (PrescriptionsDetails.Status == PrescriptionApproval.SentToPharmacy)
                                {
                                    SelectPharmaVisible = false;
                                    PharmacyVisible = true;
                                }
                                //if Pharmacy Accept prescription
                                if (PrescriptionsDetails.Medicine.Count != 0 &&
                                    Pharmacy != null &&
                                    PrescriptionsDetails.Status == PrescriptionApproval.Accept)
                                {
                                    ShareVisible = false;

                                }

                                if (PrescriptionsDetails.Medicine.Count != 0)
                                {
                                    if (Pharmacy == null)
                                        SelectPharmaVisible = true;
                                    else
                                    {
                                        PharmacyVisible = true;
                                        SelectPharmaVisible = false;
                                    }
                                }

                                if (PrescriptionsDetails.Status != PrescriptionApproval.Pending && PrescriptionsDetails.Status != PrescriptionApproval.Reject)
                                {
                                    if (result.PrescriptionDetail.Pharmacy.Latitude != null && result.PrescriptionDetail.Pharmacy.Longitude != null)
                                    {
                                        MessagingCenter.Send(new LocationModel() { Latitude = result.PrescriptionDetail.Pharmacy.Latitude, Longitude = result.PrescriptionDetail.Pharmacy.Longitude }, "LocationLoaded");
                                    }
                                    else
                                    {
                                        await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Selected pharmacy was not update thier address(Latitude and Longitude)", "Ok");
                                    } 
                                }
                                
                            }

                        }
                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void Add()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            if (UserType != UserType.Doctor)
                return;
            var page = new EditMedicinePopUp();
            page.IsEdit = false;
            page.AddEditmedicine = new AddEditmedicineModel();
            page.AddEditmedicine.PrescriptionId = PrecriptionId;
            await PopupNavigation.Instance.PushAsync(page);
            IsBusy = false;
            page.SelectionHandler += (object sender, AddEditmedicineModel addEditmedicineModel) =>
            {
                if (PrescriptionsDetails.Medicine == null)
                    PrescriptionsDetails.Medicine = new ObservableCollection<AddEditmedicineModel>();
                PrescriptionsDetails.Medicine.Add(addEditmedicineModel);
                ShareVisible = true;
            };

        }
        async void SelectPharmacy()
        {

            try
            {
                if (UserType != UserType.Patient)
                    return;
                if (PrescriptionsDetails.Status == PrescriptionApproval.Accept)
                    return;
                if (PrescriptionsDetails.Status == PrescriptionApproval.SentToPharmacy)
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert!", "Please wait for confirmation from pharmacy", "OK");
                    return;
                }
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new PharmaciesPage();
                page.ViewModel.Selection = true;
                await Application.Current.MainPage.Navigation.PushAsync(page);
                IsBusy = false;
                page.ViewModel.SelectionHandler += (object sender, PharmaciesModel pharmaciesModel) =>
                {
                    var pharmacy = new Pharmacy();
                    pharmacy.PharmacyId = pharmaciesModel.PharmacyId;
                    pharmacy.PharmacyName = pharmaciesModel.FullName;
                    pharmacy.Address = pharmaciesModel.Address;
                    pharmacy.ContactNo = pharmaciesModel.Contact;
                    pharmacy.Image = pharmaciesModel.PharmaciesImage;
                    PharmacyTemp = pharmacy;
                    PrescriptionsDetails.Pharmacy = pharmacy;
                    Pharmacy = pharmacy;
                    PharmacyVisible = true;
                    if (pharmaciesModel.Latitude!=null && pharmaciesModel.Longitude!= null)
                    {
                        MessagingCenter.Send(new LocationModel() { Latitude = pharmaciesModel.Latitude, Longitude = pharmaciesModel.Longitude }, "LocationLoaded");
                    }
                    else
                    {
                       Application.Current.MainPage.DisplayAlert("Prezript Alert", "Selected pharmacy was not update thier address(Latitude and Longitude)", "Ok");
                    }
                    
                };
            }
            catch(Exception ex)
            {
                ex.ToString();
            }

        }
        async void Edit(AddEditmedicineModel addEditmedicine)
        {
            if (UserType != UserType.Doctor)
                return;
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new EditMedicinePopUp();
            page.AddEditmedicine = JsonConvert.DeserializeObject<AddEditmedicineModel>(JsonConvert.SerializeObject(addEditmedicine));
            page.IsEdit = true;
            await PopupNavigation.Instance.PushAsync(page);
            IsBusy = false;
            page.SelectionHandler += (object sender, AddEditmedicineModel addEditmedicineModel) =>
            {
                //if (PrescriptionsDetails.Medicine != null || PrescriptionsDetails.Medicine.Count != 0)
                //    PrescriptionsDetails.Medicine = new ObservableCollection<AddEditmedicineModel>();
                //PrescriptionsDetails.Medicine.Remove(addEditmedicineModel);
            };

        }

        async void Share()
        {
            if (UserType == UserType.Patient)
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new SentToPharmcyPopUp();
                var model = new SentToPharmcyInputModel();
                model.PrescriptionId = PrecriptionId;
                model.PharmacyId = Pharmacy.PharmacyId;
                model.Address = PrescriptionsDetails.Customer.Address;
                model.Latitude = PrescriptionsDetails.Customer.Latitude;
                model.Longitude = PrescriptionsDetails.Customer.Longitude;
                page.SentToPharmcyInput = model;

                await PopupNavigation.Instance.PushAsync(page);
                IsBusy = false;
            }
            else if (UserType == UserType.Pharma)
            {
                if (IsBusy)
                {
                    return;
                }
                else
                {
                    IsBusy = true;
                }
                var page = new SentToPharmcyPopUp();
                var model = new UpdatePrescriptionByPharmacyInputModel();
                model.PrescriptionId = PrecriptionId;
                model.Status = PrescriptionsDetails.Status;
                page.UpdatePrescriptionByPharmacyInput = model;
                await PopupNavigation.Instance.PushAsync(page);
                IsBusy = false;
            }
            else if (UserType == UserType.Doctor)
            {
                if (PrescriptionsDetails.Medicine.Count == 0)
                {
                    await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Please add medicine first !", "Ok");
                    return;
                }
                else
                {
                    try
                    {
                        using (UserDialogs.Instance.Loading("Loading.."))
                        {
                            if (App.IsConnected)
                            {
                                var result = await App.PAServiceManager.SendPrescriptiontoPatient(new SendPrescriptionToPatientInputModel() { PrescriptionId = PrecriptionId });

                                if (result != null && result.Status)
                                {
                                    await Application.Current.MainPage.DisplayAlert("Success", "Prescription Sent Successfully", "Ok");
                                    await Application.Current.MainPage.Navigation.PopAsync();
                                }
                                else if (result == null)
                                {
                                    await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                                }
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Prezript Alert", "Check Internet Connection", "Ok");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }

        }

        async void CheckNotes()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new NotesPage();
            page.ViewModel.DoctorId = DoctorId;
            page.ViewModel.CustomerId = CustomerId;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }

    }
}
