﻿using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class PrivacyPolicyViewModel : BaseViewModel
    {
        public ICommand BackCmmand => new Command(Back);

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
