﻿using Acr.UserDialogs;
using FFImageLoading;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class PatientsPageViewModel : BaseViewModel
    {
        ObservableCollection<PatientModel> SearchList { get; set; }
        ObservableCollection<PatientModel> _PatientList { get; set; }
        public ObservableCollection<PatientModel> PatientList
        {
            get => _PatientList;
            set
            {
                _PatientList = value;
                SetPropertyChanged(nameof(PatientList));
            }
        }
        string _selectedPatient { get; set; }
        public string SelectedPatient
        {
            get { return _selectedPatient; }
            set { _selectedPatient = value; SetPropertyChanged(nameof(SelectedPatient)); }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }

        public ICommand BackCommand { get; set; }
        public ICommand UserProfileCommand { get; set; }


        public PatientsPageViewModel()
        {
            BackCommand = new Command(Back);
            UserProfileCommand = new Command<PatientModel>(UserProfile);

        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData(); //get Patient details
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        public async void UserProfile(PatientModel patientModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new UserProfilePage();
            page.ViewModel.UserId = patientModel.PatientId;
            page.ViewModel.UserProfileTitle = "User Profile";
            await App.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        public async void LoadData() //get Patient details
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetPatientList();

                        if (result != null && result.Data != null)
                        {
                            SearchList = result.Data;
                            PatientList = SearchList;

                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");

                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        void Search()
        {
            if (SearchList != null && SearchList.Count != 0)
            {
                if (!string.IsNullOrEmpty(SearchText))
                    PatientList = new ObservableCollection<PatientModel>(SearchList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
                else
                    PatientList = SearchList;
            }
        }
    }
}
