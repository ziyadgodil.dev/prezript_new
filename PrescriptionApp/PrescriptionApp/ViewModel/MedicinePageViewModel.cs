﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
   public  class MedicinePageViewModel :BaseViewModel
    {
        public ICommand BackCommand => new Command(Back);
        public ICommand MedicationCommand => new Command(Medication);

        async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        async void Medication()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new MedicinePage());
            IsBusy = false;
        }

    }
}
