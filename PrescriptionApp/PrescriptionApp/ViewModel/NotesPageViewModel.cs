﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using PrescriptionApp.View.Popup;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class NotesPageViewModel : BaseViewModel
    {

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        ObservableCollection<NotesModel> _listNotes { get; set; }

        public ObservableCollection<NotesModel> ListNotes
        {
            get => _listNotes;
            set { _listNotes = value; SetPropertyChanged(nameof(ListNotes)); }
        }

        ObservableCollection<NotesModel> _allListNotes { get; set; }
        public ObservableCollection<NotesModel> AllListNotes
        {
            get => _allListNotes;
            set
            {
                _allListNotes = value;
                SetPropertyChanged(nameof(AllListNotes));
            }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }


        public int DoctorId { get; set; }
        public int CustomerId { get; set; }

        public ICommand BackCommand => new Command(Back);
        public ICommand AddCommand => new Command(Add);
        public ICommand DetailsCommand => new Command<NotesModel>(Details);

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
            Search();
        }

        public NotesPageViewModel()
        {

        }

        async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.AllNotesById(
                            new AllNotesByIdInputModel()
                            {
                                PatientId = CustomerId,
                                DoctorId = DoctorId
                            });

                        if (result != null && result.Data != null)
                        {
                            ListNotes = result.Data;
                            AllListNotes = ListNotes;
                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        void Search()
        {
            if (string.IsNullOrEmpty(SearchText))
            {
                ListNotes = AllListNotes;
            }
            else
            {
                ListNotes = new ObservableCollection<NotesModel>(AllListNotes.Where(x => x.Title.ToLower().Contains(SearchText.ToLower())));
            }
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();

        }
        public async void Add()
        {
            var page = new AddNotePopup();
            page.IsEdit = true;
            page.Note = new NotesModel();
            page.Note.UserId = CustomerId;
            await PopupNavigation.Instance.PushAsync(page);
            page.SelectionHandler += (object sender, NotesModel notesModel) =>
            {
                if (AllListNotes == null)
                    AllListNotes = new ObservableCollection<NotesModel>();
                AllListNotes.Add(notesModel);
                ListNotes = AllListNotes;
            };
        }

        public async void Details(NotesModel Note)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new AddNotePopup();
            page.IsEditMode = true;
            page.Note = JsonConvert.DeserializeObject<NotesModel>(JsonConvert.SerializeObject(Note));
            await PopupNavigation.Instance.PushAsync(page);
            page.SelectionHandler += (object sender, NotesModel notesModel) =>
            {
                var note = AllListNotes.FirstOrDefault(x => x.NoteId == notesModel.NoteId);
                if (note != null)
                {
                    note.Title = notesModel.Title;                     
                    note.Description = notesModel.Description;
                    note.IsPrivate = notesModel.IsPrivate;

                    ListNotes = AllListNotes;
                }
            };
            IsBusy = false;
        }

    }
}
