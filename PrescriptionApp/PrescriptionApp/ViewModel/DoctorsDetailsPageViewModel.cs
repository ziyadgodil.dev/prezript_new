﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using PrescriptionApp.View.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class DoctorsDetailsPageViewModel : BaseViewModel
    {

        UserProfileModel _userprofileDetail { get; set; }
        public UserProfileModel UserProfileDetails
        {
            get => _userprofileDetail;
            set
            {
                _userprofileDetail = value;
                SetPropertyChanged(nameof(UserProfileDetails));
            }
        }
        public int DoctorId { get; set; }

        public ICommand BookCommand => new Command(Book);
        public ICommand BackCommand => new Command(Back);

        public DoctorsDetailsPageViewModel()
        {

        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        public async void Book()
        {
            var page = new AppointmentPopup();
            page.UserId = DoctorId;
            page.OpeningTime = UserProfileDetails.OpeningTime;
            page.ClosingTime = UserProfileDetails.ClosingTime;
            await PopupNavigation.Instance.PushAsync(page);

        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetUserDetailsById(new GetUserDetailByIdInputModel() { UserId = DoctorId });

                        if (result != null && result.Status && result.Profile != null)
                        {
                            UserProfileDetails = result.Profile;

                        }

                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");

            }
        }

    }
}
