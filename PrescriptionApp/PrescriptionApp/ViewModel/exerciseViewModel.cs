﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.OpenWhatsApp;

namespace PrescriptionApp.ViewModel
{
   public class exerciseViewModel : BaseViewModel
    {
        public ICommand BackCmmand => new Command(Back);
        public ICommand HealthBlog => new Command<exerciseModel>(HealthBlogDetail);
        public ICommand sortBy => new Command<string>(sortByexercise);

        async private void HealthBlogDetail(exerciseModel obj)
        {

            try
            {

                Chat.Open("", obj.Header.ToString() + "\n" + obj.Content.ToString() + "\n" + obj.Date.ToString());
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
            }
        }

         private void sortByexercise(string obj)
        {
            if (obj == "Strength")
            {
                OnBoardingsData = new ObservableCollection<exerciseModel>(OnBoardingsData.Where(x => x.Header == "Strength"));
            }
            else if (obj == "Balance")
            {
                OnBoardingsData = new ObservableCollection<exerciseModel>(OnBoardingsData.Where(x => x.Header == "Balance"));
            }
            else if (obj == "Flexibility")
            {
                OnBoardingsData = new ObservableCollection<exerciseModel>(OnBoardingsData.Where(x => x.Header == "Flexibility"));
            }
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        ObservableCollection<exerciseModel> _onBoardingsData { get; set; } = new ObservableCollection<exerciseModel>();

        public ObservableCollection<exerciseModel> OnBoardingsData
        {
            get => _onBoardingsData;
            set
            {
                _onBoardingsData = value;
                SetPropertyChanged(nameof(OnBoardingsData));
            }
        }

        public exerciseViewModel()
        {
            OnBoardingsData = new ObservableCollection<exerciseModel>
            {
                new exerciseModel
                {
                    Header="Strength",
                    CarosuelImage="Bilimbi.jpg",
                    Content="Bilimbi may contain bioactive compounds like amino acids,citric acid, cyanidin-3-o-h-D-glucoside,Phenolics and sugers,Additionally,ripened fruit of bilimbi may contain the following nutritional components:Portein,Fibers,Enedrgy,Lipids....",
                    Image2="whatsapp.png",
                    Date="Dr.Patel"
                },
                new exerciseModel
                {
                    Header="Balance",
                    CarosuelImage="hemp.jpg",
                    Content="It may improve memory and learning function." +
                    "it might enhance locomotor activity." +
                    "it may icrease insuline response" +
                    "it may act as an anti-fungal agent" +
                    "it may reduce pain",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new exerciseModel
                {
                    Header="Strength",
                    CarosuelImage="vaccine.jpg",
                    Content="The safety of mixing vaccines was also looked into by this test,to review its viability for use in the general people.the good news is that no adverse effect were notice in the short and long term.",
                    Image2="whatsapp.png",
                    Date="Dr.sankhe"
                },
                new exerciseModel
                {
                    Header="Balance",
                    CarosuelImage="RudraMudra.png",
                    Content="Rudra mudra belongs to the 'hasta' or the hand 'mudra' category and is believed to have powerful healing effect.'rudra' means 'Loard Shiv' and 'Mudra' means 'seal lock or hand gesture.'",
                    Image2="whatsapp.png",
                    Date="Dr.sankhe"
                },
                new exerciseModel
                {
                    Header="Strength",
                    CarosuelImage="sugar.jpg",
                    Content="Healthy Eating. Making smart food choices and bulding a diabetes-friendly meal plan will help make sure thry get the right amounts of protein,fats and carbohydrates.",
                    Image2="whatsapp.png",
                    Date="Dr.Patel"
                },
                new exerciseModel
                {
                    Header="Flexibilityr",
                    CarosuelImage="VitaminsD.png",
                    Content="People who have darker skin are at a greater risk of suffering from a viatmin D deficiency as their body doesn't absorb adequate sunlight.people who are obese or overweight.",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new exerciseModel
                {
                    Header="Strength",
                    CarosuelImage="Asthma.jpg",
                    Content="Asthma is a chronic respiratory illness that causes the narrowing of the airways due to inflammation this leads to symptoms like wheezing persistent coughing, breathlessness and more.",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new exerciseModel
                {
                    Header="Flexibilityr",
                    CarosuelImage="dentist.png",
                    Content="A successful orthodontic treatment begins with a proper diagnosis which involves a patient's full oral examination,studying the patieny's Database conducting dental  x-rays and making plaster models of the teeth.",
                    Image2="whatsapp.png",
                    Date="Dr.patel"
                }
            };
        }
        }
}
