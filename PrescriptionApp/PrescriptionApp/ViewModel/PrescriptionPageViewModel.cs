using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class PrescriptionPageViewModel : BaseViewModel
    {
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        int _selectedTab { get; set; } = 1;
        public int SelectedTab
        {
            get => _selectedTab;
            set { _selectedTab = value; SetPropertyChanged(nameof(SelectedTab)); }
        }

        ObservableCollection<PrescriptionModel> SentList { get; set; }
        ObservableCollection<PrescriptionModel> PendingList { get; set; }

        ObservableCollection<PrescriptionModel> RequestedList { get; set; }

        ObservableCollection<PrescriptionModel> _prescriptionsList { get; set; }
        public ObservableCollection<PrescriptionModel> PrescriptionsList
        {
            get => _prescriptionsList;
            set { _prescriptionsList = value; SetPropertyChanged(nameof(PrescriptionsList)); }
        }

        private string _order;
        public string Order
        {
            get => _order;
            set { _order = value; SetPropertyChanged(nameof(Order)); }
        }

        private string _orderRecieve;
        public string OrderRecieve
        {
            get => _orderRecieve;
            set { _orderRecieve = value; SetPropertyChanged(nameof(OrderRecieve)); }
        }

        private int _ctr;
        public int Ctr
        {
            get => _ctr;
            set { _ctr = value; SetPropertyChanged(nameof(Ctr)); }
        }
        bool _cancelButtonVisible { get; set; }
        public bool CancelButtonVisible
        {
            get => _cancelButtonVisible;
            set
            {
                _cancelButtonVisible = value;
                SetPropertyChanged(nameof(CancelButtonVisible));
            }
        }

        bool _approveRejectButtonVisible { get; set; }
        public bool ApproveRejectButtonVisible
        {
            get => _approveRejectButtonVisible;
            set
            {
                _approveRejectButtonVisible = value;
                SetPropertyChanged(nameof(ApproveRejectButtonVisible));
            }
        }

        public ICommand BackCommand => new Command(Back);
        public ICommand AddPrecription => new Command(AddPrescription);
        public ICommand ShowPrescriptionListCommand => new Command<PrescriptionModel>(ShowPrescription);
        public ICommand ChangeTabCommand => new Command<string>(ChangeTab);
        

        public PrescriptionPageViewModel()
        {

        }
        
       
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        public async void LoadData() //get Priscription details
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetPrescriptionDetails();

                        if (result != null && result.Data != null)
                        {

                            PendingList = result.Data.Pending;
                            SentList = result.Data.Sent;
                            RequestedList = result.Data.Requested;

                            ChangeTab(SelectedTab.ToString());
                            

                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public async void AddPrescription()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new DoctorsPage();
            page.ViewModel.Selection = true;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
            //page.ViewModel.SelectionHandler += async (object sender, DoctorsModel doctorsModel) =>
            //{
            //    var prescription = new AddPrescriptionPage();
            //    prescription.ViewModel.DoctorDetails = doctorsModel;
            //    prescription.ViewModel.IsSelectDocVisible = false;
            //    await Application.Current.MainPage.Navigation.PushAsync(prescription);
            //};
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void ShowPrescription(PrescriptionModel prescription)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new PrescriptionDetailPage();
            page.ViewModel.PrecriptionId = prescription.PrescriptionId;
            page.ViewModel.DoctorId = prescription.DoctorId;
            page.ViewModel.CustomerId = prescription.CustomerId;
            page.ViewModel.CustomerDescription = prescription.Description;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }

        private void ChangeTab(string tab)
        {
            SelectedTab = Convert.ToInt32(tab);
            if (UserType == UserType.Doctor || UserType == UserType.Pharma)
            {
                if (SelectedTab == 1)
                {
                    PrescriptionsList = PendingList;
                }
                else if (SelectedTab == 2)
                {
                    using (UserDialogs.Instance.Loading("Loading.."))
                    {
                        PrescriptionsList = SentList;
                    }
                }

            }
            else if (UserType == UserType.Patient )
            {
                if (SelectedTab == 1)
                {
                    PrescriptionsList = RequestedList;
                }
                else if (SelectedTab == 2)
                {
                    PrescriptionsList = PendingList;
                }
                else if (SelectedTab == 3)
                {
                    PrescriptionsList = SentList;
                    
                }
            }
        }
    }
}
