﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
   public  class ForgotPageViewModel : BaseViewModel
    {
        public ICommand LoginCommand { protected set; get; }

        public ForgotPageViewModel()
        {
            LoginCommand = new Command(Login);
        }
        public async void Login()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            await Application.Current.MainPage.Navigation.PushAsync(new SignInPage());
            IsBusy= false;
        }
    }
}
