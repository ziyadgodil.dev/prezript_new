﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class UserNotesPageViewModel:BaseViewModel
    {
        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        public int UserId { get; set; }
        ObservableCollection<NotesModel> _listNotes { get; set; }

        public ObservableCollection<NotesModel> ListNotes
        {
            get => _listNotes;
            set { _listNotes = value; SetPropertyChanged(nameof(ListNotes)); }
        }

        public ICommand BackCommand => new Command(Back);
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }
        async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetUserNotesById(new GetUserNotesByIdInputModel() { UserId = UserId });

                        if (result != null && result.Data != null)
                        {
                            ListNotes = result.Data;
                            
                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
