﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.OpenWhatsApp;

namespace PrescriptionApp.ViewModel
{
    public class HealthBlogViewModel : BaseViewModel
    {
        public ICommand BackCmmand => new Command(Back);
        public ICommand HealthBlog => new Command<HealthBlogModel>(HealthBlogDetail);

        async private void HealthBlogDetail(HealthBlogModel obj)
        {

            try
            {  
                  
                 Chat.Open("", obj.Header.ToString() + "\n" + obj.Content.ToString() + "\n" + obj.Date.ToString() );
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", ex.Message, "OK");
            }
        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        ObservableCollection<HealthBlogModel> _onBoardingsData { get; set; } = new ObservableCollection<HealthBlogModel>();

        public ObservableCollection<HealthBlogModel> OnBoardingsData
        {
            get => _onBoardingsData;
            set
            {
                _onBoardingsData = value;
                SetPropertyChanged(nameof(OnBoardingsData));
            }
        }

        public HealthBlogViewModel() 
        {
            OnBoardingsData = new ObservableCollection<HealthBlogModel>
            {
                new HealthBlogModel
                {
                    Header="Bilimbi : uses,benefits and side Effects By Dr.Patel",
                    CarosuelImage="Bilimbi.jpg",
                    Content="Bilimbi may contain bioactive compounds like amino acids,citric acid, cyanidin-3-o-h-D-glucoside,Phenolics and sugers,Additionally,ripened fruit of bilimbi may contain the following nutritional components:Portein,Fibers,Enedrgy,Lipids....",
                    Image2="whatsapp.png",
                    Date="Dr.Patel"
                },
                new HealthBlogModel
                {
                    Header="Hemp seed : uses,Benifits and side Effect by Dr.Janki talaviya",
                    CarosuelImage="hemp.jpg",
                    Content="It may improve memory and learning function." +
                    "it might enhance locomotor activity." +
                    "it may icrease insuline response" +
                    "it may act as an anti-fungal agent" +
                    "it may reduce pain",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new HealthBlogModel
                {
                    Header="Mixing Covishild And Covaxin Dose It work Better?",
                    CarosuelImage="vaccine.jpg",
                    Content="The safety of mixing vaccines was also looked into by this test,to review its viability for use in the general people.the good news is that no adverse effect were notice in the short and long term.",
                    Image2="whatsapp.png",
                    Date="Dr.sankhe"
                },
                new HealthBlogModel
                {
                    Header="Benefits of Rudra mudra and how to do it by Dr.sankhe",
                    CarosuelImage="RudraMudra.png",
                    Content="Rudra mudra belongs to the 'hasta' or the hand 'mudra' category and is believed to have powerful healing effect.'rudra' means 'Loard Shiv' and 'Mudra' means 'seal lock or hand gesture.'",
                    Image2="whatsapp.png",
                    Date="Dr.sankhe"
                },
                new HealthBlogModel
                {
                    Header="Daibetes caregiver's prespective:How to take care of a Diabetic patient?",
                    CarosuelImage="sugar.jpg",
                    Content="Healthy Eating. Making smart food choices and bulding a diabetes-friendly meal plan will help make sure thry get the right amounts of protein,fats and carbohydrates.",
                    Image2="whatsapp.png",
                    Date="Dr.Patel"
                },
                new HealthBlogModel
                {
                    Header="Vitamin D Deficiency:Symptoms To Watch Out For",
                    CarosuelImage="VitaminsD.png",
                    Content="People who have darker skin are at a greater risk of suffering from a viatmin D deficiency as their body doesn't absorb adequate sunlight.people who are obese or overweight.",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new HealthBlogModel
                {
                    Header="All You Need To Know About Seasonal Asthma Triggers",
                    CarosuelImage="Asthma.jpg",
                    Content="Asthma is a chronic respiratory illness that causes the narrowing of the airways due to inflammation this leads to symptoms like wheezing persistent coughing, breathlessness and more.",
                    Image2="whatsapp.png",
                    Date="Dr.Talaviya"
                },
                new HealthBlogModel
                {
                    Header="Confuced About Orthodontic Treatment With Clear Aligners?",
                    CarosuelImage="dentist.png",
                    Content="A successful orthodontic treatment begins with a proper diagnosis which involves a patient's full oral examination,studying the patieny's Database conducting dental  x-rays and making plaster models of the teeth.",
                    Image2="whatsapp.png",
                    Date="Dr.patel"
                }
            };
        }
    }
}
