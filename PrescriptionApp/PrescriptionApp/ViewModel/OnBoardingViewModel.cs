﻿using Prescription.Model;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.View;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Prescription.ViewModel
{
    public class OnBoardingViewModel : BaseViewModel
    {
        ObservableCollection<OnBoardingModel> _onBoardingsData { get; set; } = new ObservableCollection<OnBoardingModel>();

        public ObservableCollection<OnBoardingModel> OnBoardingsData
        {
            get => _onBoardingsData;
            set
            {
                _onBoardingsData = value;
                SetPropertyChanged(nameof(OnBoardingsData));
            }
        }
        int _currentIndex { get; set; }
        public int CurrentIndex
        {
            get => _currentIndex;
            set
            {

                _currentIndex = value;
                SetPropertyChanged(nameof(CurrentIndex));
            }
        }

        string _buttonName { get; set; }
        public string ButtonName
        {
            get => _buttonName;
            set
            {
                _buttonName = value;
                SetPropertyChanged(nameof(ButtonName));
            }
        }

        string _skip { get; set; }
        public bool ButtonIsVisible { get; set; }

        public string Skip
        {
            get => _skip;
            set
            {
                _skip = value;
                SetPropertyChanged(nameof(Skip));
            }
        }
        public ICommand LabeNavigation => new Command(LabelNavigate);
        public void LabelNavigate()
        {
            //Application.Current.MainPage = new NavigationPage(new SignUpPage());
            Application.Current.MainPage = new NavigationPage(new SignUpPage());
        }
        public ICommand button => new Command(ButtonNext);
        public void ButtonNext()
        {
            if (CurrentIndex == 2)
            {
                Settings.IsFirstTime= false;
                Application.Current.MainPage = new NavigationPage(new SignUpPage());
                // Application.Current.MainPage = new NavigationPage(new SignUpPage());
            }
            if (CurrentIndex < 2)
            {
                CurrentIndex++;
                if (CurrentIndex == 2)
                {     
                    Skip = string.Empty;
                    ButtonName = "Get Started";
                }
            }
        }


        public OnBoardingViewModel()
        {
            
            CurrentIndex = 0;
            Skip = "Skip >";
            ButtonName = "NEXT";
            OnBoardingsData = new ObservableCollection<OnBoardingModel>
            {
                new OnBoardingModel
                {
                    CarosuelImage ="onboarding_doctor.jpg",
                    CarosuelTitle="Book The Appointment",
                    CarosuelDescription="Find & book appointment with Doctors , clinics, Hospitals & Diagnostic Tests",
                },
                new OnBoardingModel
                {
                    CarosuelImage ="OnBoardingmedical.png",
                    CarosuelTitle="Online Consultation",
                    CarosuelDescription="Select the specialist and make an Consult through our app! ",

                },
                  new OnBoardingModel
                {
                    CarosuelImage ="OnBoardingpharmacy.png",
                    CarosuelTitle="Online Pharmacy",
                    CarosuelDescription="Order any medicine or health product and we'll deliver it for free. Enjoy discounts on everythink",                      
                

                },
            };




        }
    }
}