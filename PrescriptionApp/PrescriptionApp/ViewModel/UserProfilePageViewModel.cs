﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
    public class UserProfilePageViewModel : BaseViewModel
    {
        UserProfileModel _userprofileDetail { get; set; }
        public UserProfileModel UserProfileDetails
        {
            get => _userprofileDetail;
            set
            {
                _userprofileDetail = value;
                SetPropertyChanged(nameof(UserProfileDetails));
            }
        }

        public int UserId { get; set; }
        public int DoctorId { get; set; }
        public int PharmaId { get; set; }

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }
        public bool _pharamProfileVisible { get; set; } = true;
        public bool PharamProfileVisible
        {
            get => _pharamProfileVisible;
            set { _pharamProfileVisible = value; SetPropertyChanged(nameof(PharamProfileVisible)); }
        }

        string _userProfileTitle { get; set; }
        public string UserProfileTitle
        {
            get => _userProfileTitle;
            set { _userProfileTitle = value; SetPropertyChanged(nameof(UserProfileTitle)); }
        }
        public PrescriptionModel prescription { get; set; }
        
        public ICommand BackCommand => new Command(Back);
        public ICommand NotesCommand => new Command(CheckNotes);
        public ICommand AppointmentCommand => new Command(Appointment);
        public ICommand PrescriptionsSentCommand => new Command(PrescriptionsSent);
        public UserProfilePageViewModel()
        {

        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            if (PharmaId != 0)
            {
                PharamProfileVisible = false;
                UserId = PharmaId;
            }
            LoadData();
        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        async void CheckNotes()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            //PharamProfileVisible = false;
            var page = new NotesPage();
            if (UserType==UserType.Doctor)
            {
                page.ViewModel.CustomerId = UserId;
                page.ViewModel.DoctorId = Settings.UserId;

            }
            else if (UserType == UserType.Patient)
            {
                page.ViewModel.CustomerId = Settings.UserId;
                page.ViewModel.DoctorId = UserId;
            }
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        async void Appointment()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new AppointmentByPatient();            
            page.ViewModel.UserId = UserId;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        async void PrescriptionsSent()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new PrescriptionViewPage();
            var page1 = new PrescriptiontforPharmacy();

            if (UserType == UserType.Patient)
            {
                
                page.ViewModel.UserId = UserId;
                await Application.Current.MainPage.Navigation.PushAsync(page);
                 
            }
            else if (UserType == UserType.Doctor)
            {
                
                page.ViewModel.UserId = UserId;
                await Application.Current.MainPage.Navigation.PushAsync(page);
                 
            }
            else
            { 
                page1.ViewModel.UserId = UserId;
                await Application.Current.MainPage.Navigation.PushAsync(page1);
                
            }

            IsBusy = false;
        }

        public async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetUserDetailsById(new GetUserDetailByIdInputModel() { UserId = UserId });
                        if (result != null && result.Status && result.Profile != null)
                        {
                            UserProfileDetails = result.Profile;                             
                        }

                        else if (result != null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }
            catch (Exception e)
            {
                //await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");
            }
        }

    }
}
