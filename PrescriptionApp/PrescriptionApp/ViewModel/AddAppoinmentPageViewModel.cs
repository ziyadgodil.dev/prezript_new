﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace PrescriptionApp.ViewModel
{
    public class AddAppoinmentPageViewModel : BaseViewModel
    {

        ObservableCollection<DoctorsModel> _doctorList { get; set; }
        public ObservableCollection<DoctorsModel> DoctorList
        {
            get => _doctorList;
            set
            {
                _doctorList = value;
                SetPropertyChanged(nameof(DoctorList));
            }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                SearchList();
            }
        }

        ObservableCollection<DoctorsModel> _allDoctorList { get; set; }
        public ObservableCollection<DoctorsModel> AllDoctorList
        {
            get => _allDoctorList;
            set
            {
                _allDoctorList = value;
                SetPropertyChanged(nameof(AllDoctorList));
            }
        }

        ObservableCollection<SpecialtyModel> _specialistList { get; set; }

        public ObservableCollection<SpecialtyModel> SpecialistList
        {
            get => _specialistList;
            set
            {
                _specialistList = value;
                SetPropertyChanged(nameof(SpecialistList));
            }
        }

        public ICommand BackCommand => new Command(Back);
        public ICommand TypeSelectdCommand => new Command<SpecialtyModel>(TypeSelectd);
        public ICommand AppoinmentDetailsCommand => new Command<DoctorsModel>(AppoinmentDetails);


        public override void OnAppearing()
        {
            base.OnAppearing();

            LoadData();
            SearchList();
        }

        public AddAppoinmentPageViewModel()
        {

        }

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void LoadData()
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {

                        //Get all specialities
                        var Specialityresult = await App.PAServiceManager.GetAllSpeciality();

                        if (Specialityresult != null && Specialityresult.Data != null)
                        {
                            SpecialistList = Specialityresult.Data.Speciality;
                            SpecialistList.FirstOrDefault().IsSelected = true;
                        }
                        else if (Specialityresult == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", Specialityresult.Message, "Ok");

                        }

                        var Doctorresult = await App.PAServiceManager.GetAllDoctors();

                        if (Doctorresult != null && Doctorresult.Data != null)
                        {
                            DoctorList = Doctorresult.Data;
                            AllDoctorList = DoctorList;
                        }
                        else if (Doctorresult == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", Doctorresult.Message, "Ok");
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void AppoinmentDetails(DoctorsModel doctorsModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new DoctorsDetailsPage();
            page.ViewModel.DoctorId = doctorsModel.UserId;
            App.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }

        public void TypeSelectd(SpecialtyModel specialistType)
        {
            SpecialistList.ToList().ForEach(x => x.IsSelected = false);
            specialistType.IsSelected = true;

            if (specialistType.SpecialityId == 1)
            {
                DoctorList = AllDoctorList;
                return;
            }

            DoctorList = new ObservableCollection<DoctorsModel>(AllDoctorList.Where(x => x.SpecialityId == specialistType.SpecialityId));

        }

        void SearchList()
        {
            if (string.IsNullOrEmpty(SearchText))
            {
                DoctorList = AllDoctorList;
            }
            else
            {
                DoctorList = new ObservableCollection<DoctorsModel>(DoctorList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));
            }
        }

    }
}
