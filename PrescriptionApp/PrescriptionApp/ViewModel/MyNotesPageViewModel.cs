﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using PrescriptionApp.View.Popup;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{

    public class MyNotesPageViewModel : BaseViewModel
    {
        ObservableCollection<MyNotesModel> _myNotesList { get; set; }
        public ObservableCollection<MyNotesModel> MyNotesList
        {
            get => _myNotesList;
            set { _myNotesList = value; SetPropertyChanged(nameof(MyNotesList)); }
        }

        public ObservableCollection<MyNotesModel> AllMyNotesList { get; set; }

        UserType _userType { get; set; } = Settings.UserType;
        public UserType UserType
        {
            get => _userType;
            set { _userType = value; SetPropertyChanged(nameof(UserType)); }
        }

        string _searchText { get; set; }
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                SetPropertyChanged(nameof(SearchText));
                Search();
            }
        }
        public int customerIdpopup { get; set; }
        public ObservableCollection<NotesModel> AllListNotes { get; set; }
        public ICommand BackCommand => new Command(Back);

        public ICommand NotesDetailsCommand => new Command<MyNotesModel>(NotesDetails);
        public ICommand AddCommand => new Command(Add);
        public MyNotesPageViewModel()
        {

        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
           
        }
        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void NotesDetails(MyNotesModel myNotesModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new NotesPage();
            page.ViewModel.CustomerId = myNotesModel.CustomerId;
            customerIdpopup = myNotesModel.CustomerId;
            page.ViewModel.DoctorId = myNotesModel.DoctorId;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }

        public async void LoadData() //get Notesdetails
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetAllNotes();

                        if (result != null && result.Data != null)
                        {
                            MyNotesList = result.Data;
                            AllMyNotesList = MyNotesList;
                            Search();
                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        void Search()
        {
            if (string.IsNullOrEmpty(SearchText))
            {
                MyNotesList = AllMyNotesList;
            }
            else
            {
                MyNotesList = new ObservableCollection<MyNotesModel>(MyNotesList.Where(x => x.FullName.ToLower().Contains(SearchText.ToLower())));

            }
        }
        public async void Add()
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new AddNotePopup();
            page.IsEdit = true;
            page.Note = new NotesModel();
            page.Note.UserId = customerIdpopup;
            await PopupNavigation.Instance.PushAsync(page);
            IsBusy = false;
            page.SelectionHandler += (object sender, NotesModel notesModel) =>
            {
                if (AllListNotes == null)
                    AllListNotes = new ObservableCollection<NotesModel>();
                AllListNotes.Add(notesModel);
               // ListNotes = AllListNotes;
            };
        }
    }
}
