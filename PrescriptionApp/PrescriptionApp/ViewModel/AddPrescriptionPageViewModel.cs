﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Application = Xamarin.Forms.Application;


namespace PrescriptionApp.ViewModel
{
    public class AddPrescriptionPageViewModel : BaseViewModel
    {
        public ICommand BackCommand => new Command(Back);

        ObservableCollection<string> _diseasesList { get; set; }
        public ObservableCollection<string> DiseasesList
        {
            get => _diseasesList;
            set
            {
                _diseasesList = value;
                SetPropertyChanged(nameof(DiseasesList));
            }
        }

        DoctorsModel _doctorDetails { get; set; }
        public DoctorsModel DoctorDetails
        {
            get => _doctorDetails;
            set
            {
                _doctorDetails = value;
                SetPropertyChanged(nameof(DoctorDetails));
            }
        }

        bool _isSelectDocVisible { get; set; }
        public bool IsSelectDocVisible
        {
            get => _isSelectDocVisible;
            set
            {
                _isSelectDocVisible = value;
                SetPropertyChanged(nameof(IsSelectDocVisible));
            }
        }

        public bool IsButtonVisible
        {
            get
            {
                if (string.IsNullOrEmpty(DescriptionText) || string.IsNullOrEmpty(SelectedDisease))
                    return false;
                else
                    return true;
            }
        }

        string _selectedDisease { get; set; }
        public string SelectedDisease
        {
            get => _selectedDisease;
            set
            {
                _selectedDisease = value;
                SetPropertyChanged(nameof(SelectedDisease));
                SetPropertyChanged(nameof(IsButtonVisible));
            }
        }

        string _descriptionText { get; set; }
        public string DescriptionText
        {
            get => _descriptionText;
            set
            {
                _descriptionText = value;
                SetPropertyChanged(nameof(DescriptionText));
                SetPropertyChanged(nameof(IsButtonVisible));
            }
        }

        public int SpecialistId { get; set; }

        public ICommand RequestPrescriptionCommand => new Command(RequestPrescription);

        public AddPrescriptionPageViewModel()
        {
            IsSelectDocVisible = true;

            DiseasesList = new ObservableCollection<string>()
            {
                "Cold",
                "Fever",
                "Diabetes",
                "Flu",
                "Covid"
            };
        }

        private async void RequestPrescription()
        {
            try
            {
                if (string.IsNullOrEmpty(SelectedDisease))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please select Disease.", "Ok");
                    //UserDialogs.Instance.Toast("Please select Disease.");
                    return;
                }
                else if (string.IsNullOrEmpty(DescriptionText))
                {
                    await Application.Current.MainPage.DisplayAlert("Alert", "Please brief description of illness.", "Ok");
                    return;
                }
                else
                {
                    using (UserDialogs.Instance.Loading("Loading.."))
                    {
                        if (App.IsConnected)
                        {

                            var RequestPrecriptionData = new RequestPrecriptionModel()
                            {
                                SpecialistId = DoctorDetails.UserId,
                                Disease = SelectedDisease,
                                Description = DescriptionText,
                            };


                            var result = await App.PAServiceManager.RequestPrescription(RequestPrecriptionData);

                            if (result != null && result.Status)
                            {
                                await Application.Current.MainPage.DisplayAlert("Success", "Prescription sent successfully", "Ok");
                                //await Application.Current.MainPage.Navigation.PopAsync(true);
                                //await Application.Current.MainPage.Navigation.PopAsync(true);
                                await Application.Current.MainPage.Navigation.PushAsync(new DashboardPage());
                            }

                            else if (result != null)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", ServiceConfiguration.CommonErrorMessage, "Ok");
                            }
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                        }

                    }
                }

            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "Ok");

            }

        }

        public void Back()
        {
            Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
