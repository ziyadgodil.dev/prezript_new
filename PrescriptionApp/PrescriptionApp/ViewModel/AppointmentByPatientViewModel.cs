﻿using Acr.UserDialogs;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using PrescriptionApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrescriptionApp.ViewModel
{
   public  class AppointmentByPatientViewModel :BaseViewModel
    {
        public int  UserId { get; set; }
        ObservableCollection<AppoinmentsModel> _appoinmentsList { get; set; }
        public ObservableCollection<AppoinmentsModel> AppoinmentsList
        {
            get => _appoinmentsList;
            set { _appoinmentsList = value; SetPropertyChanged(nameof(AppoinmentsList)); }
        }
        public ICommand BackCommand => new Command(Back);
        public ICommand AppoinmentDetailsCommand => new Command<AppoinmentsModel>(AppoinmentDetails);

        public async void Back()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        public override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();            
        }
        public async void AppoinmentDetails(AppoinmentsModel appoinmentsModel)
        {
            if (IsBusy)
            {
                return;
            }
            else
            {
                IsBusy = true;
            }
            var page = new AppoinmentDetailsPage();

            page.ViewModel.AppointmentUserId = appoinmentsModel.UserId;
            await Application.Current.MainPage.Navigation.PushAsync(page);
            IsBusy = false;
        }
        public async void LoadData() //get appoinmentdetails
        {
            try
            {
                using (UserDialogs.Instance.Loading("Loading.."))
                {
                    if (App.IsConnected)
                    {
                        var result = await App.PAServiceManager.GetAppointmentByPatient(new AppointmentByPatientInput() { UserId = UserId });

                        if (result != null && result.Data != null)
                        {
                            AppoinmentsList = result.Data;

                        }
                        else if (result == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.Message, "Ok");
                        }
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert", "Check Internet Connection", "Ok");
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
