﻿using System;
using Xamarin.Forms;

namespace PrescriptionApp.Helper.Base
{
    public class BaseContentPage<T> : MainBaseContentPage where T : BaseViewModel, new()
    {
        protected T _viewModel;

        public T ViewModel => _viewModel ?? (_viewModel = new T());

        ~BaseContentPage()
        {
            _viewModel = null;
        }

        public BaseContentPage()
        {
            BindingContext = ViewModel;
        }

        protected override void OnAppearing()
        {
            ViewModel.OnAppearing();
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            ViewModel.OnDisappearing();
            base.OnDisappearing();
        }
    }

    public class MainBaseContentPage : ContentPage
    {
        public Color BarTextColor
        {
            get;
            set;
        }

        public Color BarBackgroundColor
        {
            get;
            set;
        }

        public MainBaseContentPage()
        {
            //BarBackgroundColor = Color.White;
            //BarTextColor = Colors.NavigationBarBackgroundColor;
            //BackgroundColor = Color.White;
            //Visual = VisualMarker.Material;
            //if (Parent is NavigationPage)
            //{
            //    var nav = (NavigationPage)Parent;
            //    nav.BarBackgroundColor = BarBackgroundColor;
            //    nav.BarTextColor = BarTextColor;
            //}
        }

        public bool HasInitialized
        {
            get;
            private set;
        }

        protected virtual void OnLoaded()
        {
        }

        protected virtual void Initialize()
        {
        }


        protected override void OnAppearing()
        {
            if (!HasInitialized)
            {
                HasInitialized = true;
                OnLoaded();
            }
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

    }
}
