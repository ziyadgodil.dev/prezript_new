﻿using Newtonsoft.Json;
using Plugin.Settings;
using PrescriptionApp.Helper.Resources;
using Xamarin.Essentials;

namespace PrescriptionApp.Helper.Base
{
    public static class Settings
    {
        public static bool NewToken
        {
            get => Preferences.Get(nameof(NewToken), false);

            set => Preferences.Set(nameof(NewToken), value);
        }

        public static bool NewLogin
        {
            get => Preferences.Get(nameof(NewLogin), false);

            set => Preferences.Set(nameof(NewLogin), value);
        }

        public static string UserToken
        {
            get => Preferences.Get(nameof(UserToken), string.Empty);

            set => Preferences.Set(nameof(UserToken), value);
        }

        public static bool IsFirstTime
        {
            get => Preferences.Get(nameof(IsFirstTime), true);

            set => Preferences.Set(nameof(IsFirstTime), value);
        }

        public static string FCMToken
        {
            get => Preferences.Get(nameof(FCMToken), string.Empty);

            set => Preferences.Set(nameof(FCMToken), value);
        }

        public static bool UserLoggedIn
        {
            get => Preferences.Get(nameof(UserLoggedIn), false);

            set => Preferences.Set(nameof(UserLoggedIn), value);
        }

        public static int UserId
        {
            get => Preferences.Get(nameof(UserId), 0);

            set => Preferences.Set(nameof(UserId), value);
        }

        public static UserType UserType
        {
            get => JsonConvert.DeserializeObject<UserType>(Preferences.Get(nameof(UserType), null));

            set => Preferences.Set(nameof(UserType), JsonConvert.SerializeObject(value));
        }

        public static string DeviceType
        {
            get => Preferences.Get(nameof(DeviceType), string.Empty);

            set => Preferences.Set(nameof(DeviceType), value);
        }

        public static string FirstName
        {
            get => Preferences.Get(nameof(FirstName), string.Empty);

            set => Preferences.Set(nameof(FirstName), value);
        }

        public static string LastName
        {
            get => Preferences.Get(nameof(LastName), string.Empty);

            set => Preferences.Set(nameof(LastName), value);
        }

        public static string UserImage
        {
            get => Preferences.Get(nameof(UserImage), string.Empty);

            set => Preferences.Set(nameof(UserImage), value);
        }

        public static string Email
        {
            get => Preferences.Get(nameof(Email), string.Empty);

            set => Preferences.Set(nameof(Email), value);
        }

        public static string MobileNo
        {
            get => Preferences.Get(nameof(MobileNo), string.Empty);

            set => Preferences.Set(nameof(MobileNo), value);
        }
        public static string Address
        {
            get => Preferences.Get(nameof(Address), string.Empty);

            set => Preferences.Set(nameof(Address), value);
        }
        public static string Latitude
        {
            get => Preferences.Get(nameof(Latitude), string.Empty);

            set => Preferences.Set(nameof(Latitude), value);
        }

        public static string Longitude
        {
            get => Preferences.Get(nameof(Longitude), string.Empty);

            set => Preferences.Set(nameof(Longitude), value);
        }


    }
}
