﻿namespace PrescriptionApp.Helper.Resources
{
    public static class FontSize
    {
        public static readonly double XXLargeFont = 30;
        public static readonly double PageTitle = 20;
        public static readonly double XLargeFont = 22;
        public static readonly double LargeFont = 20;
        public static readonly double EighteenFont = 18;
        public static readonly double MediumFont = 16;
        public static readonly double FourteenFont = 14;
        public static readonly double SmallFont = 13;
        public static readonly double TwelveFont = 12;
        public static readonly double MicroFont = 11;
        public static readonly double TenFont = 10;
        public static readonly double ExtraMicroFont = 9;
    }
}
