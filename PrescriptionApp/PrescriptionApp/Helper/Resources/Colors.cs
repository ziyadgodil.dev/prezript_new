﻿using Xamarin.Forms;

namespace PrescriptionApp.Helper.Resources
{
    public static class Colors
    {
        public static readonly Color BilobaFlower = Color.FromHex("#BA8CF0");
        public static readonly Color KingfisherDaisy = Color.FromHex("#512195");
        public static readonly Color WhiteSmoke = Color.FromHex("#F5F5F5");
        public static readonly Color Amethyst = Color.FromHex("#8B5EBF");
        public static readonly Color Black = Color.FromHex("#000000");
        public static readonly Color Whisper = Color.FromHex("#EDEDED");
        public static readonly Color LightGray = Color.FromHex("#707070");
        public static readonly Color Gainsboro = Color.FromHex("#DDDDDD");
        public static readonly Color Gray = Color.FromHex("#404040");
        public static readonly Color White = Color.FromHex("#FFFFFF"); 
        public static readonly Color DimGray = Color.FromHex("#6A6A6A");
        public static readonly Color PendingAlpha = Color.FromHex("#26FFA500");
        public static readonly Color ApproveAlpha = Color.FromHex("#26008000");
        public static readonly Color RejectAlpha = Color.FromHex("#26FF0000");
        public static readonly Color Pending = Color.FromHex("#FFA500");
        public static readonly Color Approve = Color.FromHex("#008000");
        public static readonly Color Reject = Color.FromHex("#FF0000");
        public static readonly Color Transparent = Color.FromHex("#ffffff");
        public static readonly Color DarkRed = Color.FromHex("#A30000");
        public static readonly Color ForestGreen = Color.FromHex("#339933");
        public static readonly Color purpal = Color.FromHex("#bec8f7");
        //public static readonly Color purpal50 = Color.FromHex("#5060b4");
        public static readonly Color pupal9b= Color.FromHex("9ba8eb");
        public static readonly Color purpal501 = Color.FromHex("#EAEBF4");
        //changes of color#B760E6
        public static readonly Color purpal50 = Color.FromHex("#BA8CF0");
        public static readonly Color purpal51 = Color.FromHex("#B760E6");

    }
}
