﻿using System;
namespace PrescriptionApp.Helper.Resources
{
    public enum UserType
    {
        Patient = 1, Doctor, Pharma
    }
    public enum PrescriptionApproval
    {
        Pending, SentToCustomer, SentToPharmacy, Accept, Reject
    }
    public enum AppointmentStatus
    {
        Pending, Approved, Rejected, Canceled, Completed
    }
    public enum NotificationTypes
    {
        Alert, Appointment,Prescription
    }
}
