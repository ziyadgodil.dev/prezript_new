﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Draftsy.helpers
{
    public class CircularImage : Frame
    {
        CachedImage image = new CachedImage();

        public double Size
        {
            set
            {
                HeightRequest = value;
                WidthRequest = value;
                CornerRadius = (float)(value / 2);
            }
        }

        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(ImageSource), typeof(CircularImage));
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set
            {
                SetValue(SourceProperty, value);
                image.Source = Source;
            }
        }

        public CircularImage()
        {
            HasShadow = false;
            Padding = new Thickness(0);
            BorderColor = (Color)Application.Current.Resources["lightBlue"];
            HorizontalOptions = LayoutOptions.Center;
            VerticalOptions = LayoutOptions.Center;
            BackgroundColor = Color.Transparent;

            image.ErrorPlaceholder = (String)Application.Current.Resources["placeholder"];
            image.LoadingPlaceholder = (String)Application.Current.Resources["placeholder"];
            image.BackgroundColor = Color.Transparent;
            image.HorizontalOptions = LayoutOptions.FillAndExpand;
            image.VerticalOptions = LayoutOptions.FillAndExpand;
            image.DownsampleToViewSize = true;
            image.Aspect = Aspect.AspectFill;
            image.Transformations = new List<FFImageLoading.Work.ITransformation>
            {
                new CircleTransformation(),
            };

            Content = image;
        }
    }
}
