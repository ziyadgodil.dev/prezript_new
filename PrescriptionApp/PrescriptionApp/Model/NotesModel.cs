﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;


namespace PrescriptionApp.Model
{
    public class NotesResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<NotesModel> Data { get; set; }


    }

    public class AddNotesResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public NotesModel Note { get; set; }
    }

    public class AllNotesByIdInputModel : BaseModel
    {

        [JsonProperty("PatientId")]
        public int PatientId { get; set; }

        [JsonProperty("DoctorId")]
        public int DoctorId { get; set; }
    }

    //public class AddNotesInputModel : BaseModel
    //{

    //    [JsonProperty("NoteId")]
    //    public int NoteId { get; set; }

    //    string _title { get; set; }
    //    [JsonProperty("Title")]
    //    public string Title
    //    {
    //        get => _title;
    //        set
    //        {
    //            _title = value;
    //            SetPropertyChanged(nameof(Title));
    //        }
    //    }

    //    string _description { get; set; }
    //    [JsonProperty("Description")]
    //    public string Description 
    //    {
    //        get => _description;
    //        set
    //        {
    //            _description = value;
    //            SetPropertyChanged(nameof(Description));
    //        }
    //    }
    //    bool _isPrivate { get; set; }
    //    [JsonProperty("IsPrivate")]
    //    public bool IsPrivate 
    //    {
    //        get => _isPrivate;
    //        set
    //        {
    //            _isPrivate = value;
    //            SetPropertyChanged(nameof(IsPrivate));
    //        }
    //    }

    //    [JsonProperty("UserId")]
    //    public int UserId { get; set; }
    //}


    public class NotesModel : BaseModel
    {
        [JsonProperty("noteId")]
        public int NoteId { get; set; }

        [JsonIgnore]
        string _title { get; set; }
        [JsonProperty("title")]
        public string Title
        {
            get => _title;
            set { _title = value; SetPropertyChanged(nameof(Title)); }
        }

        [JsonIgnore]
        string _description { get; set; }
        [JsonProperty("description")]
        public string Description
        {
            get => _description;
            set { _description = value; SetPropertyChanged(nameof(Description)); }
        }

        [JsonProperty("isPrivate")]
        public bool IsPrivate { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("doctorId")]
        public int DoctorId { get; set; }

        [JsonProperty("createdDate")]
        public DateTime Date { get; set; }

        [JsonIgnore]
        public string Lockimge { get => IsPrivate ? "lockrounded" : ""; }

        [JsonIgnore]
        public DateTime CreatedDate => Date.ToLocalTime();


    }
    public class GetUserNotesByIdInputModel
    {
        [JsonProperty("UserId")]
        public int UserId { get; set; }
    }

}
