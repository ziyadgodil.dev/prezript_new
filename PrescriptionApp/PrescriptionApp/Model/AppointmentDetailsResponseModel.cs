﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class AppointmentDetailsResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public AppoinmentsModel Data { get; set; }
    }
    //public partial class AppointmentDetail : BaseModel
    //{
    //    [JsonProperty("appointmentId")]
    //    public int AppointmentId { get; set; }

    //    [JsonProperty("contactNo")]
    //    public string ContactNo { get; set; }

    //    [JsonProperty("aboutMe")]
    //    public string AboutMe { get; set; }

    //    [JsonProperty("address")]
    //    public string Address { get; set; }

    //    [JsonProperty("image")]
    //    public string Image { get; set; }
    //    public string AppoinmentImage { get => string.IsNullOrEmpty(Image) ? "usera" : Image; }

    //    [JsonProperty("firstName")]
    //    public string FirstName { get; set; }

    //    [JsonProperty("lastName")]
    //    public string LastName { get; set; }

    //    [JsonIgnore]
    //    public string FullName { get => FirstName + " " + LastName; }

    //    [JsonProperty("email")]
    //    public string Email { get; set; }

    //    [JsonProperty("degree")]
    //    public string Degree { get; set; }

    //    [JsonProperty("appoinmentdate")]
    //    public string  Appoinmentdate { get; set; }

    //    [JsonProperty("createdDate")]
    //    public string  CreatedDate { get; set; }

    //    [JsonProperty("reason")]
    //    public string Reason { get; set; }

    //    [JsonProperty("status")]
    //    public AppointmentStatus Status { get; set; }
    //}

    public class AppointmentDetailsInputModel
    {
        [JsonProperty("appointmentId")]
        public int AppointmentId { get; set; }
    }
}
