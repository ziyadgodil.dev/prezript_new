﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{

    public class TypeModel : BaseModel
    {
        string _usertype { get; set; }
        public string UserType
        {
            get => _usertype;
            set { _usertype = value; SetPropertyChanged(nameof(UserType)); }
        }

        int _key { get; set; }
        public int Key
        {
            get => _key;
            set { _key = value; SetPropertyChanged(nameof(Key)); }
        }


    }

    public class RegistrationInputModel : BaseModel
    {
        [JsonIgnore]
        string _email { get; set; }

        [JsonProperty("email")]
        public string Email
        {
            get => _email;
            set { _email = value; SetPropertyChanged(nameof(Email)); }
        }

        [JsonIgnore]
        string _password { get; set; }
        [JsonProperty("password")]
        public string Password
        {
            get => _password;
            set { _password = value; SetPropertyChanged(nameof(Password)); }
        }

        [JsonIgnore]
        int _userTypeId { get; set; }
        [JsonProperty("type")]
        public int UserTypeId
        {

            get => _userTypeId;
            set { _userTypeId = value; SetPropertyChanged(nameof(UserTypeId)); }
        }

        [JsonIgnore]
        string _firstName { get; set; }
        [JsonProperty("FirstName")]
        public string FirstName
        {

            get => _firstName;
            set
            {
                _firstName = value; SetPropertyChanged(nameof(FirstName));
            }
        }
        string _lastName { get; set; }
        [JsonProperty("LastName")]
        public string LastName
        {

            get => _lastName;
            set
            {
                _lastName = value; SetPropertyChanged(nameof(LastName));
            }
        }
        string _contact { get; set; }
        [JsonProperty("mobileNo")]
        public string Contact
        {

            get => _contact;
            set
            {
                _contact = value; SetPropertyChanged(nameof(Contact));
            }
        }
        string _address { get; set; }
        [JsonProperty("location")]
        public string Address
        {

            get => _address;
            set
            {
                _address = value; SetPropertyChanged(nameof(Address));
            }
        }
        string _latitude { get; set; }
        [JsonProperty("latitude")]
        public string Latitude
        {

            get => _latitude;
            set
            {
                _latitude = value; SetPropertyChanged(nameof(Latitude));
            }
        }
        string _longitude { get; set; }
        [JsonProperty("longitude")]
        public string Longitude
        {

            get => _longitude;
            set
            {
                _longitude = value; SetPropertyChanged(nameof(Longitude));
            }
        }

    }
    public class RegistrationResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public DataDetail DataDetail { get; set; }

    }
}
