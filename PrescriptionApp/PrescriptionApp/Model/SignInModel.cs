﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Globalization;

namespace PrescriptionApp.Model
{
    public class LoginInputModel : BaseModel
    {
        [JsonProperty("MobileNo")]
        public string MobileNo { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }
    }
    public class LoginResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public DataDetail DataDetail { get; set; }

    }
    public partial class DataDetail
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("imageURL")]
        public string ImageUrl { get; set; }

        [JsonProperty("nationalId")]
        public string NationalId { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("userTypeId")]
        public UserType UserTypeId { get; set; }

        [JsonProperty("userTypeName")]
        public string UserTypeName { get; set; }

        [JsonProperty("contactNo")]
        public string ContactNo { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("isProfileUpdated")]
        public bool ProfileUpdated { get; set; }
         
    }
}
