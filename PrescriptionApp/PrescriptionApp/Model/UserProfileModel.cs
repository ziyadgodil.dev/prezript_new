﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{
    public class UserProfileModel : BaseModel
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonIgnore]
        string _firstName { get; set; }
        [JsonProperty("firstName")]
        public string FirstName
        {
            get => _firstName;
            set { _firstName = value; SetPropertyChanged(nameof(FirstName)); SetPropertyChanged(nameof(FullName)); }
        }

        [JsonIgnore]
        string _lastName { get; set; }
        [JsonProperty("lastName")]
        public string LastName
        {
            get => _lastName;
            set { _lastName = value; SetPropertyChanged(nameof(LastName)); SetPropertyChanged(nameof(FullName)); }
        }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonIgnore]
        public string FullName { get => FirstName + " " + LastName; }

        [JsonIgnore]
        string _email { get; set; }
        [JsonProperty("email")]
        public string Email
        {
            get => _email;
            set
            {
                _email = value; SetPropertyChanged(nameof(Email));
            }
        }

        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("patientId")]
        public int PatientId { get; set; }

        [JsonIgnore]
        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); SetPropertyChanged(nameof(UserProfleImage)); }
        }

        [JsonIgnore]
        public string UserProfleImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }
        string _userAddress { get; set; }

        [JsonProperty("address")]
        public string UserAddress
        {
            get => _userAddress;
            set { _userAddress = value; SetPropertyChanged(nameof(UserAddress)); }
        }

        [JsonProperty("contactNo")]
        public string Contact { get; set; }

        [JsonIgnore]
        string _aboutMe { get; set; }
        [JsonProperty("aboutMe")]
        public string AboutMe
        {
            get => _aboutMe;
            set { _aboutMe = value; SetPropertyChanged(nameof(AboutMe)); }
        }

        [JsonProperty("notes")]
        public NotesModel Notes { get; set; }

        [JsonProperty("appointmentId")]
        public int AppointmentId { get; set; }

        [JsonProperty("degree")]
        public string Degree { get; set; }

        [JsonProperty("appoinmentdate")]
        public DateTime Appoinmentdate { get; set; }

        [JsonProperty("nationalId")]
        public string NationalId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("openingTime")]
        public TimeSpan? OpeningTime { get; set; }

        [JsonIgnore]
        public DateTime? OpenigTimeText => DateTime.Now.Date + OpeningTime.Value;

        [JsonProperty("closingTime")]
        public TimeSpan? ClosingTime { get; set; }

        [JsonIgnore]
        public DateTime? ClosingTimeText => DateTime.Now.Date + ClosingTime.Value;

        [JsonIgnore]
        SpecialtyModel _speciality { get; set; }
        [JsonProperty("speciality")]
        public SpecialtyModel Speciality
        {
            get => _speciality;
            set { _speciality = value; SetPropertyChanged(nameof(Speciality)); }
        }

        [JsonProperty("businessRegistrationNumber")]
        public string BusinessRegistrationNumber { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        string _latitude { get; set; }
        [JsonProperty("latitude")]
        public string Latitude
        {

            get => _latitude;
            set
            {
                _latitude = value; SetPropertyChanged(nameof(Latitude));
            }
        }
        string _longitude { get; set; }
        [JsonProperty("longitude")]
        public string Longitude
        {

            get => _longitude;
            set
            {
                _longitude = value; SetPropertyChanged(nameof(Longitude));
            }
        }


    }


}
