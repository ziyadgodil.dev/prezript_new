﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class AddEditmedicineModel : BaseModel
    {
        [JsonProperty("medicineId")]
        public int MedicineId { get; set; }

        [JsonProperty("prescriptionId")]
        public int PrescriptionId { get; set; }
        [JsonIgnore]
        string _drugsName { get; set; }
        [JsonProperty("drugName")]
        public string DrugsName
        {
            get => _drugsName;
            set
            {
                _drugsName = value;
                SetPropertyChanged(nameof(DrugsName));
            }
        }
        [JsonIgnore]
        string _strength { get; set; }
        [JsonProperty("strength")]
        public string Strength
        {
            get => _strength;
            set
            {
                _strength = value;
                SetPropertyChanged(nameof(Strength));
            }
        }
        [JsonIgnore]
        string _strengthUnit { get; set; }

        [JsonProperty("unit")]
        public string StrengthUnit
        {
            get => _strengthUnit;
            set
            {
                _strengthUnit = value;
                SetPropertyChanged(nameof(StrengthUnit));
            }
        }
        [JsonIgnore]
        string _preparationValue { get; set; }
        [JsonProperty("preparation")]
        public string PreparationValue
        {
            get => _preparationValue;
            set
            {
                _preparationValue = value;
                SetPropertyChanged(nameof(PreparationValue));
            }
        }

        [JsonIgnore]
        string _routeValue { get; set; }
        [JsonProperty("route")]
        public string RouteValue
        {
            get => _routeValue;
            set
            {
                _routeValue = value;
                SetPropertyChanged(nameof(RouteValue));
            }
        }

        [JsonIgnore]
        string _directionValue { get; set; }
        [JsonProperty("direction")]
        public string DirectionValue
        {
            get => _directionValue;
            set
            {
                _directionValue = value;
                SetPropertyChanged(nameof(DirectionValue));
            }
        }

        [JsonIgnore]
        string _frequencyValue { get; set; }
        [JsonProperty("frequency")]
        public string FrequencyValue
        {
            get => _frequencyValue;
            set
            {
                _frequencyValue = value;
                SetPropertyChanged(nameof(FrequencyValue));
            }
        }
        [JsonIgnore]
        string _notes { get; set; }
        [JsonProperty("instruction")]
        public string Notes
        {
            get => _notes;
            set
            {
                _notes = value;
                SetPropertyChanged(nameof(Notes));
            }
        }


        private bool _medicinePlusVisible;
        public bool MedicinePlusVisible
        {
            get { return _medicinePlusVisible; }
            set
            {
                _medicinePlusVisible = value;
                SetPropertyChanged(nameof(MedicinePlusVisible));
            }

        }


    }

    public class AddMedicineResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public AddEditmedicineModel Medicine { get; set; }
    }
}


