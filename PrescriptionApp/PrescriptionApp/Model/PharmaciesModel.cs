﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{
    public class PharmaciesResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<PharmaciesModel> Data { get; set; }
    }

    public class PharmaciesModel : BaseModel
    {
        [JsonProperty("PharmacyId")]
        public int PharmacyId { get; set; }

        [JsonProperty("firstName")]
        public string UserFname { get; set; }

        [JsonProperty("pharmacyName")]
        public string PharmacyName { get; set; }

        [JsonIgnore]
        public string FullName { get { return UserFname + " " + UserLname; } }

        [JsonIgnore]
        public string Name => PharmacyName;

        [JsonProperty("lastName")]
        public string UserLname { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonIgnore]
        public string PharmaciesImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("contactNo")]
        public string Contact { get; set; }

        [JsonProperty("location")]
        public string GpsbaseLocation { get; set; }

        [JsonProperty("aboutMe")]
        public string About { get; set; }

        [JsonProperty("openingTime")]
        public TimeSpan? OpeningTime { get; set; }

        [JsonProperty("closingTime")]
        public TimeSpan? ClosingTime { get; set; }

        [JsonProperty("businessRegistrationNumber")]
        public string BusinessRegistrationNumber { get; set; }

    }
}
