﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class RequestPrecriptionModel : BaseModel
    {

        public int SpecialistId { get; set; }
        public string Description { get; set; }
        public string Disease { get; set; }


    }




    public class RequestPrescriptionResponseModel : BaseResponseModel
    {

    }
}
