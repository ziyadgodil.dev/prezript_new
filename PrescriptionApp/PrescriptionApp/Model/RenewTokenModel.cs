﻿using System;
using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using Xamarin.Essentials;

namespace PrescriptionApp.Model
{
    public class RenewTokenInputModel
    {
        public int userId = Settings.UserId;
    }

    public class RenewTokenResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public string NewToken { get; set; }
    }
}
