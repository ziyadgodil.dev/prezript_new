using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace PrescriptionApp.Model
{
    public class PrescriptionResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public PrescriptionData Data { get; set; }
    }

    public partial class PrescriptionData
    {
        [JsonProperty("received")]
        public ObservableCollection<PrescriptionModel> Pending { get; set; }

        [JsonProperty("Sent")]
        public ObservableCollection<PrescriptionModel> Sent { get; set; }

        [JsonProperty("request")]
        public ObservableCollection<PrescriptionModel> Requested { get; set; }

    }
    public class PrescriptionModel : BaseModel
    {
        [JsonProperty("prescriptionId")]
        public int PrescriptionId { get; set; }

        [JsonProperty("appointmentId")]
        public int AppointmentId { get; set; }

        [JsonProperty("doctorId")]
        public int DoctorId { get; set; }

        [JsonProperty("patientId")]
        public int PatientId { get; set; }

        [JsonProperty("pharmacyId")]
        public int PharmacyId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("patientName")]
        public string PatientName { get; set; }

        [JsonProperty("pharmacyName")]
        public string PharmacyName { get; set; }

        [JsonProperty("doctorImage")]
        public string DoctorImage { get; set; }

        [JsonProperty("patientImage")]
        public string PatientImage { get; set; }

        [JsonProperty("pharmacyImage")]
        public string PharmacyImage { get; set; }

        [JsonProperty("customerId")]
        public int CustomerId { get; set; }

        [JsonProperty("disease")]
        public string Disease { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("mobileNo")]
        public string MobileNo { get; set; }

        [JsonProperty("prescriptionDate")]
        public DateTime PrescriptionDate { get; set; }


        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("medicine")]
        public string Medicine { get; set; }

        [JsonProperty("status")]
        public PrescriptionApproval Status { get; set; }
        public string Reason { get; set; }
        public double Amount { get; set; }
        [JsonProperty("updateDate")]
        public DateTime UpdateDate { get; set; }
        public Color StatusColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy
                       || Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                       && Settings.UserType == UserType.Doctor)
                    return Colors.Approve;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma)
                    return Colors.Approve;

                else if (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Patient)
                    return Colors.Approve;

                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    || (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.Approve;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    || (Status == PrescriptionApproval.SentToCustomer  && Settings.UserType == UserType.Pharma)
                    || (Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor))
                    return Colors.Approve;

                else if (Status == PrescriptionApproval.Reject)
                    return Colors.Reject;
                else
                    return Colors.Pending;
            }
        }

        public Color StatusBackgroundColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy
                       || Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                       && Settings.UserType == UserType.Doctor)
                    return Colors.ApproveAlpha;

                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma)
                    return Colors.ApproveAlpha;

                else if (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Patient)
                    return Colors.ApproveAlpha;

                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    || (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.ApproveAlpha;
                else if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient)
                    || (Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma)
                    || (Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Doctor))
                    return Colors.ApproveAlpha;

                else if (Status == PrescriptionApproval.Reject)
                    return Colors.RejectAlpha;

                else
                    return Colors.PendingAlpha;
            }
        }

        public string StatusName
        {
            get
            {
                if ((Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                    && Settings.UserType == UserType.Doctor)
                    return "Sent";

                else if (Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    return "Received";                

                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma)
                    return "Received";

                else if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.Reject)
                    && Settings.UserType == UserType.Patient)
                    return "Reject By Pharmacy";
                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient)
                    return "Sent to Pharmacy";
                else if ((Status == PrescriptionApproval.SentToCustomer
                            || Status == PrescriptionApproval.Reject
                         || Status == PrescriptionApproval.SentToPharmacy) && Settings.UserType == UserType.Doctor)
                    return "Sent";
                

                else
                    return Enum.GetName(typeof(PrescriptionApproval), Status);
            }
        }
        //(Status == PrescriptionApproval.SentToCustomer|| Status == PrescriptionApproval.SentToPharmacy

        //[JsonIgnore]
        //ObservableCollection<MedicineModel> _medicine { get; set; }
        //[JsonProperty("medicine")]
        //public ObservableCollection<MedicineModel> Medicine
        //{
        //    get => _medicine;
        //    set { _medicine = value; SetPropertyChanged(nameof(Medicine)); }
        //}

        //[JsonProperty("doctor")]
        //public UserProfileModel Doctor { get; set; }

        //UserProfileModel _pharmacy { get; set; }
        //[JsonProperty("pharmacy")]
        //public UserProfileModel Pharmacy
        //{
        //    get => _pharmacy;
        //    set { _pharmacy = value; SetPropertyChanged(nameof(Pharmacy)); SetPropertyChanged(nameof(PharmacyNotnull)); }
        //}
        //public bool PharmacyNotnull
        //{
        //    get => Pharmacy != null;
        //}
        //[JsonProperty("customer")]
        //public UserProfileModel Customer { get; set; }

    }

}
