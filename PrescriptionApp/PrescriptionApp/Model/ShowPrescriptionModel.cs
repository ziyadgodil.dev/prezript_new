﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace PrescriptionApp.Model
{
    public class PrescriptionByIdInputModel
    {
        public int PrescriptionId { get; set; }
        public int DoctorId { get; set; }

    }

    public class PrescriptionByIdResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public PrescriptionDetail PrescriptionDetail { get; set; } 
    }

    public class PrescriptionDetail : BaseModel
    {
        [JsonProperty("prescriptionId")]
        public int PrescriptionId { get; set; }

        [JsonIgnore]
        string _disease { get; set; }
        [JsonProperty("disease")]
        public string Disease
        {
            get => _disease;
            set { _disease = value; SetPropertyChanged(nameof(Disease)); }
        }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public PrescriptionApproval Status { get; set; }

        [JsonProperty("prescriptionDate")]
        public DateTime PrescriptionDate { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("address")]
        public string  Address { get; set; }
        string _latitude { get; set; }
        [JsonProperty("latitude")]
        public string Latitude
        {

            get => _latitude;
            set
            {
                _latitude = value; SetPropertyChanged(nameof(Latitude));
            }
        }
        string _longitude { get; set; }
        [JsonProperty("longitude")]
        public string Longitude
        {

            get => _longitude;
            set
            {
                _longitude = value; SetPropertyChanged(nameof(Longitude));
            }
        }
        [JsonIgnore]
        public string FormatedAmount { get => string.Format("{0:0.00}", Amount); }


        [JsonProperty("deliveryOption")]
        public string  DeliveryOption { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        
        [JsonIgnore]
        ObservableCollection<AddEditmedicineModel> _medicine { get; set; }
        [JsonProperty("medicine")]
        public ObservableCollection<AddEditmedicineModel> Medicine
        {
            get => _medicine;
            set { _medicine = value; SetPropertyChanged(nameof(Medicine)); }
        }

        [JsonIgnore]
        Doctor _doctor { get; set; }

        [JsonProperty("doctor")]
        public Doctor Doctor
        {
            get => _doctor;
            set { _doctor = value; SetPropertyChanged(nameof(Doctor)); }
        }

        [JsonIgnore]
        Pharmacy _pharmacy { get; set; }
        [JsonProperty("pharmacy")]
        public Pharmacy Pharmacy
        {
            get => _pharmacy;
            set { _pharmacy = value; SetPropertyChanged(nameof(Pharmacy)); }
        }

        [JsonIgnore]
        Customer _customer { get; set; }
        [JsonProperty("customer")]
        public Customer Customer
        {
            get => _customer;
            set { _customer = value; SetPropertyChanged(nameof(Customer)); }
        }

        [JsonIgnore]
        public bool ReasonVisible => Status == PrescriptionApproval.Reject;
        
        [JsonIgnore]
        public bool AmountVisible => Status == PrescriptionApproval.Accept;
        

        public Color StatusColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma) || 
                        (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Doctor))
                    return Colors.Approve;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient) ||
                        (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.Approve;
                
                else if (Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.Accept)
                    return Colors.Approve;
                else if (Status == PrescriptionApproval.Reject)
                    return Colors.Reject;
                else
                    return Colors.Pending;
            }
        }

        public Color StatusBackgroundColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma)
                    return Colors.ApproveAlpha;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient) ||
                        (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.ApproveAlpha;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient) ||
                        (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.ApproveAlpha;
                else if (Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.Accept)
                    return Colors.ApproveAlpha;
                else if (Status == PrescriptionApproval.Reject)
                    return Colors.RejectAlpha;
                else
                    return Colors.PendingAlpha;
            }
        }

        public string StatusName
        {
            get
            {
                if (Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor)
                    return "Sent";
                else if (Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    return "Received";
                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient)
                    return "Sent";
                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma)
                    return "Received";
               
                else
                    return Enum.GetName(typeof(PrescriptionApproval), Status);
            }
        }

    }
    public class Doctor : BaseModel
    {
        [JsonIgnore]
        string _firstName { get; set; }
        [JsonProperty("firstName")]
        public string FirstName
        {
            get => _firstName;
            set { _firstName = value; SetPropertyChanged(nameof(FirstName)); SetPropertyChanged(nameof(FullName)); }
        }

        [JsonIgnore]
        string _lastName { get; set; }
        [JsonProperty("lastName")]
        public string LastName
        {
            get => _lastName;
            set { _lastName = value; SetPropertyChanged(nameof(LastName)); SetPropertyChanged(nameof(FullName)); }
        }

        public string FullName { get => FirstName + " " + LastName; }

        [JsonIgnore]
        string _contactNo { get; set; }
        [JsonProperty("contactNo")]
        public string ContactNo
        {
            get => _contactNo;
            set { _contactNo = value; SetPropertyChanged(nameof(ContactNo)); }
        }

        [JsonIgnore]
        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); SetPropertyChanged(nameof(UserProfleImage)); }
        }

        public string UserProfleImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }

        [JsonIgnore]
        string _degree { get; set; }
        [JsonProperty("degree")]
        public string Degree
        {
            get => _degree;
            set { _degree = value; SetPropertyChanged(nameof(Degree)); }
        }

        [JsonProperty("address")]
        public string Address { get; set; }
    }

    public class Pharmacy : BaseModel
    {
        [JsonProperty("pharmacyId")]
        public int PharmacyId { get; set; }
        string _pharmacyName { get; set; }
        [JsonProperty("pharmacyName")]
        public string PharmacyName
        {
            get => _pharmacyName;
            set { _pharmacyName = value; SetPropertyChanged(nameof(PharmacyName)); }
        }

        [JsonIgnore]
        string _contactNo { get; set; }
        [JsonProperty("contactNo")]
        public string ContactNo
        {
            get => _contactNo;
            set { _contactNo = value; SetPropertyChanged(nameof(ContactNo)); }
        }

        [JsonIgnore]
        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); SetPropertyChanged(nameof(UserProfleImage)); }
        }

        public string UserProfleImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }
        string _address { get; set; }
        [JsonProperty("address")]
        public string Address
        {
            get => _address;
            set { _address = value;SetPropertyChanged(nameof(Address)); }
        }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }
    }

    public class Customer : BaseModel
    {
        [JsonIgnore]
        string _firstName { get; set; }
        [JsonProperty("firstName")]
        public string FirstName
        {
            get => _firstName;
            set { _firstName = value; SetPropertyChanged(nameof(FirstName)); SetPropertyChanged(nameof(FullName)); }
        }

        [JsonIgnore]
        string _lastName { get; set; }
        [JsonProperty("lastName")]
        public string LastName
        {
            get => _lastName;
            set { _lastName = value; SetPropertyChanged(nameof(LastName)); SetPropertyChanged(nameof(FullName)); }
        }

        public string FullName { get => FirstName + " " + LastName; }

        [JsonProperty("contactNo")]
        public string ContactNo { get; set; }

        [JsonIgnore]
        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(Image)); SetPropertyChanged(nameof(UserProfleImage)); }
        }

        public string UserProfleImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }
        string _address { get; set; }
        [JsonProperty("address")]
        public string Address
        {
            get => _address;
            set { _address = value;SetPropertyChanged(nameof(Address)); }
        }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

    }

    //public class Medicine
    //{
    //    [JsonProperty("medicineId")]
    //    public int MedicineId { get; set; }

    //    [JsonProperty("medicineName")]
    //    public string MedicineName { get; set; }

    //    [JsonProperty("dosage")]
    //    public string Dosage { get; set; }

    //    [JsonProperty("prescriptionId")]
    //    public int PrescriptionId { get; set; }

    //    [JsonProperty("description")]
    //    public string Description { get; set; }

    //    [JsonProperty("dateAdded")]
    //    public DateTime DateAdded { get; set; }

    //    [JsonProperty("unit")]
    //    public string Unit { get; set; }

    //    [JsonProperty("preparation")]
    //    public string Preparation { get; set; }

    //    [JsonProperty("route")]
    //    public string Route { get; set; }

    //    [JsonProperty("direction")]
    //    public string Direction { get; set; }

    //    [JsonProperty("frequency")]
    //    public string Frequency { get; set; }

    //    [JsonProperty("prescription")]
    //    public object Prescription { get; set; }
    //}
}
