﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class AddApointmentResponseModel : BaseResponseModel
    {

    }
    public class AddApointmentInputModel
    {
        [JsonProperty("UserId")]
        public int UserId { get; set; }

        [JsonProperty("AppointmentDate")]
        public DateTime AppointmentDate { get; set; }

        //[JsonProperty("Appointment")]
        //public string Appointment { get; set; }
    }
    public class AcceptorRejectAppointmentResponseModel : BaseResponseModel
    {

    }
    public class AcceptorRejectAppointmentInputModel
    {
        [JsonProperty("AppointmentId")]
        public int AppointmentId { get; set; }

        [JsonProperty("Reason")]
        public string Reason { get; set; }

        [JsonProperty("Status")]
        public int Status { get; set; }

    }
}


