﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class SentToPharmcyResponseModel : BaseResponseModel
    {

    }

    public class SentToPharmcyInputModel : BaseModel
    {
        [JsonProperty("PrescriptionId")]
        public int PrescriptionId { get; set; }

        [JsonProperty("PharmacyId")]
        public int PharmacyId { get; set; }

        [JsonProperty("DeliveryOption")]
        public string DeliveryOption { get; set; }

        [JsonIgnore]
        string _address { get; set; }
        [JsonProperty("Address")]
        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                SetPropertyChanged(nameof(Address));
            }
        }

        [JsonIgnore]
        string _selectedOption;
        [JsonIgnore]
        public string SelectedOption
        {
            get => _selectedOption;
            set
            {
                _selectedOption = value;
                DeliveryOption = value;
                SetPropertyChanged(nameof(SelectedOption));
                SetPropertyChanged(nameof(DeliverySelected));
            }
        }

        [JsonIgnore]
        public bool DeliverySelected => SelectedOption == "Delivery";

        string _latitude { get; set; }
        [JsonProperty("latitude")]
        public string Latitude
        {

            get => _latitude;
            set
            {
                _latitude = value; SetPropertyChanged(nameof(Latitude));
            }
        }
        string _longitude { get; set; }
        [JsonProperty("longitude")]
        public string Longitude
        {

            get => _longitude;
            set
            {
                _longitude = value; SetPropertyChanged(nameof(Longitude));
            }
        }
    }

    public class UpdatePrescriptionByPharmacyInputModel : BaseModel
    {
        [JsonProperty("PrescriptionId")]
        public int PrescriptionId { get; set; }

        [JsonProperty("Amount")]
        public double Amount { get; set; }

        [JsonProperty("Status")]
        public PrescriptionApproval Status { get; set; }

        [JsonProperty("Reason")]
        public string Reason { get; set; }
    }

    public class UpdatePrescriptionByPharmacyResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
