﻿using System;
using Newtonsoft.Json;

namespace PrescriptionApp.Model
{
    public class GetUserDetailByIdInputModel
    {
         
        public int UserId { get; set; }
    }
    public class GetUserDetailByIdResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public UserProfileModel Profile { get; set; }
    }

}
