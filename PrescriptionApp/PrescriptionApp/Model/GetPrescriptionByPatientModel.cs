﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace PrescriptionApp.Model
{
   public  class GetPrescriptionByPatientReponseModel :BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<PrescriptionByUser> Data { get; set; }
    }
    public partial class PrescriptionByUser
    {
        [JsonProperty("prescriptionId")]
        public int PrescriptionId { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("name")]
        public string  Name { get; set; }

        [JsonProperty("image")]
        public string  Image { get; set; }

        [JsonProperty("prescriptionDate")]
        public DateTime PrescriptionDate { get; set; }

        [JsonProperty("mobileNo")]
        public string MobileNo { get; set; }

        [JsonProperty("disease")]
        public string  Disease { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("updateDate")]
        public DateTime UpdateDate { get; set; }
        

        [JsonProperty("status")]
        public PrescriptionApproval Status { get; set; }
         public Color StatusColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy
                       || Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                       && Settings.UserType == UserType.Doctor)
                    return Colors.Approve;
                else if((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma)
                    return Colors.Approve;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient) ||
                        (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.Pending;
                else if (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Patient )
                    return Colors.Approve;
                else if (Status == PrescriptionApproval.Reject)
                    return Colors.Reject;
                else
                    return Colors.Pending;
            }
        }

        public Color StatusBackgroundColor
        {
            get
            {
                if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.SentToPharmacy
                       || Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                       && Settings.UserType == UserType.Doctor)
                    return Colors.ApproveAlpha;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Doctor) ||
                    (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient) ||
                    Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Pharma)
                    return Colors.ApproveAlpha;
                else if ((Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient) ||
                        (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma))
                    return Colors.PendingAlpha;

                else if (Status == PrescriptionApproval.Accept && Settings.UserType == UserType.Patient)
                    return Colors.ApproveAlpha;
                else if (Status == PrescriptionApproval.Reject)
                    return Colors.RejectAlpha;
                else
                    return Colors.PendingAlpha;
            }
        }

        public string StatusName
        {
            get
            {
                if ((Status == PrescriptionApproval.Accept || Status == PrescriptionApproval.Reject)
                    && Settings.UserType == UserType.Doctor)
                    return "Sent";
                else if (Status == PrescriptionApproval.SentToCustomer && Settings.UserType == UserType.Patient)
                    return "Received";
                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Patient)
                    return "Sent to Pharmacy";
                else if (Status == PrescriptionApproval.SentToPharmacy && Settings.UserType == UserType.Pharma)
                    return "Received";
                else if ((Status == PrescriptionApproval.SentToCustomer || Status == PrescriptionApproval.Reject)
                    && Settings.UserType == UserType.Patient)
                    return "Reject By Pharmacy";
                else if ((Status == PrescriptionApproval.SentToCustomer 
                            || Status == PrescriptionApproval.Reject 
                         || Status == PrescriptionApproval.SentToPharmacy)   && Settings.UserType == UserType.Doctor)
                    return "Sent";

                else
                    return Enum.GetName(typeof(PrescriptionApproval), Status);
            }
        }
    }
    public  class GetPrescriptionByPatientInputModel
    {
        [JsonProperty("UserId")]
        public int UserId { get; set; }
    }

}
