﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class UpdateModel : BaseModel
    {

    }
    public class UpdateProfileResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public string Data { get; set; }
    }

    public class UpdateProfileInputModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("address")]
        public string UserAddress { get; set; }

        [JsonProperty("Contact")]
        public string Contact { get; set; }

        [JsonProperty("NationalId")]
        public string NationalId { get; set; }

        [JsonProperty("location")]
        public string GpsbaseLocation { get; set; }

        [JsonProperty("firstName")]
        public string UserFname { get; set; }

        [JsonProperty("lastName")]
        public string UserLname { get; set; }
        string _degree { get; set; }

        [JsonProperty("degree")]
        public string Degree { get; set; }

        [JsonProperty("OpeningTime")]
        public TimeSpan? OpeningTime { get; set; }

        [JsonProperty("ClosingTime")]
        public TimeSpan? ClosingTime { get; set; }

        [JsonProperty("HospitalName")]
        public string HospitalName { get; set; }

        [JsonProperty("BusinessRegistrationNumber")]
        public string BusinessRegistrationNumber { get; set; }

        [JsonProperty("Specialist")]
        public string Specialist { get; set; }

        [JsonProperty("Aboutme")]
        public string Aboutme { get; set; }
    }
}
