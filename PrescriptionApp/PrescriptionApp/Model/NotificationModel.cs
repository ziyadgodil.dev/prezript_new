﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PrescriptionApp.Model
{
    public class NotificationModel : BaseModel
    {
        [JsonProperty("messageId")]
        public int MessageId { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("notificationType")]
        public NotificationTypes NotificationType { get; set; }

        [JsonProperty("notificationTypeId")]
        public int? NotificationTypeId { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        string _Title { get; set; }
        [JsonProperty("title")]
        public string Title
        {
            get => _Title;
            set
            {
                _Title = value;
                SetPropertyChanged(nameof(Title));
            }

        }

        [JsonProperty("dateCreate")]
        public DateTime DateCreate { get; set; }

        [JsonIgnore]
        bool _status { get; set; }

        [JsonProperty("status")]
        public bool Status
        {
            get => _status;
            set
            {
                _status = value;
                SetPropertyChanged(nameof(Status));
            }
        }

    }

    //public class NotificationCountResponseModel : BaseResponseModel
    //{

    //    [JsonProperty("data")]
    //    public ObservableCollection<NotificationModel> Data { get; set; }
    //}
    public class NotificationResponseModel : BaseResponseModel
    {

        [JsonProperty("data")]
        public ObservableCollection<NotificationModel> Data { get; set; }

    }

}

