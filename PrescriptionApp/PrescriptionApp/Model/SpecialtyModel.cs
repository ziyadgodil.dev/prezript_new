﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{

    public class SpecialtyResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public AllData Data { get; set; }
    }
    public class AllData
    {
        [JsonProperty("speciality")]
        public ObservableCollection<SpecialtyModel> Speciality { get; set; }

    }


    public class SpecialtyModel : BaseModel
    {
        [JsonProperty("specialityId")]
        public int SpecialityId { get; set; }

        [JsonProperty("speciality")]
        public string Speciality { get; set; }

        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image;
            set
            {
                _image = value;
                SetPropertyChanged(nameof(Image));
            }
        }
        public string SpecialtyImage => ServiceConfiguration.SpecialityImageUrl + Image;

        [JsonIgnore]
        bool _isSelected { get; set; }
        [JsonIgnore]
        public bool IsSelected
        {
            get => _isSelected;
            set { _isSelected = value; SetPropertyChanged(nameof(IsSelected)); }
        }


    }
}
