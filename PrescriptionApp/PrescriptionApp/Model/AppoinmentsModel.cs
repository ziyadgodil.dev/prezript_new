﻿
using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;

namespace PrescriptionApp.Model
{
    public class AppoinmentsModel : BaseModel
    {
        [JsonProperty("appointmentId")]
        public int UserId { get; set; }

        [JsonProperty("contactNo")]
        public string ContactNo { get; set; }

        [JsonProperty("aboutMe")]
        public string AboutMe { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("degree")]
        public string Degree { get; set; }
        [JsonIgnore]
        public string AppoinmentImage { get => string.IsNullOrEmpty(Image) ? "usera" : Image; }

        [JsonProperty("firstName")]
        public string Name { get; set; }

        [JsonIgnore]
        public string FullName { get => Name + " " + LastName; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonIgnore]
        DateTime _date { get; set; }
        [JsonProperty("appoinmentdate")]
        public DateTime Date
        {
            get => _date;
            set
            {
                _date = value;
                SetPropertyChanged(nameof(Date));
                SetPropertyChanged(nameof(AppoinmentDate));
            }
        }
        public DateTime AppoinmentDate => Date.ToLocalTime();

        [JsonProperty("status")]
        AppointmentStatus _appointmentstatus { get; set; }
        public AppointmentStatus Appointmentstatus
        {
            get => _appointmentstatus;
            set
            {
                _appointmentstatus = value;
                SetPropertyChanged(nameof(Appointmentstatus));
                SetPropertyChanged(nameof(AppointmentStatusText));
                SetPropertyChanged(nameof(ColorText));
                SetPropertyChanged(nameof(ColorBgText));
            }

        }
        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonIgnore]
        public string AppointmentStatusText => Enum.GetName(typeof(AppointmentStatus), Appointmentstatus);

        [JsonIgnore]
        public Color ColorText
        {
            get
            {
                if (Appointmentstatus == AppointmentStatus.Approved)
                {
                    return Colors.Approve;
                }
                else if (Appointmentstatus == AppointmentStatus.Rejected)
                {
                    return Colors.Reject;
                }
                else if (Appointmentstatus == AppointmentStatus.Canceled)
                {
                    return Colors.Reject;
                }
                else if (Appointmentstatus == AppointmentStatus.Completed)
                {
                    return Colors.Approve;
                }
                else
                    return Colors.Pending;
            }
        }

        [JsonIgnore]
        public Color ColorBgText
        {
            get
            {
                if (Appointmentstatus == AppointmentStatus.Approved || Appointmentstatus == AppointmentStatus.Completed)
                {
                    return Colors.ApproveAlpha;
                }
                else if (Appointmentstatus == AppointmentStatus.Rejected || Appointmentstatus == AppointmentStatus.Canceled)
                {
                    return Colors.RejectAlpha;
                }
                else
                    return Colors.PendingAlpha;
            }
        }


    }
}

public class AppoinmentResponseModel : BaseResponseModel
{
    [JsonProperty("data")]
    public AppoinmentsData Data { get; set; }
}

public partial class AppoinmentsData
{
    [JsonProperty("upcoming")]
    public ObservableCollection<AppoinmentsModel> Upcoming { get; set; }

    [JsonProperty("past")]
    public ObservableCollection<AppoinmentsModel> Past { get; set; }
}

public class AppointmentUserProfileResponseModel : BaseResponseModel
{
    [JsonProperty("data")]
    public AppoinmentsData Data { get; set; }
}

public class AppointmentUserProfileInputModel
{
    [JsonProperty("appointmentId")]
    public int AppointmentId { get; set; }
}
public class AcceptorRejectAppointmentResponseModel : BaseResponseModel
{

}
public class AcceptorRejectAppointmentInputModel
{
    [JsonProperty("AppointmentId")]
    public int AppointmentId { get; set; }

    [JsonProperty("Reason")]
    public string Reason { get; set; }

    [JsonProperty("Status")]
    public AppointmentStatus Status { get; set; }

}