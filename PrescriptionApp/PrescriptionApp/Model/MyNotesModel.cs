﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using PrescriptionApp.Model;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{
    public class MyNotesModel : BaseModel
    {
        [JsonProperty("doctorId")]
        public int DoctorId { get; set; }

        [JsonProperty("customerId")]
        public int CustomerId { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonIgnore]
        public string FullName { get => FirstName + " " + LastName; }

        [JsonIgnore]
        string _image { get; set; }
        [JsonProperty("image")]
        public string image
        {
            get => _image;
            set { _image = value; SetPropertyChanged(nameof(image)); SetPropertyChanged(nameof(Image)); }
        }

        public string Image => string.IsNullOrEmpty(image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(image);

        [JsonProperty("contactNo")]
        public string ContactNo { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

    }
    public class MyNotesResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<MyNotesModel> Data { get; set; }

    }





}


//[JsonIgnore]
//public Color ColorText
//{
//    get
//    {
//        if (Appointmentstatus == AppointmentStatus.Approved)
//        {
//            return Colors.Approve;
//        }
//        else if (Appointmentstatus == AppointmentStatus.Rejected)
//        {
//            return Colors.Reject;
//        }
//        else if (Appointmentstatus == AppointmentStatus.Canceled)
//        {
//            return Colors.Reject;
//        }
//        else if (Appointmentstatus == AppointmentStatus.Completed)
//        {
//            return Colors.Approve;
//        }
//        else
//            return Colors.Pending;
//    }
//}

//[JsonIgnore]
//public Color ColorBgText
//{
//    get
//    {
//        if (Appointmentstatus == AppointmentStatus.Approved || Appointmentstatus == AppointmentStatus.Completed)
//        {
//            return Colors.ApproveAlpha;
//        }
//        else if (Appointmentstatus == AppointmentStatus.Rejected || Appointmentstatus == AppointmentStatus.Canceled)
//        {
//            return Colors.RejectAlpha;
//        }
//        else
//            return Colors.PendingAlpha;
//    }
//}



//public class AppointmentUserProfileResponseModel : BaseResponseModel
//{
//    [JsonProperty("data")]
//    public AppoinmentsData Data { get; set; }
//}

//public class AppointmentUserProfileInputModel
//{
//    [JsonProperty("appointmentId")]
//    public int AppointmentId { get; set; }
//}
