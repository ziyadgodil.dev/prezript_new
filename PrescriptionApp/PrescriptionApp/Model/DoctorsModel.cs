﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{
    public class DoctorsModel : BaseModel
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("firstName")]
        public string UserFname { get; set; }

        [JsonIgnore]
        public string FullName { get { return UserFname + " " + UserLname; } }

        [JsonProperty("lastName")]
        public string UserLname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTime Dob { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonIgnore]
        public string DoctorImage { get => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image); }

        [JsonProperty("contactNo")]
        public string Contact { get; set; }

        [JsonProperty("location")]
        public string GpsbaseLocation { get; set; }

        [JsonProperty("specialist")]
        public string Specialist { get; set; }

        [JsonProperty("specialityId")]
        public int SpecialityId { get; set; }

        [JsonProperty("openingTime")]
        public TimeSpan? OpeningTime { get; set; }

        [JsonProperty("closingTime")]
        public TimeSpan? ClosingTime { get; set; }

        [JsonProperty("degree")]
        public string Degree { get; set; }

        [JsonProperty("speciality")]
        public string Speciality { get; set; }

        [JsonProperty("specialityImage")]
        public string SpecialityImage { get; set; }
    }

    public class DoctorsResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<DoctorsModel> Data { get; set; }
    }

}
