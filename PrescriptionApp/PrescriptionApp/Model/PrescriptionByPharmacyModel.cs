﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PrescriptionApp.Model
{
  public  class PrescriptionByPharmacyResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<PrescriptionModel> Data { get; set; }
    }
    public class PrescriptionByPharmacyInputModel 
    {
        [JsonProperty("UserId")]
        public int UserId { get; set; }
    }
}
