﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class LogoutModel:BaseModel
    {

    }
    public class LogoutResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public Object Data { get; set; }
    }
}
