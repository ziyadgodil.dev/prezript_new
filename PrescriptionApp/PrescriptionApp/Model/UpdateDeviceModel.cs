﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
   public class UpdateDeviceModel :BaseModel 
    {

    }
    public class UpdateDeviceResponseModel : BaseResponseModel
    {

        [JsonProperty("data")]
        public object Data { get; set; }
    }
    public class UpdateDeviceInputModel
    {
        [JsonProperty("DeviceId")]
        public string DeviceId { get; set; }

        [JsonProperty("DeviceType")]
        public string DeviceType { get; set; }
    }
}
