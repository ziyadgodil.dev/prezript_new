﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
   public  class NotificationCountModel :BaseModel
    {
        [JsonProperty("notification")]
        public int Notification { get; set; }

        [JsonProperty("prescription")]
        public int Prescription { get; set; }

        [JsonProperty("appointment")]
        public int Appointment { get; set; }
    }
    public class NotificationCountResponseModel : BaseResponseModel
    {

        [JsonProperty("data")]
        public NotificationCountModel Data { get; set; }
    }
}
