﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class ReadWriteNotificationResponseModel:BaseResponseModel
    {

        [JsonProperty("data")]
        public object Data { get; set; }
    }
   public class ReadWriteNotificationInputModel:BaseModel
    {
        [JsonProperty("NotificationId")]
        public int NotificationId { get; set; }
    }
}
