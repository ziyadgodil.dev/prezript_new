﻿using System;
using Newtonsoft.Json;

namespace PrescriptionApp.Model
{
    public class MedicineModel
    {
        [JsonProperty("medicineId")]
        public long MedicineId { get; set; }

        [JsonProperty("medicineName")]
        public string MedicineName { get; set; }

        [JsonProperty("dosage")]
        public string Dosage { get; set; }

        [JsonProperty("prescriptionId")]
        public long PrescriptionId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("dateAdded")]
        public DateTime DateAdded { get; set; }

        [JsonProperty("prescription")]
        public object Prescription { get; set; }
    }
}
