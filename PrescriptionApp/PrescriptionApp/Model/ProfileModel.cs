﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class ProfileResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public UserProfileModel Data { get; set; }
    }

    public class UpdateImageResponseModel
    {

        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }
    }
    public class UpdateImageInputModel : BaseModel
    {
        string _image { get; set; }
        [JsonProperty("Image")]
        public string Image
        {
            get => _image; set { _image = value; SetPropertyChanged(nameof(Image)); }
        }

        string _imageName { get; set; }
        [JsonProperty("ImageName")]
        public string ImageName
        {
            get => _imageName; set { _imageName = value; SetPropertyChanged(nameof(ImageName)); }
        }
    }

}
