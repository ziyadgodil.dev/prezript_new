﻿using PrescriptionApp.Helper.Base;
using PrescriptionApp.Helper.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class LocationModel : BaseModel
    {


        public UserType UserType => Settings.UserType;

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Address { get; set; }
    }
}
