﻿using PrescriptionApp.Helper.Base;
using Xamarin.Forms;

namespace Prescription.Model
{

    public class OnBoardingModel : BaseModel
        {
            ImageSource _carosuelImage { get; set; }
            public ImageSource CarosuelImage
            {
                get => _carosuelImage;
                set
                {
                    _carosuelImage = value;
                    SetPropertyChanged(nameof(CarosuelImage));
                }
            }



            public string CarosuelTitle { get; set; }

            public string CarosuelDescription { get; set; }

            public string buttonNext { get; set; }
           
     
        public string Skip {get; set; } 




    }
}
