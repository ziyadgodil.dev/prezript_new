﻿using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace PrescriptionApp.Model
{
    public class PatientModel : BaseModel
    {
        string _userFname { get; set; }
        [JsonProperty("firstName")]
        public string UserFName
        {
            get => _userFname;
            set
            {
                _userFname = value;
                SetPropertyChanged(nameof(UserFName));
            }

        }
        [JsonIgnore]
        public string FullName { get { return UserFName + " " + UserLname; } }


        string _userLname { get; set; }
        [JsonProperty("lastName")]
        public string UserLname
        {
            get => _userLname;
            set
            {
                _userLname = value;
                SetPropertyChanged(nameof(UserLname));
            }

        }


        int _age { get; set; }
        [JsonProperty("age")]
        public int Age
        {
            get => _age;
            set
            {
                _age = value;
                SetPropertyChanged(nameof(Age));
            }

        }

        int _patientId { get; set; }
        [JsonProperty("patientId")]
        public int PatientId
        {
            get => _patientId;
            set
            {
                _patientId = value;
                SetPropertyChanged(nameof(PatientId));
            }

        }

        string _image { get; set; }
        [JsonProperty("image")]
        public string Image
        {
            get => _image; set { _image = value; SetPropertyChanged(nameof(Image)); SetPropertyChanged(nameof(PatientImage)); }
        }
        [JsonIgnore]
        public string PatientImage => string.IsNullOrEmpty(Image) ? "usera" : ServiceConfiguration.ImageUrl + Path.GetFileName(Image);

        string _userAddress { get; set; }
        [JsonProperty("address")]
        public string UserAddress
        {
            get => _userAddress; set { _userAddress = value; SetPropertyChanged(nameof(UserAddress)); }
        }

        string _contact { get; set; }
        [JsonProperty("contactNo")]
        public string Contact
        {

            get => _contact;
            set
            {
                _contact = value; SetPropertyChanged(nameof(Contact));
            }
        }



        string _aboutme { get; set; }
        [JsonProperty("aboutMe")]
        public string Aboutme
        {

            get => _aboutme;
            set
            {
                _aboutme = value; SetPropertyChanged(nameof(Aboutme));
            }
        }


        NotesModel _notes { get; set; }
        [JsonProperty("notes")]
        public NotesModel Notes
        {

            get => _notes;
            set
            {
                _notes = value; SetPropertyChanged(nameof(Notes));
            }
        }
    }

    public class AllPatientResponseModel : BaseResponseModel
    {
        [JsonProperty("data")]
        public ObservableCollection<PatientModel> Data { get; set; }
    }

}
