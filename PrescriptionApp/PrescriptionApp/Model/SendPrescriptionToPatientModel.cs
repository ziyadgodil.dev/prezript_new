﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class SendPrescriptionToPatientInputModel
    {
        public int PrescriptionId { get; set; }
    }
    public class SendPrescriptionToPatientResponseModel : BaseResponseModel
    {

    }

}
