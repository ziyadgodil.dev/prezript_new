﻿using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
   public class exerciseModel : BaseModel
    {
        string _header { get; set; }

        public string Header
        {
            get
            {
                return _header;
            }
            set
            {
                _header = value;
                SetPropertyChanged(nameof(Header));
            }
        }

        string _content { get; set; }

        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                SetPropertyChanged(nameof(Content));
            }
        }

        string _date { get; set; }

        public string Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
                SetPropertyChanged(nameof(Date));
            }
        }

        string _carosuelImage { get; set; }
        public string CarosuelImage
        {
            get => _carosuelImage;
            set
            {
                _carosuelImage = value;
                SetPropertyChanged(nameof(CarosuelImage));
            }
        }

        string _image2 { get; set; }
        public string Image2
        {
            get => _image2;
            set
            {
                _image2 = value;
                SetPropertyChanged(nameof(Image2));
            }
        }
    }
}
