﻿using System;
using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;

namespace PrescriptionApp.Model
{
    public class BaseResponseModel : BaseModel
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

}
