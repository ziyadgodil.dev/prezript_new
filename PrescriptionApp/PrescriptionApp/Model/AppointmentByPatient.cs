﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PrescriptionApp.Model
{
    public class AppointmentByPatientResponse :BaseResponseModel
    {       

        [JsonProperty("data")]
        public ObservableCollection<AppoinmentsModel> Data { get; set; }
    }
    
    public class AppointmentByPatientInput 
    {
        [JsonProperty("UserId")]
        public long UserId { get; set; }
    }
}
