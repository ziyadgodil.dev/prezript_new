﻿using PrescriptionApp.Helper.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrescriptionApp.Model
{
    public class DoctorsDetailsModel : BaseModel
    {
        public string ProfileImage { get; set; }
        public string Name { get; set; }
        public string Specilization { get; set; }
        public string Specialist { get; set; }
        public string Phone { get; set; }

        public string Rating { get; set; }
        public string About { get; set; }
        public string Timing { get; set; }
        public string Fees { get; set; }
        public string Address { get; set; }

    }
}
