﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

[assembly: ExportFont("Poppins-Black.ttf")]
[assembly: ExportFont("Poppins-BlackItalic.ttf")]
[assembly: ExportFont("Poppins-Bold.ttf")]
[assembly: ExportFont("Poppins-BoldItalic.ttf")]
[assembly: ExportFont("Poppins-ExtraBold.ttf")]
[assembly: ExportFont("Poppins-ExtraBoldItalic.ttf")]
[assembly: ExportFont("Poppins-ExtraLightItalic.ttf")]
[assembly: ExportFont("Poppins-ExtraLight.ttf")]
[assembly: ExportFont("Poppins-Italic.ttf")]
[assembly: ExportFont("Poppins-Light.ttf")]
[assembly: ExportFont("Poppins-LightItalic.ttf")]
[assembly: ExportFont("Poppins-Medium.ttf")]
[assembly: ExportFont("Poppins-MediumItalic.ttf")]
[assembly: ExportFont("Poppins-Regular.ttf")]
[assembly: ExportFont("Poppins-Semibold.ttf")]
[assembly: ExportFont("Poppins-SemiboldItalic.ttf")]
[assembly: ExportFont("Poppins-Thin.ttf")]
[assembly: ExportFont("Poppins-ThinItalic.ttf")]