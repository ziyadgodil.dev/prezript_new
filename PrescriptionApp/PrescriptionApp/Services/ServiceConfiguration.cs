using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PrescriptionApp.Services
{
    public static class ServiceConfiguration
    {
        //public static string URL = "http://4bed929e9fab.ngrok.io";
        //public static string URL = "http://soyebkhan-001-site1.ctempurl.com";
        public static string URL = "https://api.prezript.com";
     //   public static string URL = "http://52.175.222.176:8089";

        public static string BaseURL = URL + "/Prescriptions/";

        public static string SpecialityImageUrl = URL + "/Helper/Images/SpecialityImages/";

        public static string ImageUrl = URL + "/Helper/Images/ProfileImages/";

        public static string CommonErrorMessage = "Something went wrong please try again";



        public static string LoginURL = "login";
        public static string RenewToken = "RenewToken";
        public static string RegisterUser = "Registration";
        public static string UpdateProfile = "UpdateProfile";
        public static string GetUserDetails = "GetUserDetails";
        public static string UpdateImage = "ImageUpload";
        public static string GetPatientList = "GetPatientList";
        public static string GetAllAppointment = "GetAllAppointment";
        public static string GetAllPrescriptions = "GetAllPrescriptions";
        public static string GetAllPharmacy = "GetAllPharmacy";
        public static string GetAllDoctors = "GetAllDoctors";
        public static string GetUserDetailsbyid = "GetUserDetailsbyid";
        public static string GetAppointment = "GetAppintmentbyId";
        public static string GetPrescriptionsById = "GetPrescriptionsById";
        public static string GetAllSpeciality = "GetAllSpeciality";
        public static string GetAllNotes = "GetAllNotes";
        public static string AddAppointment = "AddAppointment";
        public static string AllNotesById = "AllNotesById";
        public static string AddNotes = "AddNotes";
        public static string AddMedicine = "AddMedicine";
        public static string AcceptorRejectAppointment = "AcceptorRejectAppointment";
        public static string RequestPrescription = "PrescriptionRequest";
        public static string SendPrescriptionToPharmacy = "SendPrescriptionToPharmacy";
        public static string SendPrescriptiontoPatient = "SendPrescriptiontoPatient";
        public static string PrescriptionUpdatebyPharmacy = "PrescriptionUpdatebyPharmacy";
        public static string AppointmentByPatient = "GetAppointmentByPatient"; 
        public static string GetUserNotesById = "GetUserNotesById";
        public static string GetPrescriptionByPatient = "GetPrescriptionByPatient";
        public static string GetPrescriptionByPharmacy = "GetPrescriptionByPharmacy"; 
        public static string UpdateDevice = "UpdateDevice"; 
        public static string GetAllNotification = "GetAllNotification"; 
        public static string Logout = "Logout"; 
        public static string ReadWriteNotification = "ReadWriteNotification"; 
        public static string NotificationCount = "NotificationCount"; 


    }
}
