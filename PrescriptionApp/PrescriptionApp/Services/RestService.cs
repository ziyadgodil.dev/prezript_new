﻿using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PrescriptionApp.Helper.Base;
using PrescriptionApp.Model;
using RestSharp;

namespace PrescriptionApp.Services
{
    public class RestService
    {
        RestClient _client;
        public RestService()
        {
            _client = new RestClient();
            Uri uri = new Uri(ServiceConfiguration.BaseURL);
            _client.BaseUrl = uri;
            if (!string.IsNullOrEmpty(Settings.UserToken))
            {
                _client.AddDefaultHeader("Authorization", "Bearer " + Settings.UserToken);
            }
        }

        async Task<string> GetResponse(string url, Method method, string body)
        {
            //url = "https://localhost:5001/Prescriptions" + url;
            try
            {
            Retry:
                var request = new RestRequest(method);
                request.Resource = url;
                if (method == Method.POST)
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await _client.ExecuteAsync(request);
                if (response.StatusCode == 0)
                {
                    await RenewToken();
                    goto Retry;
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await RenewToken();
                    goto Retry;
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new BaseResponseModel() { Status = false, Message = ServiceConfiguration.CommonErrorMessage });
            }   
        }


        public async Task RenewToken()
        {
            try
            {
                var request = new RestRequest(Method.POST);
                request.Resource = ServiceConfiguration.RenewToken;
                request.AddParameter("application/json", JsonConvert.SerializeObject(new RenewTokenInputModel()), ParameterType.RequestBody);
                var response = await _client.ExecuteAsync(request);

                if (response.IsSuccessful)
                {
                    var result = JsonConvert.DeserializeObject<RenewTokenResponseModel>(response.Content);
                    _client.RemoveDefaultParameter("Authorization");
                    _client.AddDefaultHeader("Authorization", "Bearer " + result.NewToken);
                    Settings.UserToken = result.NewToken;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public async Task<LoginResponseModel> Login(LoginInputModel loginInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.LoginURL, Method.POST, JsonConvert.SerializeObject(loginInputModel));
                var result = JsonConvert.DeserializeObject<LoginResponseModel>(response);
                _client.RemoveDefaultParameter("Authorization");
                _client.AddDefaultHeader("Authorization", "Bearer " + result.DataDetail.Token);
                Settings.UserToken = result.DataDetail.Token;
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new LoginResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<RegistrationResponseModel> Registration(RegistrationInputModel registrationInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.RegisterUser, Method.POST, JsonConvert.SerializeObject(registrationInputModel));

                var result = JsonConvert.DeserializeObject<RegistrationResponseModel>(response);
                _client.RemoveDefaultParameter("Authorization");
                _client.AddDefaultHeader("Authorization", "Bearer " + result.DataDetail.Token);
                Settings.UserToken = result.DataDetail.Token;
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new RegistrationResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<ProfileResponseModel> GetProfileDetail()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetUserDetails, Method.GET, null);
                var result = JsonConvert.DeserializeObject<ProfileResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new ProfileResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<ProfileResponseModel> UpdateProfile(UserProfileModel profileModelInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.UpdateProfile, Method.POST, JsonConvert.SerializeObject(profileModelInput));
                var result = JsonConvert.DeserializeObject<ProfileResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new ProfileResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<UpdateImageResponseModel> UpdateImage(UpdateImageInputModel updateImageInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.UpdateImage, Method.POST, JsonConvert.SerializeObject(updateImageInputModel));
                var result = JsonConvert.DeserializeObject<UpdateImageResponseModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new UpdateImageResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AllPatientResponseModel> GetPatientList()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetPatientList, Method.GET, null);
                var result = JsonConvert.DeserializeObject<AllPatientResponseModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AllPatientResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<PrescriptionResponseModel> GetPrescriptionDetails()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllPrescriptions, Method.GET, null);
                var result = JsonConvert.DeserializeObject<PrescriptionResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new PrescriptionResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AppoinmentResponseModel> GetAppoinmentsDetails()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllAppointment, Method.GET, null);
                var result = JsonConvert.DeserializeObject<AppoinmentResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AppoinmentResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<PharmaciesResponseModel> GetAllPharmacy()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllPharmacy, Method.GET, null);
                var result = JsonConvert.DeserializeObject<PharmaciesResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new PharmaciesResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }

        }

        public async Task<DoctorsResponseModel> GetAllDoctors()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllDoctors, Method.GET, null);
                var result = JsonConvert.DeserializeObject<DoctorsResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new DoctorsResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }

        }

        public async Task<AppointmentDetailsResponseModel> GetAppointment(AppointmentDetailsInputModel appointmentDetailsInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAppointment, Method.POST, JsonConvert.SerializeObject(appointmentDetailsInputModel));
                var result = JsonConvert.DeserializeObject<AppointmentDetailsResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AppointmentDetailsResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<GetUserDetailByIdResponseModel> GetUserDetailsById(GetUserDetailByIdInputModel getUserDetailByIdInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetUserDetailsbyid, Method.POST, JsonConvert.SerializeObject(getUserDetailByIdInput));
                var result = JsonConvert.DeserializeObject<GetUserDetailByIdResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new GetUserDetailByIdResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<PrescriptionByIdResponseModel> GetPrescriptionsById(PrescriptionByIdInputModel prescriptionByIdInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetPrescriptionsById, Method.POST, JsonConvert.SerializeObject(prescriptionByIdInput));
                var result = JsonConvert.DeserializeObject<PrescriptionByIdResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new PrescriptionByIdResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SpecialtyResponseModel> GetAllSpeciality()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllSpeciality, Method.GET, null);
                var result = JsonConvert.DeserializeObject<SpecialtyResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new SpecialtyResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<MyNotesResponseModel> GetAllNotes()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllNotes, Method.GET, null);
                var result = JsonConvert.DeserializeObject<MyNotesResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new MyNotesResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AddNotesResponseModel> AddNotes(NotesModel notesModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AddNotes, Method.POST, JsonConvert.SerializeObject(notesModel));
                var result = JsonConvert.DeserializeObject<AddNotesResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AddNotesResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AddApointmentResponseModel> AddAppointment(AddApointmentInputModel addpointmentInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AddAppointment, Method.POST, JsonConvert.SerializeObject(addpointmentInputModel));
                var result = JsonConvert.DeserializeObject<AddApointmentResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AddApointmentResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<NotesResponseModel> AllNotesById(AllNotesByIdInputModel allNotesByIdInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AllNotesById, Method.POST, JsonConvert.SerializeObject(allNotesByIdInput));
                var result = JsonConvert.DeserializeObject<NotesResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new NotesResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AddMedicineResponseModel> AddMedicine(AddEditmedicineModel addMedicineInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AddMedicine,
                    Method.POST,
                    JsonConvert.SerializeObject(addMedicineInputModel));
                var result = JsonConvert.DeserializeObject<AddMedicineResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AddMedicineResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<AcceptorRejectAppointmentResponseModel> AcceptorRejectAppointment(AcceptorRejectAppointmentInputModel acceptorRejectAppointmentInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AcceptorRejectAppointment,
                    Method.POST,
                    JsonConvert.SerializeObject(acceptorRejectAppointmentInputModel));
                var result = JsonConvert.DeserializeObject<AcceptorRejectAppointmentResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AcceptorRejectAppointmentResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<RequestPrescriptionResponseModel> RequestPrescription(RequestPrecriptionModel requestPrecriptionModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.RequestPrescription,
                    Method.POST,
                    JsonConvert.SerializeObject(requestPrecriptionModel));
                var result = JsonConvert.DeserializeObject<RequestPrescriptionResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new RequestPrescriptionResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SentToPharmcyResponseModel> SentPrescriptionToPharmacy(SentToPharmcyInputModel sentToPharmcyInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.SendPrescriptionToPharmacy,
                    Method.POST,
                    JsonConvert.SerializeObject(sentToPharmcyInputModel));

                var result = JsonConvert.DeserializeObject<SentToPharmcyResponseModel>(response);

                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new SentToPharmcyResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<SendPrescriptionToPatientResponseModel> SendPrescriptiontoPatient(SendPrescriptionToPatientInputModel sendPrescriptionToPatientInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.SendPrescriptiontoPatient,
                    Method.POST,
                    JsonConvert.SerializeObject(sendPrescriptionToPatientInputModel));
                var result = JsonConvert.DeserializeObject<SendPrescriptionToPatientResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new SendPrescriptionToPatientResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

        public async Task<UpdatePrescriptionByPharmacyResponseModel> PrescriptionUpdatebyPharmacy(UpdatePrescriptionByPharmacyInputModel updatePrescriptionByPharmacyInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.PrescriptionUpdatebyPharmacy,
                    Method.POST,
                    JsonConvert.SerializeObject(updatePrescriptionByPharmacyInput));

                var result = JsonConvert.DeserializeObject<UpdatePrescriptionByPharmacyResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new UpdatePrescriptionByPharmacyResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<AppointmentByPatientResponse> GetAppointmentByPatient(AppointmentByPatientInput appointmentByPatientInput)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.AppointmentByPatient, Method.POST, JsonConvert.SerializeObject(appointmentByPatientInput));
                var result = JsonConvert.DeserializeObject<AppointmentByPatientResponse>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new AppointmentByPatientResponse();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<NotesResponseModel> GetUserNotesById(GetUserNotesByIdInputModel getUserNotesByIdInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetUserNotesById, Method.POST, JsonConvert.SerializeObject(getUserNotesByIdInputModel));
                var result = JsonConvert.DeserializeObject<NotesResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new NotesResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<GetPrescriptionByPatientReponseModel> GetPrescriptionByPatient(GetPrescriptionByPatientInputModel getPrescriptionByPatientInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetPrescriptionByPatient, Method.POST, JsonConvert.SerializeObject(getPrescriptionByPatientInputModel));
                var result = JsonConvert.DeserializeObject<GetPrescriptionByPatientReponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new GetPrescriptionByPatientReponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<UpdateDeviceResponseModel> UpdateDevice(UpdateDeviceInputModel updateDeviceInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.UpdateDevice, Method.POST, JsonConvert.SerializeObject(updateDeviceInputModel));
                var result = JsonConvert.DeserializeObject<UpdateDeviceResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new UpdateDeviceResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<NotificationResponseModel> GetNotification()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.GetAllNotification, Method.GET, null);
                var result = JsonConvert.DeserializeObject<NotificationResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new NotificationResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<LogoutResponseModel> Logout()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.Logout, Method.GET, null);
                var result = JsonConvert.DeserializeObject<LogoutResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new LogoutResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        public async Task<ReadWriteNotificationResponseModel> ReadWriteNotification(ReadWriteNotificationInputModel readWriteNotificationInputModel)
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.ReadWriteNotification, Method.POST, JsonConvert.SerializeObject(readWriteNotificationInputModel));
                var result = JsonConvert.DeserializeObject<ReadWriteNotificationResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new ReadWriteNotificationResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }
        
        public async Task<NotificationCountResponseModel> GetNotificationCount()
        {
            try
            {
                var response = await GetResponse(ServiceConfiguration.NotificationCount, Method.GET, null);
                var result = JsonConvert.DeserializeObject<NotificationCountResponseModel>(response);
                return result;

            }
            catch (Exception ex)
            {
                ex.ToString();
                var result = new NotificationCountResponseModel();
                result.Status = false;
                result.Message = ServiceConfiguration.CommonErrorMessage;
                return result;
            }
        }

    }


}

