﻿using PrescriptionApp.CustomeRender;
using PrescriptionApp.iOS.CustomRenderer;
using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TimePickerCtrl), typeof(TimePickerCtrlRenderer))]
namespace PrescriptionApp.iOS.CustomRenderer
{
    public class TimePickerCtrlRenderer : TimePickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            if (this.Control == null)
                return;
            Control.BorderStyle = UITextBorderStyle.None;
            UITextField entry = Control;
            UIDatePicker picker = (UIDatePicker)entry.InputView;
            picker.PreferredDatePickerStyle = UIDatePickerStyle.Wheels;
        }
        private void OnCanceled(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
        private void OnDone(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
    }
}
