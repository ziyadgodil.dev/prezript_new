﻿using PrescriptionApp.CustomeRender;
using PrescriptionApp.iOS.CustomRenderer;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PLPicker), typeof(PLPickerRenderer))]
namespace PrescriptionApp.iOS.CustomRenderer
{
    public class PLPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                Control.Layer.BorderWidth = 0;
                Control.BorderStyle = UITextBorderStyle.None;
            }
        }
    }
}