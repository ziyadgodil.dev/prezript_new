﻿using System;
using PrescriptionApp.CustomeRender;
using PrescriptionApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(DatePickerCtrl), typeof(DatePickerCtrlRenderer))]
namespace PrescriptionApp.iOS
{
    public class DatePickerCtrlRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            if (this.Control == null)
                return;
            Control.BorderStyle = UITextBorderStyle.None;
            UITextField entry = Control;
            UIDatePicker picker = (UIDatePicker)entry.InputView;
            picker.PreferredDatePickerStyle = UIDatePickerStyle.Wheels;
        }
        private void OnCanceled(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
        private void OnDone(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
    }
}