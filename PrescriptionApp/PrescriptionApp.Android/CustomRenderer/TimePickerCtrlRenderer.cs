﻿
using Android.Graphics.Drawables;
using PrescriptionApp.CustomeRender;
using PrescriptionApp.Droid.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TimePickerCtrl), typeof(TimePickerCtrlRenderer))]  
namespace PrescriptionApp.Droid.CustomRenderer
{
    public class TimePickerCtrlRenderer : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            GradientDrawable gd = new GradientDrawable();
            this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            this.Control.SetPadding(0, 0, 0, 0);
            this.Control.SetBackgroundDrawable(gd);
            this.Control.SetTextColor(Android.Graphics.Color.Black);

            //  SetCornerRadius(25); //increase or decrease to changes the corner look
        }


    }
}