﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PrescriptionApp.CustomeRender;
using PrescriptionApp.Droid.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PLPicker), typeof(PLPickerRenderer))]
namespace PrescriptionApp.Droid.CustomRenderer
{
    public class PLPickerRenderer : PickerRenderer
    {
        public PLPickerRenderer(Context context) : base(context) { }
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            try
            {
                if (e.OldElement == null)
                {
                    Control.Background = null;

                    var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                    layoutParams.SetMargins(0, 0, 0, 0);
                    LayoutParameters = layoutParams;
                    Control.LayoutParameters = layoutParams;
                    Control.SetPadding(0, 0, 0, 0);
                    SetPadding(0, 0, 0, 0);

                    Control.SetHintTextColor((Element as PLPicker).SetHintTextColor.ToAndroid());

                }

                if (Element.IsEnabled == false)
                {
                    //Set the text color when the DatePicker is enabled
                    Control.SetTextColor(Android.Graphics.Color.Black);

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}