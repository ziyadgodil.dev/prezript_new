﻿using PrescriptionApp.Droid.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(ButtonZeroPaddingRenderer))]
namespace PrescriptionApp.Droid.CustomRenderer
{
    public class ButtonZeroPaddingRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            Control?.SetPadding(0, Control.PaddingTop, 0, Control.PaddingBottom);
        }
    }
}