﻿using System;
using Android.Graphics.Drawables;
using PrescriptionApp.CustomeRender;
using PrescriptionApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(DatePickerCtrl), typeof(DatePickerCtrlRenderer))]
namespace PrescriptionApp.Droid
{
    public class DatePickerCtrlRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            GradientDrawable gd = new GradientDrawable();
            this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            this.Control.SetPadding(0, 0, 0, 0);
           this.Control.SetBackgroundDrawable(gd);
            this.Control.SetTextColor(Android.Graphics.Color.Black);
          
            //  SetCornerRadius(25); //increase or decrease to changes the corner look
        }
    }
}