﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;
using Xamarin.Essentials;
using PrescriptionApp.Helper.Base;
using AndroidX.Core.App;

namespace PrescriptionApp.Droid.Firebase
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;
        public override void OnNewToken(string token)
        {
            base.OnNewToken(token);
            Settings.FCMToken = token;
            Settings.NewToken = true;
        }

        public override void OnMessageReceived(RemoteMessage message)
        {
            var title = message.GetNotification().Title;
            var body = message.GetNotification().Body;
            SendNotification(title, body, message);
        }

        void SendNotification(string title, string messageBody, RemoteMessage message)
        {
            CreateNotificationChannel();
            var intent = new Intent(this, typeof(MainActivity));
            intent.PutExtra("Prescription", messageBody);
            intent.AddFlags(ActivityFlags.ClearTop);
            foreach (var key in message.Data.Keys)
            {
                intent.PutExtra(key, message.Data[key]);
            }
            // intent.PutExtra("pushnotification", "yes");
            var pendingIntent = PendingIntent.GetActivity(this, NOTIFICATION_ID, intent, PendingIntentFlags.OneShot);
            Notification.BigTextStyle textStyle = new Notification.BigTextStyle();

            var notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                                      .SetSmallIcon(Resource.Drawable.Prezript)
                                      .SetContentTitle(title)
                                      .SetContentText(messageBody)
                                      .SetAutoCancel(true)
                                      .SetStyle(new NotificationCompat.BigTextStyle().BigText(messageBody))
                                      .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate)
                                      .SetContentIntent(pendingIntent);
            //.SetColor(Resources.GetColor(Android.Resource.Color.Transparent));

            var notificationManager = NotificationManagerCompat.From(this);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID, "FCM Notifications", NotificationImportance.Default)
            {
                Importance = NotificationImportance.Max,
                LockscreenVisibility = 0

            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

    }
}